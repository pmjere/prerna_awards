 
<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Prerna Awards</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function () {
                    if (this.checked) {
                        checkbox.each(function () {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function () {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function () {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
        </script>
    </head>
    <body>
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="homepage.html">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav">                                 
                                 
                                    <li><a onclick="window.location.href = 'Update_profile.php'">Back</a></li>
                                    <li><a onclick="window.location.href = 'cont_Login.php'">Logout</a></li>

                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->


        <section id="registration">
            <div class="container">   
                <div class="sectionContent">
                    <div class="row">
                         <div class="col-md-6">
                            <div class="text-center">

                                <img src="images/what-we-do/cup.png" alt=""/>
<!--                                <img src="images/what-we-do/regsquad-teamjump.png" alt="">-->
                            </div>
                        </div>
                        <div class="col-md-6"> 

                            <br><br>  <h2 class="nino-sectionHeading">
                                Competitions
                            </h2>
                             <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>

                                        <th style="font-size: 20px;color: black">Language</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>

                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>

                                        <th style="font-size: 20px;color: black">Competition</th>  

                                    </tr>
                                </thead>
                                <tbody style="font-size: 15px;color: black">
                                    <?php
                                    $link = DbConnect::GetConnection();
                                    if ($link) {
                                        $RId = $_SESSION['Creg_id'];
//                                $sql = " SELECT * FROM    contestant_registration r  INNER JOIN language l ON r.Language_id = l.lang_id INNER JOIN competition c ON r.competition_id = c.com_id where Contestant_id='$RId'";
                                        $sql = "SELECT * FROM  participation r  INNER JOIN language l ON r.pLanguage_id = l.lang_id INNER JOIN competition c ON r.pCompetition_id = c.com_id where pCont_id='$RId'";
                                        $result = $link->query($sql);
                                        $record = '';
                                        while ($row = $result->fetch_assoc()) {

                                            $reg_id = $row["pLanguage_id"];
                                            $reg_id = $row["pCompetition_id"];

                                            $record .= '<tr><td>' . $row["language"] . '</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>' . $row["competition"] . '</td><td></td><td>
                                                     <a href="" onclick="editCompetition(\'' . $row["language"] . '\',\'' . $row["competition"] . '\',\'' . $row["File_Name"] . '\',\'' . $row["p_id"] . '\')" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                            </td></tr>';
                                        }
                                        echo $record;
                                    }
                                    ?>
                                </tbody>
                            </table>
                             </div>
                        </div>                   
                    </div>               
                </div>     
        </section>

        <div id="editCompModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="Update_comp.php" method="post" enctype="multipart/form-data">
                        <div class="modal-header">						
                            <h4 class="modal-title">Edit Language</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">	
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <input type="text" class="form-control" name="eId" id="eId" style="display: none" >
                                    <label>Language : </label>
                                    <input type="text" class="form-control" name="LanguageEdit" id="LanguageEdit"disabled="disabled" >
                                </div>

                            </div>	
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <label>Competition : </label>
                                    <input type="text" class="form-control" name="CompEdit" id="CompEdit"disabled="disabled" >
                                </div>
                            </div>	

                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <label>Uploaded File : -</label>
                                    <label id="FileEdit">File : -</label>  
                                </div>
                            </div>	
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <input type="checkbox" name="update" id="update" value="Uncheck"/> Do You Want to Update this File?
                                </div>
                            </div>
                            <div class="form-group"  id="Ufile1" style="display: none">   
                                <div class='input-group input-group-lg'>
                                    <input type="file" name="Ufile" id="Ufile" required/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">                     
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-success" value="Add" id="submit" name="submit" >
                        </div>                     
                    </form>
                </div>
            </div>
        </div>
        
        <script>

            function editCompetition(lang, comp, file, id)
            {

                $("#LanguageEdit").val(lang);
                $("#CompEdit").val(comp);
                $("#FileEdit").text(file);
                $("#eId").val(id);
                $("#editCompModal").modal();
            }
        </script>
        <script>
        $("#update").click(function () {
            var updateFile = $('#update').is(':checked');

            if (updateFile == true)
            {
                document.getElementById("Ufile1").style.display = "block";
                document.getElementById("Bedit").style.display = "block";

            } else
            {
                document.getElementById("Ufile1").style.display = "none";
                document.getElementById("Bedit").style.display = "none";
            }

        })
    </script>

        <!-- Edit Modal HTML -->
         
        <!-- Delete Modal HTML -->
         
         
        <footer id="footer">
            <div class="nino-copyright">
                <div class="row">
                    <div class="colInfo">
                        <div class="col-md-1"></div>
                        <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                            <ul class="nav navbar-nav">
                                <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                       <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3" style="padding-left: 30px">
                            <div class="nino-followUs">
                                <div class="socialNetwork">
                                    <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                    <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
    <!--                                <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                                                <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>-->
    <!--                                                            <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
        </div>
    </footer>
</body>

</html>     
<?php
$recordAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
}

if ($recordAdded) {
    echo '
<script type="text/javascript">
    function hideMsg()
    {
        document.getElementById("popup").style.visibility = "hidden";
    }

    document.getElementById("popup").style.visibility = "visible";
    window.setTimeout("hideMsg()", 2000);
</script>';
}
?>