 

<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 
                                    <!--                                    <li><a onclick="window.location.href = 'index.php'">Home</a></li>-->
                                    <li><a onclick="window.location.href = 'Update_profile.php'">Back</a></li>
                                    <li><a onclick="window.location.href = 'cont_Login.php'">Logout</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->




        <!-- Services
        ================================================== -->
        <section id="registration">
            <div class="container">
                <h2 class="nino-sectionHeading">
<!--                    <span class="nino-subHeading">For all users</span>-->
                    Register For Competition
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">

                                <img src="images/what-we-do/cup.png" alt=""/>
<!--                                <img src="images/what-we-do/regsquad-teamjump.png" alt="">-->
                            </div>
                        </div>
                        <div class="col-md-6"> 
                            <!--                            <form method="post" autocomplete="off" action="Update_comp.php"id="frm_update" name="frm_update" enctype="multipart/form-data" class="nino-subscribeForm">
                                                            <lable id="language_error" style="color: Red; display: none">Please Select Language</lable>
                                                            <lable id="RegComp_error" style="color: Red; display: none">You Are already registred for this Competition</lable>
                                                            <div class="form-group">
                                                                <div class="input-group input-group-lg">
                                                                    <span class="input-group-addon"><span class="fa fa-american-sign-language-interpreting text-success"  style="color: #172f6a"></span></span>
                            <?php
                            $link = DbConnect::GetConnection();
                            if ($link) {
                                $sql = "SELECT lang_id, language FROM language";
                                $result = $link->query($sql);
                                $select = '<select name="Language" id="Language"  class="form-control" required>';
                                $select .= '<option value="">Select Language</option>';
                                while ($row = $result->fetch_assoc()) {
                                    $select .= '<option value="' . $row['lang_id'] . '">' . $row['language'] . '</option>';
                                }
                                $select .= '</select>';
                                echo $select;
                            }
                            ?>
                                                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                                                    <script type="text/javascript">
                                                                    $("#Language").blur(function () {
                                                                        if ($(this).val() == "") {
                                                                            document.getElementById("language_error").style.display = 'block';
                                                                            $("#Language").focus();
                                                                        } else
                                                                        {
                                                                            document.getElementById("language_error").style.display = 'none';
                                                                        }
                                                                    });
                                                                    </script>
                            
                                                                </div>
                                                            </div>
                                                            <lable id="comp_error" style="color: Red; display: none">Please Select Competition</lable>
                                                            <div class="form-group" >
                                                                <div class="input-group input-group-lg">
                                                                    <span class="input-group-addon" ><span class="glyphicon glyphicon-copyright-mark text-success"  style="color: #172f6a"></span></span>
                                                                    <div id="div_comp"  name ="div_comp" class="input-group input-group-lg" style="width: 100%">
                                                                    </div>
                                                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                                                    <script type="text/javascript">
                                                                    $("#Compitation").blur(function () {
                                                                        if ($(this).val() == "") {
                                                                            document.getElementById("comp_error").style.display = 'block';
                                                                            $("#Compitation").focus();
                                                                        } else
                                                                        {
                                                                            document.getElementById("comp_error").style.display = 'none';
                                                                        }
                                                                    });
                                                                    </script>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">                    
                                                                <div class='input-group input-group-lg'>
                                                                    <input type="file" name="file" id="file"  required/>
                                                                </div>                    
                                                            </div>
                            
                                                            <lable id="PFee25" style="color: orange;font-size: 20px; text-align: center; display: block">Participation Fee : 25$</lable> 
                                                            <lable id="PFee10" style="color: orange;font-size: 20px; text-align: center; display: none">Participation Fee : 10$</lable> 
                                                            <div class="form-group">
                                                                <div class="input-group input-group-lg" id="check">
                                                                    <input type="checkbox" name="checkpromo" id="checkpromo" value="Uncheck"/> I have a Promocode
                                                                </div>
                                                            </div>
                            
                                                            <script>
                                                                $("#checkpromo").click(function () {
                                                                    var promoFile = $('#checkpromo').is(':checked');
                            
                                                                    if (promoFile == true)
                                                                    {
                                                                        document.getElementById("promocode").style.display = "block";
                                                                        document.getElementById("apply").style.display = "block";
                                                                        document.getElementById("promo25").style.display = "none";
                            //                                            document.getElementById("PFee25").style.display = "none";
                                                                           document.getElementById("promo_sucess").style.display = "none";
                                                                            document.getElementById("promo_error").style.display = "none";
                            
                            
                            
                                                                    } else
                                                                    {
                                                                        document.getElementById("promocode").style.display = "none";
                                                                        document.getElementById("apply").style.display = "none";
                                                                        document.getElementById("promo25").style.display = "block";
                            //                                            document.getElementById("PFee25").style.display = "block";
                                                                         document.getElementById("promo_sucess").style.display = "none";
                                                                          document.getElementById("promo_error").style.display = "none";
                            
                                                                    }
                            
                                                                })
                                                            </script>
                            
                            
                                                               <lable id="promo_sucess" style="color: green; display: none">Promocode is sucessfully applied.</lable>
                                                            <lable id="promo_error" style="color: Red; display: none">Promocode is not applied.</lable>
                                                            
                                                            <div class="form-group" name="promocode" id="promocode" style="display: none">
                                                                <div class="input-group input-group-lg">
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user text-success"  style="color: #172f6a" ></span></span>
                                                                    <input type="text"  class="form-control" name="txtpromocode" id="txtpromocode" placeholder="Enter Promocode"  required/>
                                                                </div>
                                                            </div>   
                            
                                                            <div class="form-group">
                                                                <button type="button" class="btn btn-block btn-success" name="apply" id="apply"style="display: none">Apply </button>
                                                            </div>
                                                            <hr>
                                                            <div class="form-group">
                                                                <button type="submit" class="btn btn-block btn-success" name="signup" id="signup" style="display: none">Register </button>
                                                            </div>
                            
                                                        </form>-->


                            <!--                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="promo25" name="promo25" style="display: block" >
                                                            <input type="hidden" name="cmd" value="_xclick">
                                                            <input type="hidden" name="business" value="hindifoundation@gmail.com">
                                                            <input type="hidden" name="lc" value="US">
                                                            <input type="hidden" name="item_name" value="Prerna Awards Participation Fee">
                                                            <input type="hidden" name="amount" value="25.00">
                                                            <input type="hidden" name="currency_code" value="SGD">
                                                            <input type="hidden" name="button_subtype" value="services">
                                                            <input type="hidden" name="no_note" value="0">
                                                            <input type="hidden" name="tax_rate" value="0.000">
                                                            <input type="hidden" name="shipping" value="0.00">
                                                            <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
                                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif?akam_redir=1" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                             
                                                        </form>
                            
                            
                                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="promo10" name="promo10" style="display: none">
                                                            <input type="hidden" name="cmd" value="_xclick">
                                                            <input type="hidden" name="business" value="hindifoundation@gmail.com">
                                                            <input type="hidden" name="lc" value="US">
                                                            <input type="hidden" name="item_name" value="Prerna Awards Participation Fee">
                                                            <input type="hidden" name="amount" value="10.00">
                                                            <input type="hidden" name="currency_code" value="SGD">
                                                            <input type="hidden" name="button_subtype" value="services">
                                                            <input type="hidden" name="no_note" value="0">
                                                            <input type="hidden" name="tax_rate" value="0.000">
                                                            <input type="hidden" name="shipping" value="0.00">
                                                            <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
                                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif?akam_redir=1" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                             
                                                        </form>-->

                            <!--                            <div id="popup3"style="display: none">
                                                            <h2 style="text-align: center">Thank you!</h2> 
                                                            <div class="content" style="text-align: center">                               
                                                                <br><br>Team Prerna wishes you the very best. Thank you for your participation!
                                                                <form action="Competition_reg.php">
                                                                    <br><br><button type="submit" class="btn btn-block btn-success">Register for another competititon </button>
                                                                </form>
                                                                <form action="Update_profile.php">
                                                                    <br><br><button type="submit" class="btn btn-block btn-success">Back </button>
                                                                </form>
                                                            </div>
                                                        </div> -->
                            <br><br>  <h2 class="nino-sectionHeading">
                                Competitions
                            </h2>
                            <table class="table table-striped table-hover table-responsive ">
                                <thead>
                                    <tr>

                                        <th style="font-size: 20px;color: black">Language</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>

                                        <th style="font-size: 20px;color: black">Competition</th>  

                                    </tr>
                                </thead>
                                <tbody style="font-size: 15px;color: black">
                                    <?php
                                    $link = DbConnect::GetConnection();
                                    if ($link) {
                                        $RId = $_SESSION['Creg_id'];
//                                $sql = " SELECT * FROM    contestant_registration r  INNER JOIN language l ON r.Language_id = l.lang_id INNER JOIN competition c ON r.competition_id = c.com_id where Contestant_id='$RId'";
                                        $sql = "SELECT * FROM  participation r  INNER JOIN language l ON r.pLanguage_id = l.lang_id INNER JOIN competition c ON r.pCompetition_id = c.com_id where pCont_id='$RId'";
                                        $result = $link->query($sql);
                                        $record = '';
                                        while ($row = $result->fetch_assoc()) {

                                            $reg_id = $row["pLanguage_id"];
                                            $reg_id = $row["pCompetition_id"];

                                            $record .= '<tr><td>' . $row["language"] . '</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>' . $row["competition"] . '</td><td></td><td>
                                                     <a href="" onclick="editCompetition(\'' . $row["language"] . '\',\'' . $row["competition"] . '\',\'' . $row["File_Name"] . '\',\'' . $row["p_id"] . '\')" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                            </td></tr>';
                                        }
                                        echo $record;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--            <div id="popup">
            <Center>
                Record added successfully 
                    </Center>
                    </div>-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>


        <script>
                                        $(function () {
                                            $("#Language").change(function () {
                                                //               var selectedText = $(this).find("option:selected").text();
                                                //                var selectedValue = $(this).val();
                                                //                alert("Selected Text: " + selectedText + " Value: " + selectedValue);
                                                LoadComp();
                                            });

                                            LoadComp();

                                        });



                                        function LoadComp() {
                                            $.ajax({
                                                url: 'Reg_responce.php',
                                                type: "POST",
                                                data: ({Language: $("#Language").val()}),

                                                success: function (data) {
                                                    console.log(data);
                                                    $("#div_comp").html(data);
                                                }
                                            });
                                        }
        </script>

        <!--/#nino-services-->
        <div id="editCompModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="Edit_RegCompetition.php" method="post" enctype="multipart/form-data">
                        <div class="modal-header">						
                            <h4 class="modal-title">Edit Language</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">	
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <input type="text" class="form-control" name="eId" id="eId" style="display: none" >
                                    <label>Language : </label>
                                    <input type="text" class="form-control" name="LanguageEdit" id="LanguageEdit"disabled="disabled" >
                                </div>

                            </div>	
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <label>Competition : </label>
                                    <input type="text" class="form-control" name="CompEdit" id="CompEdit"disabled="disabled" >
                                </div>
                            </div>	

                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <label>Uploaded File : -</label>
                                    <label id="FileEdit">File : -</label>  
                                </div>
                            </div>	
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <input type="checkbox" name="update" id="update" value="Uncheck"/> Do You Want to Update this File?
                                </div>
                            </div>
                            <div class="form-group"  id="Ufile1" style="display: none">   
                                <div class='input-group input-group-lg'>
                                    <input type="file" name="Ufile" id="Ufile" required/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">                     
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-success" value="Add" >
                        </div>                     
                    </form>
                </div>
            </div>
        </div>
        <footer id="footer">
            <div class="nino-copyright">
                <div class="row">
                    <div class="colInfo">
                        <div class="col-md-1"></div>
                        <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                            <ul class="nav navbar-nav">
                                <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3" style="padding-left: 30px">
                            <div class="nino-followUs">
                                <div class="socialNetwork">
                                    <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                    <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
    <!--                                <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                                                <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>-->
    <!--                                                            <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
        </div>
    </footer>
    <!-- Search Form - Display when click magnify icon in menu
    ================================================== -->
    <form action="" id="nino-searchForm">
        <input type="text" placeholder="Search..." class="form-control nino-searchInput">
        <i class="mdi mdi-close nino-close"></i>
    </form><!--/#nino-searchForm-->

    <!-- Scroll to top
    ================================================== -->
    <a href="#" id="nino-scrollToTop">Go to Top</a>

    <!-- javascript -->


    <script type="text/javascript" src="js/jquery.min.js"></script>	
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/unslider-min.js"></script>
    <script type="text/javascript" src="js/template.js"></script>
    <script src="js_1/moment.js" type="text/javascript"></script>

    <script>

                                        function editCompetition(lang, comp, file, id)
                                        {

                                            $("#LanguageEdit").val(lang);
                                            $("#CompEdit").val(comp);
                                            $("#FileEdit").text(file);
                                            $("#eId").val(id);
                                            $("#editCompModal").modal();
                                        }
    </script>

    <script>
        $("#update").click(function () {
            var updateFile = $('#update').is(':checked');

            if (updateFile == true)
            {
                document.getElementById("Ufile1").style.display = "block";
                document.getElementById("Bedit").style.display = "block";

            } else
            {
                document.getElementById("Ufile1").style.display = "none";
                document.getElementById("Bedit").style.display = "none";
            }

        })
    </script>
    <script>
        $("#apply").click(function () {


            $.ajax({
                url: 'applycode.php',
                type: "POST",
                data: ({Promo: $('#txtpromocode').val()}),

                async: false,
                success: function (data) {
                    console.log(data);
                    var str = $.trim(data);
                    if (str === "success")
                    {

                        document.getElementById("promo10").style.display = "block";
                        document.getElementById("promo25").style.display = "none";
                        document.getElementById("promo_sucess").style.display = "block";
                        document.getElementById("promo_error").style.display = "none";
                        document.getElementById("PFee10").style.display = "block";
                        document.getElementById("PFee25").style.display = "none";
                        document.getElementById("apply").style.display = "none";
                        document.getElementById("check").style.display = "none";
                        document.getElementById("txtpromocode").disabled = "disabled"





                    } else
                    {
                        document.getElementById("promo10").style.display = "none";
                        document.getElementById("promo25").style.display = "block";
                        document.getElementById("promo_sucess").style.display = "none";
                        document.getElementById("promo_error").style.display = "block";
                        document.getElementById("PFee10").style.display = "none";
                        document.getElementById("PFee25").style.display = "block";
                        document.getElementById("apply").style.display = "block";
                        document.getElementById("check").style.display = "block";


                    }
                }
            });
        });
    </script>
</body>
</html>
<?php
$recordAdded = false;
$recordNOTAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
} elseif (isset($_GET['status']) && $_GET['status'] == 2) {
    $recordNOTAdded = true;
}

if ($recordAdded) {
    echo '
<script type="text/javascript">
     
    document.getElementById("frm_update").style.display = "none";
    document.getElementById("popup3").style.display = "block";
  
</script>';
}
if ($recordNOTAdded) {
    echo '
<script type="text/javascript">
     
    document.getElementById("RegComp_error").style.display = "block";
   
  
</script>';
}
?>