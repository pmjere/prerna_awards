 

<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 
                                    <li><a onclick="window.location.href = 'index.php'">Home</a></li>
                                         <li><a onclick="window.location.href = 'cont_Login.php'">Login</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->




        <!-- Services
        ================================================== -->
        <section id="registration">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading"></span>
                    Forgot Your Password?
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6" style="padding-top: 10px;padding-left: 40px">
                            <div class="text-center">
                                <img src="images/what-we-do/lo.png" alt="">

                            </div>
                        </div>
                        <div class="col-md-6"> 

                            <form id="main_forgot_form" name="main_forgot_form"  method="post" autocomplete="off"  enctype="multipart/form-data"class="nino-subscribeForm" onsubmit="return false;">
                                <?php
                                if (isset($errMSG)) {
                                    ?>

                                    <div class="form-group"> 
                                        <div style="color: red">
                                            <i><lable>Incorrect UserName or Password</lable></i>
                                        </div> 
                                    </div>
                                    <div class="form-group"  style="width: 50%">
                                        <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                            <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                To reset your password, please enter your Email Id.<br> 
                                Well will send the password reset instructions to the email address for this account. <br><br>

                                <div class="form-group" id="Forgot_error" style="display: none"> 
                                    <div style="color: red">
                                        <i><lable>We are anable to access your information.<br/>Please try again.</lable></i>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user text-success"  style="color: #172f6a" ></span></span>
                                        <input type="email" name="Forgot_email" id="Forgot_email" class="form-control" placeholder="Email"  required/>
                                    </div>
                                </div>     
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success" name="ForgottBtn" id="ForgottBtn">Submit </button>
                                </div>
                            </form>
                             <div id="forgotpass_done"style="display: none">
                                <b style="font-size: 15px; text-align: center"></b> <br>
                                <div class="content" style="text-align: center">                               
                                    <br><b style="font-size: 15px; text-align: center">Thank you ! Please check your email account.</b>
                                    <form action="cont_Login.php">
                                        <br><br><button type="submit" class="btn btn-block btn-success">Back </button>
                                    </form>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div id="popup">
                <Center>
                    Record added successfully 
                        </Center>
                        </div>-->
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

            <script type="text/javascript" src="js_1/tos.js"></script>
            <script src="js_1/moment.js" type="text/javascript"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
        </section><!--/#nino-services-->
        <footer id="footer">
            <div class="nino-copyright">
                <div class="row">
                    <div class="colInfo">
                        <div class="col-md-1"></div>
                        <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                            <ul class="nav navbar-nav">
                                <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                            <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3" style="padding-left: 30px">
                            <div class="nino-followUs">
                                <div class="socialNetwork">
                                    <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                    <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
        </div>
    </footer>

    <!-- Search Form - Display when click magnify icon in menu
    ================================================== -->
    <form action="" id="nino-searchForm">
        <input type="text" placeholder="Search..." class="form-control nino-searchInput">
        <i class="mdi mdi-close nino-close"></i>
    </form><!--/#nino-searchForm-->

    <!-- Scroll to top
    ================================================== -->
    <a href="#" id="nino-scrollToTop">Go to Top</a>

    <!-- javascript -->
    <script type="text/javascript" src="js/jquery.min.js"></script>	
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/unslider-min.js"></script>
    <script type="text/javascript" src="js/template.js"></script>
    <script src="js_1/moment.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="js/Forgot_pass.js" type="text/javascript"></script>


</body>
</html>