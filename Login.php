<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Login</title>
    <link rel="stylesheet" href="css_1/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="css_1/style.css" type="text/css"/>
    <link href="css_1/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap.icon-large.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div id="login-form">
            <form method="post" autocomplete="off" action="User_login.php" enctype="multipart/form-data">
                <div class="col-md-8">
                    <row>
                        <div class="form-group">
                            <center>
                                <h2 class="">Login</h2>
                                <?php
                                if (isset($_GET['flag']) && $_GET['flag'] == 1) {
                                    echo '<div style="color: red">
                                    <i><lable>Incorrect UserName or Password</lable></i>
                                </div>';                                  
                                }                               
                                ?>                              
                            </center>
                        </div>
                        <div class="form-group">
                            <hr/>
                        </div>
                        <?php
                        if (isset($errMSG)) {
                            ?>
                            <div class="form-group">
                                <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                    <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <div class="form-group">

                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user" ></span></span>
                                <input type="text" name="uname" id="uname" class="form-control" placeholder="UserName"  required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock" ></span></span>
                                <input type="password" name="Password" id="Password" class="form-control" placeholder="Password"  required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn    btn-block btn-primary" name="signup" id="signup">Register</button>
                        </div>
                        <div class="form-group">
                            <hr/>
                        </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/tos.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
</body>
</html>
<?php
$recordAdded = false;

if ($recordAdded) {
    echo '
<script type="text/javascript">
    function hideMsg()
    {
        document.getElementById("popup").style.visibility = "hidden";
    }

    document.getElementById("popup").style.visibility = "visible";
    window.setTimeout("hideMsg()", 2000);
</script>';
}
?>