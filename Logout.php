<?php  
/** 
 * Created by PhpStorm. 
 * User: Ehtesham Mehmood 
 * Date: 11/21/2014 
 * Time: 2:46 AM 
 */  
include_once './dbconnect.php';
session_start();
$link = DbConnect::GetConnection();
  if(!empty($_POST["logoutPage"])) {
	$_SESSION["id"] = "";
	session_destroy();
}


header("Location: login.php");//use for the redirection to some page  
?>  

