 

<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="homepage.html">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 

                                    <li><a onclick="window.location.href = 'admindashboard.php'">Back</a></li>
                                    <li><a onclick="window.location.href = 'cont_Login.php'">Logout</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>

            </div>
        </header><!--/#header-->




        <!-- Services
        ================================================== -->
        <section id="registration">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">For all users</span>
                    Create New User
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6" style="padding-top: 10px;padding-left: 40px">
                            <div class="text-center">
                                <img src="images/what-we-do/Users-icon.png" alt="">

                            </div>
                        </div>
                        <div class="col-md-6"> 
                            <form method="post" autocomplete="off" action="insert_newUser.php" enctype="multipart/form-data"id="freg_id22" name="freg_id22" >

                                <div class="row">

                                    <?php
                                    if (isset($errMSG)) {
                                        ?>
                                        <div class="form-group">
                                            <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                                <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <lable id="name_error1" style="color: Red; display: none">Please Enter Name</lable>
                                    <div class="form-group">
                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user  text-success"  style="color: #182441" ></span></span>
                                            <input type="text" name="Fname" id="Fname" class="form-control" placeholder="Enter Your Name"  required/>
                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                            <script type="text/javascript">
                                        $(function () {
                                            $("#Fname").blur(function () {
                                                if ($(this).val() == "") {
                                                    document.getElementById("name_error1").style.display = 'block';
                                                    $("#Fname").focus();
                                                } else
                                                {
                                                    document.getElementById("name_error1").style.display = 'none';
                                                }
                                            });
                                        });
                                            </script>
                                        </div>
                                    </div>
                                    <lable id="Email_error" style="color: Red; display: none">Enter Email Id</lable>
                                    <lable id="EmailValid_error" style="color: Red; display: none">Please enter valid Email</lable>
                                    <lable id="PresentEmail_error" style="color: Red; display: none">This Email Id is already present</lable>

                                    <div class="form-group">
                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-envelope  text-success"  style="color: #182441"></span></span>
                                            <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" required/>
                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                            <script type="text/javascript">
                                        $("#email").blur(function () {

                                            $('#email').filter(function () {
                                                var emil = $('#email').val();
                                                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;


                                                if (!emailReg.test(emil)) {
                                                    document.getElementById("Email_error").style.display = 'block';
                                                    $("#Email_error").text("Please enter valid email");
                                                    $("#email").focus();

                                                } else if ($('#email').val() == "")
                                                {
                                                    document.getElementById("Email_error").style.display = 'block';
                                                    $("#Email_error").text("Enter Email Id");
                                                    $("#email").focus();
                                                } else
                                                {
                                                    document.getElementById("Email_error").style.display = 'none';
                                                }
                                            });
                                        });

                                            </script>
                                        </div>
                                    </div>
                                    <lable id="name_error3" style="color: Red; display: none">Please Enter UserName</lable>
                                    <lable id="uname_error" style="color: Red; display: none">Please Enter UserName</lable>
                                    <div class="form-group">
                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user  text-success"  style="color: #182441" ></span></span>
                                            <input type="text" name="uname" id="uname" class="form-control" placeholder="Username"  required/>
                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                            <script type="text/javascript">
                                        $(function () {
                                            $("#uname").blur(function () {
                                                if ($(this).val() == "") {
                                                    document.getElementById("name_error3").style.display = 'block';
                                                    $("#uname").focus();
                                                } else
                                                {
                                                    document.getElementById("name_error3").style.display = 'none';
                                                }
                                            });
                                        });
                                            </script>
                                        </div>
                                    </div>
                                    <lable id="name_error4" style="color: Red; display: none">Please Enter Password</lable>
                                    <div class="form-group">
                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-lock  text-success"  style="color: #182441" ></span></span>
                                            <input type="password" name="Password" id="Password" class="form-control" placeholder="Password"  required/>
                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                            <script type="text/javascript">
                                        $(function () {
                                            $("#Password").blur(function () {
                                                if ($(this).val() == "") {
                                                    document.getElementById("name_error3").style.display = 'block';
                                                    $("#Password").focus();
                                                } else
                                                {
                                                    document.getElementById("name_error3").style.display = 'none';
                                                }
                                            });
                                        });
                                            </script>

                                        </div>
                                    </div>
                                    <lable id="name_error5" style="color: Red; display: none">Please Select User Type</lable>
                                    <div class="form-group">
                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user  text-success"  style="color: #182441" ></span></span>
                                            <select class = "form-control" name="User_type" id="User_type" required>
                                                <option value="">Select User Type</option>
                                                <option value="Admin">Admin</option>
                                                <option value="judge">Judge</option>
                                            </select>
                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                            <script type="text/javascript">
                                        $(function () {
                                            $("#User_type").blur(function () {
                                                if ($(this).val() == "") {
                                                    document.getElementById("name_error5").style.display = 'block';
                                                    $("#User_type").focus();
                                                } else
                                                {
                                                    document.getElementById("name_error5").style.display = 'none';
                                                }
                                            });
                                            $("#User_type").change(function () {
                                                if ($(this).val() == "") {
                                                    document.getElementById("name_error5").style.display = 'block';
                                                    $("#User_type").focus();
                                                } else
                                                {
                                                    document.getElementById("name_error5").style.display = 'none';
                                                }
                                            });
                                        });
                                            </script>
                                        </div>
                                    </div>
                                    <!--                                    <div class="form-group" style="display:none" id="judge_lang" name="judge_lang">
                                                                            <div class="input-group input-group-lg">
                                                                                <span class="input-group-addon"><span class="fa fa-american-sign-language-interpreting  text-success"  style="color: #182441"></span></span>
                                    <?php
                                    $link = DbConnect::GetConnection();
                                    if ($link) {
                                        $sql = "SELECT lang_id, language FROM language";
                                        $result = $link->query($sql);
                                        $select = '<select name="Language" id="Language"  class="form-control" required>';
                                        $select .= '<option value="">Select Language</option>';
                                        while ($row = $result->fetch_assoc()) {
                                            $select .= '<option value="' . $row['lang_id'] . '">' . $row['language'] . '</option>';
                                        }
                                        $select .= '</select>';
                                        echo $select;
                                    }
                                    ?>
                                                                            </div>
                                                                        </div>
                                    
                                    
                                                                        <lable id="AgeCategory_error" style="color: Red; display: none">Please Select Age Category</lable>
                                                                        <div class="form-group" id="judge_age_category" name="judge_age_category" style="display:none">                    
                                                                            <div class='input-group input-group-lg'>
                                                                                <span class="input-group-addon">
                                                                                    <span class="glyphicon glyphicon-calendar"  style="color: #182441"></span>
                                                                                </span>
                                                                                <select  class = "form-control" name="judge_age" id="judge_age">
                                                                                    <option value="">Select Age Category </option>
                                                                                    <option value="Child"> 8 - 12 Yrs</option>
                                                                                    <option value="Youth"> 13 -16 Yrs</option>
                                                                                    <option value="Open">17 Yrs & Above </option>    
                                                                                </select>
                                    
                                                                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                                                                <script type="text/javascript">
                                                                            $("#age").blur(function () {
                                                                                if ($(this).val() == "") {
                                                                                    document.getElementById("AgeCategory_error").style.display = 'block';
                                                                                    $("#age").focus();
                                                                                } else
                                                                                {
                                                                                    document.getElementById("AgeCategory_error").style.display = 'none';
                                                                                }
                                                                            });
                                                                                </script>
                                    
                                                                            </div>                    
                                                                        </div>-->
                                    <!--       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        -->
                                    <lable id="Language_checkError" style="color: Red; display: none">Please Select language </lable>
                                
                                    <div class="form-group" style="display: none" id="judge_lang" name="judge_lang">
                                        <div class="input-group input-group-lg" style="font-size: 15px">
    <lable  class="input-group input-group-lg"   style=" font-size: 17px;">Select Language : </lable> <br>
                                            <input type="checkbox" name="Hindi" id="Hindi"/> Hindi &#8194;&#8194;&#8194;&#8194;
                                            <input type="checkbox" name="Tamil" id="Tamil"/> Tamil &#8194;&#8194;&#8194;&#8194;
                                            <input type="checkbox" name="Bangla" id="Bangla"/> Bangla  &#8194;&#8194;&#8194;&#8194;
                                            <input type="checkbox" name="Gujarati" id="Gujarati"/> Gujarati

                                        </div>
                                    </div>
                                    <lable id="age_categoryError" style="color: Red; display: none">Please Select age category </lable>
                                
                                    <div class="form-group" style="display: none" id="judge_age_category" name="judge_age_category">
                                        <div class="input-group input-group-lg" style="font-size: 15px">
                                            <lable  class="input-group input-group-lg"   style=" font-size: 17px;">Select Age Category : </lable> <br>
                                            <input type="checkbox" name="child" id="child"/> 8 - 12 Yrs &#8194;&#8194;&#8194;&#8194;
                                            <input type="checkbox" name="youth" id="youth"/> 13 -16 Yrs &#8194;&#8194;&#8194;&#8194;
                                            <input type="checkbox" name="open" id="open" />17 Yrs & Above 
                                        </div>
                                    </div>

                                    <!--       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                         -->


                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success" name="signup" id="signup">Create </button>
                                    </div>
                                    </div>
                            </form>


                            <div id="popup11"style="display: none">
                                <h2 style="text-align: center">Thank you !</h2> <br>
                                <div class="content" style="text-align: center">                               
                                    <br><br>The new Judge is created successfully. 
                                    <form action="admindashboard.php">
                                        <br><br><button type="submit" class="btn btn-block btn-success">Back </button>
                                    </form>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--            <div id="popup">
            <Center>
                Record added successfully 
                    </Center>
                    </div>-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js_1/tos.js"></script>
        <script src="js_1/moment.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>



    </section><!--/#nino-services-->





    <!-- javascript -->
    <script type="text/javascript" src="js/jquery.min.js"></script>	
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/unslider-min.js"></script>
    <script type="text/javascript" src="js/template.js"></script>
    <script src="js_1/moment.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script>
                                                $(function () {
                                                    $("#User_type").change(function () {
                                                        var u_type = $("#User_type").val();
                                                        if (u_type === "judge")
                                                        {
                                                            document.getElementById('judge_lang').style.display = 'block';
                                                            document.getElementById('judge_age_category').style.display = 'block';

                                                        } else
                                                        {
                                                            document.getElementById('judge_lang').style.display = 'none';
                                                            document.getElementById('judge_age_category').style.display = 'none';

                                                        }

                                                    });
                                                });
    </script>
    <script type="text/javascript">
        $("#signup").click(function () {
            if ($("#Fname").val() == "") {
                document.getElementById("name_error1").style.display = 'block';
                $("#Fname").focus();
                return false;
            } else
            {
                document.getElementById("name_error1").style.display = 'none';
            }

            var emil = $('#email').val();
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if (!emailReg.test(emil)) {
                document.getElementById("Email_error").style.display = 'block';
                $("#Email_error").text("Please enter valid email");
                $("#email").focus();
            } else if ($('#email').val() == "")
            {
                document.getElementById("Email_error").style.display = 'block';
                $("#Email_error").text("Enter Email Id");
                $("#email").focus();
            }

            $.ajax({
                url: 'CheckUsername.php',
                type: "POST",
                data: {Email: $("#email").val(), UserName: $("#uname").val()},
                async: false,
                success: function (data) {
                    console.log(data);
                    var str = $.trim(data);
                    if (str === "success")
                    {
                        if ($("#uname").val() == "") {
                            document.getElementById("name_error3").style.display = 'block';
                            $("#uname").focus();
                            return false;
                        } else
                        {
                            document.getElementById("name_error3").style.display = 'none';
                        }
                        if ($("#Password").val() == "") {
                            document.getElementById("name_error4").style.display = 'block';
                            $("#Password").focus();
                            return false;
                        } else
                        {
                            document.getElementById("name_error4").style.display = 'none';
                        }
                        if ($("#User_type").val() == "") {
                            document.getElementById("name_error5").style.display = 'block';
                            $("#User_type").focus();
                            return false;
                        } else
                        {
                            document.getElementById("name_error5").style.display = 'none';
                        }


                        document.getElementById("Email_error").style.display = 'none';
                        document.getElementById('uname_error').style.display = 'none';
                        $("#freg_id22").submit();


                    } else
                    {
                        document.getElementById('PresentEmail_error').style.display = 'block';
                        document.getElementById('uname_error').style.display = 'block';
                        return false;
                    }

                }
            }
            );
            return false;
        }
        );
    </script>

    <footer id="footer">
        <div class="nino-copyright">
            <div class="row">
                <div class="colInfo">
                    <div class="col-md-1"></div>
                    <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                        <ul class="nav navbar-nav">
                            <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                            <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3" style="padding-left: 30px">
                        <div class="nino-followUs">
                            <div class="socialNetwork">
                                <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
<!--                                <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                                            <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>-->
<!--                                                            <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>-->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
    </div>
</footer>
</body>
</html>
<?php
$recordAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
}

if ($recordAdded) {
    echo '
<script type="text/javascript">
     
    document.getElementById("freg_id22").style.display = "none";
    document.getElementById("popup11").style.display = "block";
  
</script>';
}
?>