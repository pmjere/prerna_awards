 

<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 
                                        <li><a onclick="window.location.href = 'index.php'">Home</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->




        <!-- Services
        ================================================== -->
         <div class="col-md-6" style="padding-top: 40px">
                                <div class="text-center">
                                    <img src="images/what-we-do/New_Logo.png" alt="">
                                </div>
                            </div>
    <section1 id="registration">
        <div class="container">
            <div class="sectionContent" style="text-align: justify">

                <div>
                    <row> 
                        <h4 class="nino-sectionHeading1">

                          PRERNA AWARDS 2018 
                        </h4>
                        <h2 class="nino-sectionHeading2">

                             Privacy Policy
                        </h2>
                        
                         
                
                    </row>
                        
                        
                  
                    </row>
                </div>
                <p style="padding-bottom: 10px">
                    This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.
                </p>




                <p style="color: black"><b>What personal information do we collect from the people that visit our blog, website or app?</b></p>

                When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address or other details to help you with your experience.<br><br>



                <p style="color: black"><b>When do we collect information?</b></p> 
                We collect information from you when you register on our site, subscribe to a newsletter, fill out a form or enter information on our site.<br><br><br>
      

                <p style="color: black"><b>How do we use your information?</b></p>

                We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:<br>

                • To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.<br>


                • To allow us to better service you in responding to your customer service requests.<br>

                • To send periodic emails regarding your order or other products and services.<br>

                • To follow up with them after correspondence (live chat, email or phone inquiries)<br><br>


                <p style="color: black"><b>How do we protect your information? </b></p>
             We do not use vulnerability scanning and/or scanning to PCI standards.<br>
             We only provide articles and information. We never ask for credit card numbers.<br>
             We use regular Malware Scanning.<br><br> 
             We do not use an SSL certificate<br>
             <p style="padding-left: 30px"> • We only provide articles and information. We never ask for personal or private information like names, email addresses, or credit card numbers.</p>
             <p style="color: black"><b> Do we use 'cookies'?</b></p> 

                We do not use cookies for tracking purposes
                <br><br>

               You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.
               <br><br>

               If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.that make your site experience more efficient and may not function properly.
               <br><br>

                  <p style="color: black"><b>Third-party disclosure</b></p>
                  We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information.<br><br>
                  
                    <p style="color: black"><b>Third-party links</b></p>
                  We do not include or offer third-party products or services on our website.<br><br>
                  
                  <p style="color: black"><b>Google</b></p>
                  Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en <br>
                  We have not enabled Google AdSense on our site but we may do so in the future.<br><br>
                  
                  <p style="color: black"><b>California Online Privacy Protection Act</b></p>
                
CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf<br><br>
                  
                  
                  <p style="color: black"><b>According to CalOPPA, we agree to the following:</b></p>
                 Users can visit our site anonymously.<br>
                Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.<br>
                Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.<br><br>
                   
                   You will be notified of any Privacy Policy changes:<br>
                   <p style="padding-left: 30px">  • On our Privacy Policy Page</p><br>
                         Can change your personal information:<br>
                         <p style="padding-left: 30px"> • By logging in to your account</p><br>
                  
                  
                  <p style="color: black"><b>How does our site handle Do Not Track signals?</b></p>
                We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.<br><br>
                  
                  
                  <p style="color: black"><b>Does our site allow third-party behavioral tracking?</b></p>
               It's also important to note that we do not allow third-party behavioral tracking<br><br>
                  
                  <p style="color: black"><b>COPPA (Children Online Privacy Protection Act)</b></p>
                 When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.<br>
                 We do not specifically market to children under the age of 13 years old.<br><br>
                  
                 <p style="color: black"><b>Fair Information Practices</b></p>
             The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.<br><br>
               
               <p style="color: black"><b>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</b></p>
               We will notify you via email<br> 
                 <p style="padding-left: 30px">  • Within 7 business days</p><br>
                  WWe also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.<br><br>
               
               <p style="color: black"><b>CAN SPAM Act</b></p>
              The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.<br><br>
               
               <p style="color: black"><b>We collect your email address in order to:</b></p>
             <p style="padding-left: 30px">  • Send information, respond to inquiries, and/or other requests or questions</p><br>
               <p style="padding-left: 30px">    • Process orders and to send information and updates pertaining to orders.</p><br>
                 <p style="padding-left: 30px">   • Send you additional information related to your product and/or service</p><br>
               
                
                 
                 <p style="color: black"<b>>To be in accordance with CANSPAM, we agree to the following:</b></p>
             <p style="padding-left: 30px">    • Not use false or misleading subjects or email addresses.</p><br>
               <p style="padding-left: 30px">     • Identify the message as an advertisement in some reasonable way.</p><br>
                 <p style="padding-left: 30px">    • Include the physical address of our business or site headquarters.</p><br>
                   <p style="padding-left: 30px">  • Monitor third-party email marketing services for compliance, if one is used.</p><br>
               <p style="padding-left: 30px">      • Honor opt-out/unsubscribe requests quickly.</p><br>
                 <p style="padding-left: 30px">      • Allow users to unsubscribe by using the link at the bottom of each email.</p><br>
                 
                 
                 
            </div>
        </div>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js_1/tos.js"></script>
        <script src="js_1/moment.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>



    </section1><!--/#nino-services-->
  <footer id="footer">
            <div class="nino-copyright">
                <div class="row">
                    <div class="colInfo">
                        <div class="col-md-1"></div>
                        <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                            <ul class="nav navbar-nav">
                                <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                       <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                               
                            </ul>
                        </div>
                        <div class="col-md-3" style="padding-left: 30px">
                            <div class="nino-followUs">
                                <div class="socialNetwork">
                                    <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                    <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
        </div>
    </footer>




    <!-- javascript -->
    <script type="text/javascript" src="js/jquery.min.js"></script>	
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/unslider-min.js"></script>
    <script type="text/javascript" src="js/template.js"></script>
    <script src="js_1/moment.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script>
        $(function () {
            $("#User_type").change(function () {
                var u_type = $("#User_type").val();
                if (u_type == "judge")
                {
                    document.getElementById('judge_lang').style.display = 'block';
                    document.getElementById('judge_comp').style.display = 'block';
                } else
                {
                    document.getElementById('judge_lang').style.display = 'none';
                    document.getElementById('judge_comp').style.display = 'none';
                }

            });
        });
    </script>

</body>
</html>
<?php
$recordAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
}

if ($recordAdded) {
    echo '
<script type="text/javascript">
    function hideMsg()
    {
        document.getElementById("popup").style.visibility = "hidden";
    }

    document.getElementById("popup").style.visibility = "visible";
    window.setTimeout("hideMsg()", 2000);
</script>';
}
?>

