<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Prerna Awards</title>
        <link href="css/Table.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!--        <style type="text/css">
    body {
        color: #566787;
        background: #f5f5f5;
        font-family: 'Varela Round', sans-serif;
        font-size: 13px;
    }
    .table-wrapper {
        background: #fff;
        padding: 20px 25px;
        margin: 30px 0;
        border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }
    .table-title {        
        padding-bottom: 15px;
        background: #435d7d;
        color: #fff;
        padding: 16px 30px;
        margin: -20px -25px 10px;
        border-radius: 3px 3px 0 0;
    }
    .table-title h2 {
        margin: 5px 0 0;
        font-size: 24px;
    }
    .table-title .btn-group {
        float: right;
    }
    .table-title .btn {
        color: #fff;
        float: right;
        font-size: 13px;
        border: none;
        min-width: 50px;
        border-radius: 2px;
        border: none;
        outline: none !important;
        margin-left: 10px;
    }
    .table-title .btn i {
        float: left;
        font-size: 21px;
        margin-right: 5px;
    }
    .table-title .btn span {
        float: left;
        margin-top: 2px;
    }
    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
        padding: 12px 15px;
        vertical-align: middle;
    }
    table.table tr th:first-child {
        width: 60px;
    }
    table.table tr th:last-child {
        width: 100px;
    }
    table.table-striped tbody tr:nth-of-type(odd) {
        background-color: #fcfcfc;
    }
    table.table-striped.table-hover tbody tr:hover {
        background: #f5f5f5;
    }
    table.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }	
    table.table td:last-child i {
        opacity: 0.9;
        font-size: 22px;
        margin: 0 5px;
    }
    table.table td a {
        font-weight: bold;
        color: #566787;
        display: inline-block;
        text-decoration: none;
        outline: none !important;
    }
    table.table td a:hover {
        color: #2196F3;
    }
    table.table td a.edit {
        color: #FFC107;
    }
    table.table td a.delete {
        color: #F44336;
    }
    table.table td i {
        font-size: 19px;
    }
    table.table .avatar {
        border-radius: 50%;
        vertical-align: middle;
        margin-right: 10px;
    }
    .pagination {
        float: right;
        margin: 0 0 5px;
    }
    .pagination li a {
        border: none;
        font-size: 13px;
        min-width: 30px;
        min-height: 30px;
        color: #999;
        margin: 0 2px;
        line-height: 30px;
        border-radius: 2px !important;
        text-align: center;
        padding: 0 6px;
    }
    .pagination li a:hover {
        color: #666;
    }	
    .pagination li.active a, .pagination li.active a.page-link {
        background: #03A9F4;
    }
    .pagination li.active a:hover {        
        background: #0397d6;
    }
    .pagination li.disabled i {
        color: #ccc;
    }
    .pagination li i {
        font-size: 16px;
        padding-top: 6px
    }
    .hint-text {
        float: left;
        margin-top: 10px;
        font-size: 13px;
    }    
    /* Custom checkbox */
    .custom-checkbox {
        position: relative;
    }
    .custom-checkbox input[type="checkbox"] {    
        opacity: 0;
        position: absolute;
        margin: 5px 0 0 3px;
        z-index: 9;
    }
    .custom-checkbox label:before{
        width: 18px;
        height: 18px;
    }
    .custom-checkbox label:before {
        content: '';
        margin-right: 10px;
        display: inline-block;
        vertical-align: text-top;
        background: white;
        border: 1px solid #bbb;
        border-radius: 2px;
        box-sizing: border-box;
        z-index: 2;
    }
    .custom-checkbox input[type="checkbox"]:checked + label:after {
        content: '';
        position: absolute;
        left: 6px;
        top: 3px;
        width: 6px;
        height: 11px;
        border: solid #000;
        border-width: 0 3px 3px 0;
        transform: inherit;
        z-index: 3;
        transform: rotateZ(45deg);
    }
    .custom-checkbox input[type="checkbox"]:checked + label:before {
        border-color: #03A9F4;
        background: #03A9F4;
    }
    .custom-checkbox input[type="checkbox"]:checked + label:after {
        border-color: #fff;
    }
    .custom-checkbox input[type="checkbox"]:disabled + label:before {
        color: #b8b8b8;
        cursor: auto;
        box-shadow: none;
        background: #ddd;
    }
    /* Modal styles */
    .modal .modal-dialog {
        max-width: 400px;
    }
    .modal .modal-header, .modal .modal-body, .modal .modal-footer {
        padding: 20px 30px;
    }
    .modal .modal-content {
        border-radius: 3px;
    }
    .modal .modal-footer {
        background: #ecf0f1;
        border-radius: 0 0 3px 3px;
    }
    .modal .modal-title {
        display: inline-block;
    }
    .modal .form-control {
        border-radius: 2px;
        box-shadow: none;
        border-color: #dddddd;
    }
    .modal textarea.form-control {
        resize: vertical;
    }
    .modal .btn {
        border-radius: 2px;
        min-width: 100px;
    }	
    .modal form label {
        font-weight: normal;
    }	
    #popup {
        visibility: hidden; 
        position: absolute;
        top: 10px;
        z-index: 100; 
        height: 100px;
        width: 300px
    }
</style>-->
        <script type="text/javascript">
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function () {
                    if (this.checked) {
                        checkbox.each(function () {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function () {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function () {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
        </script>
    </head>
    <body>
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="homepage.html">Prerna Awards</a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 

                                    <li><a onclick="window.location.href = 'admin_password.php'">Change Password</a></li>
                                    <li><a onclick="window.location.href = 'cont_Login.php'">Logout</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->
    <section1 id="registration"> 
        <div class="container">
            <div class="sectionContent">
                   <div class="table-responsive">
                <h2 class="nino-sectionHeading">

                    Registration List
                </h2>              
                <table class="table table-striped table-hover table-responsive" id="orders-table" data-search="false" data-striped="true" data-pagination="true" data-filter-control="true" data-side-pagination="client" data-page-size="10" data-page-list="[10, 25, 50, 100, ALL]">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Contact</th>
                            <th>Email</th>

                            <th data-field="Language" data-filter-control="select" >Language<br></th>
                            <th data-field="AgeCategory" data-filter-control="select">Age Category</th>
                            <th>Country</th>
                            <th data-field="Marks" data-filter-control="select">Marks</th>

<!--                         <th style="font-size: 20px;color: black">Name</th>  
<th style="font-size: 20px;color: black">Contact</th> 
<th style="font-size: 20px;color: black">Email</th>  
<th style="font-size: 20px;color: black">Language</th>
<th style="font-size: 20px;color: black">Age Category</th>
<th style="font-size: 20px;color: black">Country</th>  -->
                            <th style="font-size: 20px;color: black">Edit</th>  
                        </tr>
                    </thead>
                    <tbody style="font-size: 15px;color: black">
                        <?php
                        $link = DbConnect::GetConnection();
                        if ($link) {

                            if (isset($_GET["flag"])) {
                                $falg_lang = $_GET["flag"];

                                $sql = " SELECT * FROM    contestant_registration r 
 INNER JOIN participation p on r.Contestant_id=p.pCont_id
 INNER join language l on p.pLanguage_id=l.lang_id 
 INNER join competition c on p.pCompetition_id=c.com_id 
 Inner join judge j on j.category_id= r.age_category and j.language_id = p.pLanguage_id
 where j.judge_id='$falg_lang' and r.Payment=true and r.is_reg=true;";
                            } else {
                                $sql = " SELECT * FROM    contestant_registration r  INNER JOIN participation p on r.Contestant_id=p.pCont_id INNER join language l on p.pLanguage_id=l.lang_id INNER join competition c on p.pCompetition_id=c.com_id where Payment=true and r.is_reg=true";
                            }
                            $result = $link->query($sql);
                            $record = '';
                            while ($row = $result->fetch_assoc()) {
                                $l = $row["pLanguage_id"];
                                $c1 = $row["pCompetition_id"];
                                $reg_id = $row["reg_id"];
                                $age_category = $row["age_category"];
                                if ($age_category === '1') {
                                    $category = 'Child : 8-12';
                                }
                                if ($age_category === '2') {
                                    $category = 'Youth :13-17';
                                }
                                if ($age_category === '3') {
                                    $category = 'Adults : 18 & above';
                                }
                                $judge_id = $_SESSION["usre_id"];

                                $sql1 = "SELECT COUNT(judge_id) as count FROM marks WHERE language_id=$l and competition_id=$c1  and Mreg_id=$reg_id";
                                $result1 = mysqli_query($link, $sql1);
                                $row1 = $result1->fetch_assoc();

                                $type = $row1["count"];
                                $sql2 = "SELECT marks FROM marks WHERE language_id=$l and competition_id=$c1 and judge_id=$judge_id  and Mreg_id=$reg_id";
                                $result2 = mysqli_query($link, $sql2);
                                $row2 = $result2->fetch_assoc();
                                if ($row2 == 0) {
                                    $row2["marks"] = 0;
                                } else {
                                    
                                }
                                $record .= '<tr><td>' . $row["name"] . '</td><td>' . $row["contact_no"] . '</td><td>' . $row["email"] . '</td><td>' . $row["language"] . '</td><td>' . $category . '</td> <td>' . $row["country"] . '</td> <td>' . $row2["marks"] . '</td> <td>
                                   <a href="" onclick="RegDetails(' . $row["reg_id"] . ',\'' . $row["name"] . '\',\'' . $row["contact_no"] . '\',\'' . $row["email"] . '\',\'' . $row["age_category"] . '\',\'' . $row["address"] . '\',\'' . $row["DOB"] . '\',\'' . $row["language"] . '\',\'' . $row["pLanguage_id"] . '\',\'' . $row["competition"] . '\',\'' . $row["pCompetition_id"] . '\',\'' . $row["country"] . '\',\'' . $row["reference"] . '\',\'' . $row["File_Name"] . '\',\'' . $row1["count"] . '\')" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Details">&#xE254;</i></a>                               
                            </td></tr>';
                            }
                            echo $record;
                        }
                        ?>
                    </tbody>
                </table>
                   </div>
<!--                <div class="clearfix">
                    <ul class="pagination">
                        <li class="page-item disabled"><a href="#">Previous</a></li>
                        <li class="page-item"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">Next</a></li>
                    </ul>
                </div>-->
            </div>
        </div>
    </section1>
    <!-- Edit Modal HTML -->
    <div id="RegDetailModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="insert_marks.php" method="post">
                    <div class="modal-header">						
                        <h4 class="modal-title">Registration Details</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <input type="text" id="r_id" name="r_id" hidden> 
                    <div class="modal-body">	
                        <div class="form-group">
                            <label style="color: red">Name : -</label></b>
                            <label id="name" ></label>
                        </div>	
                        <div class="form-group">
                            <label style="color: red">Contact No : -</label>
                            <label id="contact">Contact No : -</label>
                        </div>	
                        <div class="form-group">
                            <label style="color: red">Email ID : -</label>
                            <label id="email">Email ID : -</label>
                        </div>	
                        <div class="form-group">
                            <label style="color: red">Address : -</label>
                            <label id="address">Address : -</label>
                        </div>	
                        <div class="form-group">
                            <label style="color: red">Date Of Birth : -</label>
                            <label id="dob">Date Of Birth : -</label>
                        </div>	
                        <div class="form-group">
                            <label id style="color: red">Language : -</label>
                            <label id="language">Language : -</label>
                            <input type="text" id="language_id" name="language_id" hidden> 
                        </div>	
                        <div class="form-group">
                            <label style="color: red">Competition : -</label>
                            <label id="Competition">Competition : -</label>
                            <input type="text" id="Competition_id" name="Competition_id" hidden> 
                        </div>
                        <div class="form-group">
                            <label style="color: red">Country : -</label>
                            <label id="Country">Country : -</label>
                        </div> 
                        <div class="form-group">
                            <label style="color: red">Reference : -</label>
                            <label id="ref">Reference : -</label>
                        </div>
                        <div class="form-group Mark_count" style="display:none" id="J_marks">
                            <div class="input-group">
                                <label style="color: red">Enter Marks : -</label>
                                <input type="number" name="marks" id="marks" required/>
                            </div>
                        </div>
                        <div class="form-group Mark_count" style="display:none;" id="J_comment">
                            <div class="input-group">
                                <label style="color: red">Enter Comment : -</label>
                                <textarea name="comment" id="comment" required /></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label style="color: red">Uploaded File : -</label>
                            <label id="file">Uploaded File : -</label>  
                            <span><a href="" id="File1" name="File1" style="color:blue">[ DOWNLOAD ]</a></span>
                        </div>
                        <input type="text" id ="cat" name="cat"style="display: none"/>
                        <lable id="Mark_error" style="color: #F14F72; display: none;font-size: 15px">All three Judges are assigned their  marks</lable>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-success" value="Add" id="add" name="add">
                    </div> 
                </form>
            </div>
        </div>
    </div>
    <script src="js/tableFilter.js" type="text/javascript"></script>
    <script>
        function RegDetails(rid, name, contact, email, cat, add, dob, lang, lang_id, comp, comp_id, country, ref, file, count)
        {
            $("#r_id").val(rid);
            $("#name").text(name);
            $("#contact").text(contact);
            $("#email").text(email);
            $("#cat").val(cat);

            $("#address").text(add);
            $("#dob").text(dob);
            $("#language").text(lang);
            $("#language_id").val(lang_id);
            $("#Competition").text(comp);
            $("#Competition_id").val(comp_id);
            $("#Country").text(country);
            $("#ref").text(ref);
            $("#file").text(file);
            $("#File1").attr("href", "download.php?file=" + $("#file").text());
            var C = count.valueOf();
            if (C <= 2)
            {
                document.getElementById('J_marks').style.display = 'block';
                document.getElementById('J_comment').style.display = 'block';
                // document.getElementById('add').style.display = 'block';
                document.getElementById('Mark_error').style.display = 'none';
            } else
            {
                document.getElementById('J_marks').style.display = 'none';
                document.getElementById('J_comment').style.display = 'none';
                //   document.getElementById('add').style.display = 'none';
                document.getElementById('Mark_error').style.display = 'block';

            }

            $("#RegDetailModal").modal();
        }


//            function GetName() {
//                $.ajax({
//                    url: 'download.php',
//                    type: "POST",
//                    data: ({file: $("#file").text()}),
//                    success: function (data) {
//                        console.log(data);
//                        $("#file_dow").html(data);
//                    }
//                });
//            }
//
//            function Download()
//            {
//                return "download.php?file=" + $("#file").text();
//            }
    </script>
    <footer id="footer">
        <div class="nino-copyright">
            <div class="row">
                <div class="colInfo">
                    <div class="col-md-1"></div>
                    <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                        <ul class="nav navbar-nav">
                            <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                            <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3" style="padding-left: 30px">
                        <div class="nino-followUs">
                            <div class="socialNetwork">
                                <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
<!--                                <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                                            <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>-->
<!--                                                            <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>-->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
    </div>
</footer>
</body>
</html>     
<?php
$recordAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
}

//if ($recordAdded) {
//    echo '
//<script type="text/javascript">
//    function hideMsg()
//    {
//        document.getElementById("popup").style.visibility = "hidden";
//    }
//
//    document.getElementById("popup").style.visibility = "visible";
//    window.setTimeout("hideMsg()", 2000);
//</script>';
//}
?>