 

<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
       <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 
                                        <li><a onclick="window.location.href = 'index.php'">Home</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->




        <!-- Services
        ================================================== -->
         <div class="col-md-6" style="padding-top: 40px">
                                <div class="text-center">
                                    <img src="images/what-we-do/New_Logo.png" alt="">
                                </div>
                            </div>
        <section1 id="registration">
            <div class="container">
                <div class="sectionContent" style="text-align: justify">
          
                <div>
                <row> 
                    <p style="text-align: center;font-size: 20px;color: black">
                    RULES & REGUALTIONS
                    <p>
                </row>
            </div>
                    <p style="padding-bottom: 10px">
Registration will be complete only upon payment confirmation acknowledged by Singapore Language Organization on / by 15 January'18
                </p>

                <row>
                <p style="text-align: center;font-size: 20px;color: black">Entries have to follow the theme; FOLK TALES RETOLD WITH THE MODERN TWIST</p>

                <row>
                    <p style="padding-bottom: 10px"> You can submit your entries in one or more language in your age group via website submission, designated partner organization or email to singaporelanguage@gmail.com with subject for Prerna award 2017-18/ Language & Name of the participant submission before the submission date ( 26 Jan'18)  upon registration and payment. </p>

                    <p style="color: black">Complete Script, presentation & / or  Video has to be submitted.<p>

                        Participants are free to use any medium for telling their stories ranging from PowerPoint, Comic Strips, Playacting, Puppet etc.<br><br>

                        Presenters are expected to be innovative in their choice of topic, drawing upon combinations of research, personal experience and imagination.<br><br>

                    <p style="color: black">*Shortlisted entries will be invited for presentation in front of the Jury Panel on final competition day to be informed by 2 Feb’18 and it will be held over the weekends in Feb’18.<br><br>

JUDGING CRITERIA </p>

                    Entries will be judged on four criteria with equal emphasis on each:<br>

                    •             Concept & Presentation<br>
                    

                    •             Voice and Tone in Writing<br>

                    •             Creativity & Innovation<br>

                    •             Organized and Cohesive Work<br><br>

 
                    <p style="font-size: 20px;color: black">PRIZES: </p>

                    There are three prizes in each of the three age groups & languages :<br>

Best Entry:              Gold <br>

First Runner Up:     Silver <br>

Second Runner Up:  Bronze <br>

Honourable Mention:  Certificate of Appreciation<br>

In addition, all winners will receive:<br>

An opportunity to intern as a writer with a published author/ script writer/ journalist<br>

Selected entries will also be published on Prerna Awards various online channels and e publications!<br>

All Participants get certificate of participation from the organizers <br><br>


<p style="font-size: 20px;color: black">GENERAL RULES & REGULATIONS</p>

<p style="font-size: 15px;color: black">JUDGING CRITERIA</p>

Entries will be judged on four criteria with equal emphasis on each: <br>

•             Concept & Presentation <br>

•             Voice and Tone in Writing <br>

•             Creativity & Innovation <br>

•             Organized and Cohesive Work <br><br><br>

<div style="padding-left: 50px">
•  Participants are free to use any medium for telling their stories ranging from Comic Strips, Presenters are expected to be innovative in their choice of topic, drawing upon combinations of research, personal experience and imagination.<br>
•  Initial shortlisting of entries is undertaken by a team of experienced readers associated with the organizing team. Final judging of the shortlist will be undertaken by experienced writers appointed by the organizers.<br>
•  The organizers reserve the right to modify judging mechanics and change the judging panel without notice and not to award prizes or host a prize giving if, in the organizers opinion, such an action is justified for any reason.<br>
•  The judges’ decision is final and neither the judges nor the organizers will enter into any correspondence.<br>
•  Judges are unable to comment on individual entries.<br><br><br>
</div>
<p style="font-size: 20px;color: black">STORY WRITING & STORY TELLING COMPETITION</p>

1. The story must be entirely the original work of the entrant and must have reference to any folk tale.The story must be between 250 -1200 words and original. Stories can be about anything   under the sun, so long as they follow the theme & guidelines and must include one verse (4 lines) of poetry (quoted or original)<br><br>

2. Your title will not be the part of the word count.<br><br>

3. Contractions count as one word. Hyphenated words are one word.<br><br>

4. Entries must be accompanied by a completed entry form and payment accepted. All entries will be considered anonymously by the judges.<br><br>

5. Inappropriate language or comments will be disqualified (vulgarity, swearing and that sort).<br><br>

6. No (explicit) sexual content is allowed.<br><br>

7.  No religious references can be made.<br><br>

8. Video Recording Or link to uploaded video on you tube of one to be send via email to  singaporelanguage@gmail.com before 30 Jan’18.<br><br>

9. Entries must be typed; single sided A4. Handwritten entries may be scanned and must be legible. A video submission via email or private YouTube link of the story telling  also has to be submitted along with the transcript. Shortlisted participants will have to present in front of the judges.<br><br>

10. Entries must never have been published, self-published, published on any website or broadcast in any form nor currently entered in any other competition. The poem must not have won a prize in any previous competition.<br><br>

11. Entries must be accompanied by a completed entry form and payment accepted. All entries will be considered anonymously by the judges.<br><br>

12. Competitions entries will not be returned, so please keep a copy of your submission. <br><br>

13. We are unable under any circumstances to allow changes to be made to poems once entered or for fees to be refunded. Entrants may withdraw entries from the competition by notification in writing but any entry fees will not be refunded.<br><br>

14. Singapore Language organization reserves the right to amend these rules where it is deemed necessary to do so or where circumstances are beyond the organizers control. Any changes to the rules will be posted on the www.singaporelangauge.org website.<br><br>

 15. Submissions must be entered under an entrant’s real name.<br><br>

 16. The organizers cannot accept responsibility for any damage, loss, injury or disappointment suffered by any entrant entering the Competition.<br><br>

 17. Winners will be notified by 5th February 18  and invited to an award ceremony in early 2018. All winners will be required to provide a biography and photograph.<br><br>

 18. The copyright of all the entries remains with the authors. However, the participant's submitted entry to the competition, grant  Singapore Language Organisation  the right to publish in print and online/digital and/or broadcast their winning story in perpetuity..<br><br>

 
 <p style="font-size: 20px;color: black">CRITERIA FOR JUDGING THE STORY TELLING PRESENTATION</p><br>

 <p style="font-size: 10px;color: black">10 Points</p> 

A virtually flawless presentation in every respect – no memory slips, false starts or significant pronunciation errors of any kind. Creative expression and intonation are appropriate to the content and flow spontaneously, indicating an excellent grasp of the actual content of the story and a clear insight into the writer’s intention. Presentation, whether delivered solely with voice modulations or with other appropriate gestures, reflect a high level of poise and smoothness, free from all visible signs of nervousness.<br><br>

 <p style="font-size: 10px;color: black">8 Points</p> 

Well balanced recitation with very few or no memory slips or false starts, and no more than 1 or 2 significant pronunciation errors. Creative expression and intonation are appropriate to the poem and flow spontaneously, indicating a good grasp of the content and a definite feel for the writer’s intentions. Presentation is executed with a high level of poise and smoothness, flawed only in a small degree due to slight nervousness.<br><br>

 <p style="font-size: 10px;color: black">6 Points</p> 

Good recitation with an occasional memory slip or false start and perhaps several significant pronunciation errors which begin to interfere with smoothness and delivery. Creative expression and intonation appropriate to the content but lacking somewhat in spontaneity and ease, tending to suggest less than adequate preparation. Presentation, suffering from lack of poise and smoothness, is hindered by nervousness.<br><br>

 <p style="font-size: 10px;color: black">4 Points</p> 

Fair presentation with several memory slips or false starts and a considerable number of pronunciation errors which definitely interfere with smoothness of delivery. Cretaive expression and intonation lacking considerably or inappropriate for the content.<br><br>

 <p style="font-size: 10px;color: black">2 Points</p> 

Poor presentation with persistent, serious memory slips and pronunciation errors which keep the content from holding together in delivery. Overwhelming absence of appropriate creative expression and intonation, poise and spontaneity.  Participant finishes presentation.<br><br>

 <p style="font-size: 10px;color: black">0 Point</p> 

Disqualification, Participant does not finish the presentation, even if 2/3 is recited. The participant MUST FINISH in order and receiving a score of 1 or more.<br><br>

 
<p style="font-size: 20px;color: black">PRESENTATION GUIDELINES – INITIAL POWERPOINT / VIDEO SUBMISIION  & LIVE PRESENTATION IF SHORTLISTED</p>
1. Stories & presentation must be original by the participant. Where research is used, sources should be identified. Judges may disqualify competitors for plagiarism.<br><br>
2. Participants are free to choose their own topic. However, they are expected to monitor the content and delivery of the speeches according to the standards of good taste and according to the theme of the competition.<br><br>
3. Presenters should use language, which is appropriate to the topic, the audience and the formality of the occasion. Avoid off-color jokes, slang expressions and “bathroom humor”.<br><br>
4. Statements which might be construed as slanderous, embarrassing, or containing gender, cultural, or racial stereotypes are unsuitable for a public competition. Speakers, from the beginning, should consider their intended audience to be “the public” – a mixed age group, not simply an audience of peers.<br><br>
5. Participants are expected to dress neatly and appropriately to suit the formality of the occasion, and to stand appropriately on the platform.<br><br>
6. Gestures & expressions can be dramatic and props are allowed to be used for Story Telling.( Examples of unacceptable expressions- turning one’s back to the audience, vulgar & ugly gestures).<br><br>
7. Notes on cards not exceeding 3” x 5” in size are permitted for reference for final competition<br><br>
8. Presentation shall not be less than two minutes and not more than 10 minutes in length. One point shall be deducted for each ten seconds or part thereof over or under this time limit.<br><br>
9. The decision of the judges shall be final and binding on all the participants & organizers. 
  
                </div>
            </div>
            
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js_1/tos.js"></script>
            <script src="js_1/moment.js" type="text/javascript"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>



        </section1><!--/#nino-services-->

  <footer id="footer">
            <div class="nino-copyright">
                <div class="row">
                    <div class="colInfo">
                        <div class="col-md-1"></div>
                        <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                            <ul class="nav navbar-nav">
                                <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                     <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                                
                            </ul>
                        </div>
                        <div class="col-md-3" style="padding-left: 30px">
                            <div class="nino-followUs">
                                <div class="socialNetwork">
                                    <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                    <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
        </div>
    </footer>
   
        

        <!-- javascript -->
        <script type="text/javascript" src="js/jquery.min.js"></script>	
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
        <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="js/unslider-min.js"></script>
        <script type="text/javascript" src="js/template.js"></script>
        <script src="js_1/moment.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script>
        $(function () {
        $("#User_type").change(function () {
            var u_type=$("#User_type").val();
            if(u_type=="judge")
            {
                 document.getElementById('judge_lang').style.display = 'block';
                 document.getElementById('judge_comp').style.display = 'block';
            }
            else
            {
                document.getElementById('judge_lang').style.display = 'none';
                document.getElementById('judge_comp').style.display = 'none';
            }

        });
        });
        </script>

    </body>
</html>
<?php
        $recordAdded = false;

        if (isset($_GET['status']) && $_GET['status'] == 1) {
            $recordAdded = true;
        }

        if ($recordAdded) {
            echo '
<script type="text/javascript">
    function hideMsg()
    {
        document.getElementById("popup").style.visibility = "hidden";
    }

    document.getElementById("popup").style.visibility = "visible";
    window.setTimeout("hideMsg()", 2000);
</script>';
        }
        ?>
