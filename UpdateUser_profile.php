 

<?php
session_start();
include_once './dbconnect.php';
include("functions.php");
$link = DbConnect::GetConnection();
if ($link) {
    $sid = $_SESSION['Creg_id'];

    $sql = "SELECT * FROM contestant_registration where Contestant_id=$sid";
    $result = $link->query($sql);
    $record = '';
    while ($row = $result->fetch_assoc()) {
        $RegName = $row['name'];
        $RegPhn = $row['contact_no'];
        $RegEmail = $row['email'];
        $Regadd = $row['address'];

        $Regdob = ConvertToRegularFormat($row['DOB']);


        $Regreference = $row['reference'];

        $RegSchool = $row['school_name'];
        $RegTeacher = $row['teacher_name'];
        $RegParents = $row['Parents_name'];
        $RegP_Email = $row['Parents_email'];
        $RegCountry = $row['country'];
    }

    echo $record;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 
                                    <!--                                    <li><a onclick="window.location.href = 'index.php'">Home</a></li>-->
                                    <li><a onclick="window.location.href = 'Update_profile.php'">Back</a></li>
                                    <li><a onclick="window.location.href = 'cont_Login.php'">Logout</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->




        <!-- Services
        ================================================== -->
        <section id="registration">
            <div class="container">
                <h2 class="nino-sectionHeading">

                    Update Profile
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">



                                <img src="images/what-we-do/Prerna Flyer final.png" alt=""/>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-top: 50px"> 
                            <form method="post" autocomplete="off" action="update_RegData.php"id="freg_id1" name="freg_id1" enctype="multipart/form-data"class="nino-subscribeForm">
                                <?php
                                if (isset($errMSG)) {
                                    ?>
                                    <div class="form-group"  style="width: 50%">
                                        <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                            <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <lable id="name_error" style="color: Red; display: none">Please Enter Name</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user"  style="color: #182441" ></span></span> 
                                        <input type="text" name="uname" id="uname" class="form-control" placeholder="Enter Name" disabled="disabled" value="<?php echo $RegName; ?>"required/>
                                        <?PHP
                                        ?>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $(function () {
                                            $("#uname").blur(function () {
                                                if ($(this).val() == "") {
                                                    document.getElementById("name_error").style.display = 'block';
                                                    $("#uname").focus();
                                                } else
                                                {
                                                    document.getElementById("name_error").style.display = 'none';
                                                }
                                            });
                                        });
                                        </script>
                                    </div>
                                </div>
                                <lable id="phone_error" style="color: Red; display: none">Enter Phone no</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt text-success"  style="color: #182441" ></span></span>
                                        <input type="number" name="phone" id="phone" class="form-control" placeholder="Phone No."  value="<?php echo $RegPhn; ?>"required/>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#phone").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("phone_error").style.display = 'block';
                                                $("#phone").focus();
                                            } else
                                            {
                                                document.getElementById("phone_error").style.display = 'none';
                                            }

                                        });
                                        </script>
                                    </div>
                                </div>
                                <lable id="Email_error" style="color: Red; display: none">Enter Email Id</lable>
                                <lable id="EmailValid_error" style="color: Red; display: none">Please enter valid Email</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope text-success"  style="color: #182441"></span></span>
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" disabled="disabled" value="<?php echo $RegEmail; ?>"required/>

                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#email").blur(function () {

                                            $('#email').filter(function () {
                                                var emil = $('#email').val();
                                                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                                                if (!emailReg.test(emil)) {
                                                    document.getElementById("Email_error").style.display = 'block';
                                                    $("#Email_error").text("Please enter valid email");
                                                    $("#uname").focus();

                                                } else if ($('#email').val() == "")
                                                {
                                                    document.getElementById("Email_error").style.display = 'block';
                                                    $("#Email_error").text("Enter Email Id");
                                                    $("#uname").focus();
                                                } else
                                                {
                                                    document.getElementById("Email_error").style.display = 'none';
                                                }
                                            });
                                        });

                                        </script>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-home text-success"  style="color: #182441"></span></span>
                                        <input type="textarea" name="add" id="add" class="form-control" placeholder="Enter Address"  value="<?php echo $Regadd; ?>" required/>
                                    </div>
                                </div>
                                <lable id="dob_error" style="color: Red; display: none">Enter date of birth</lable>
                                <div class="form-group">                    
                                    <div class='input-group input-group-lg date' id='datetimepicker1'>
                                        <span class="input-group-addon">
                                            <span class="fa fa-birthday-cake text-success"  style="color: #182441"></span>
                                        </span>
                                        <input type="text" name="dob" id="dob" class = "form-control" placeholder = "Date Of  Birth" disabled="disabled" value="<?php echo $Regdob; ?>" required/>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#dob").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("dob_error").style.display = 'block';
                                                $("#dob").focus();
                                            } else
                                            {
                                                document.getElementById("dob_error").style.display = 'none';
                                            }
                                        });
                                        </script>
                                    </div>                    
                                </div>
                                <lable id="AgeCategory_error" style="color: Red; display: none">Please Select Age Category</lable>
                                <div class="form-group">                    
                                    <div class='input-group input-group-lg'>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"  style="color: #182441"></span>
                                        </span>
                                        <select  class = "form-control" name="age" id="age" disabled="disabled">
                                            <option value="">Select Age Category </option>
                                            <option value="Child"> 8 - 12 Yrs</option>
                                            <option value="Youth"> 13 -16 Yrs</option>
                                            <option value="Open">17 Yrs & Above </option>    
                                        </select>
                                        <input type="text" id="Dage" name="Dage" style="display: none">
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#age").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("AgeCategory_error").style.display = 'block';
                                                $("#age").focus();
                                            } else
                                            {
                                                document.getElementById("AgeCategory_error").style.display = 'none';
                                            }
                                        });
                                        </script>

                                    </div>                    
                                </div>
                                <div class="form-group">                    
                                    <div class='input-group input-group-lg'>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-heart text-success"  style="color: #182441"></span>
                                        </span>

                                        <select  class = "form-control" name="abt_us" id="abt_us" required>
                                            <option value="">How did you know about us</option>
                                            <option value="Facebook" <?php
                                            if ($Regreference === "Facebook") {
                                                echo 'selected="selected"';
                                            }
                                            ?> >Facebook</option>
                                            <option value="Urbandesis" <?php
                                            if ($Regreference === "Urbandesis") {
                                                echo 'selected="selected"';
                                            }
                                            ?> >Urbandesis</option>
                                            <option value="School"<?php
                                            if ($Regreference === "School") {
                                                echo 'selected="selected"';
                                            }
                                            ?> >School</option>    
                                            <option value="Internet" <?php
                                            if ($Regreference === "Internet") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Internet</option>    
                                        </select>
                                    </div>                    
                                </div>

                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-home text-success"  style="color: #182441"></span></span>
                                        <input type="text" name="School" id="School" class="form-control" placeholder="Enter School Name" value="<?php echo $RegSchool; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"> <span class="fa fa-user" style="color: #182441"></span></span>
                                        <input type="text" name="Teacher" id="Teacher" class="form-control" placeholder="Enter Teacher Name" value="<?php echo $RegTeacher; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group"  id="country">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-home text-success" style="color: #182441"></span></span>
                                        <select name="cntry" id="cntry"  class="form-control" disabled="disabled" required>
                                            <option value="">Select Country</option>
                                            <option value="India" <?php
                                            if ($RegCountry === "India") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>India</option>
                                            <option value="Sri Lanka" <?php
                                            if ($RegCountry === "Sri Lanka") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Sri Lanka</option>
                                            <option value="Singapore" <?php
                                            if ($RegCountry === "Singapore") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Singapore</option>
                                            <option value="Malaysia" <?php
                                            if ($RegCountry === "Malaysia") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Malaysia</option>
                                            <option value="Indonesia" <?php
                                            if ($RegCountry === "Indonesia") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Indonesia</option>
                                            <option value="Thailand" <?php
                                            if ($RegCountry === "Thailand") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Thailand</option>
                                            <option value="Myanmar" <?php
                                            if ($RegCountry === "Myanmar") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Myanmar</option>
                                            <option value="Brunei Darussalam" <?php
                                            if ($RegCountry === "Brunei Darussalam") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Brunei Darussalam</option>
                                            <option value="Bangladesh" <?php
                                            if ($RegCountry === "Bangladesh") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Bangladesh</option>
                                            <option value="Pakistan" <?php
                                            if ($RegCountry === "Pakistan") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Pakistan</option>
                                            <option value="United Arab Emirates" <?php
                                            if ($RegCountry === "United Arab Emirates") {
                                                echo 'selected="selected"';
                                            }
                                            ?>>United Arab Emirates</option>


                                        </select>

                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#cntry").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("country_error").style.display = 'block';
                                                $("#cntry").focus();
                                            } else
                                            {
                                                document.getElementById("country_error").style.display = 'none';
                                            }
                                        });
                                        </script>
<!--                                        <input type="text" name="Country" id="Country" class="form-control" placeholder="Enter Country Of Residence" value="<?php echo $RegCountry; ?>" required/>-->
                                    </div>
                                </div>

                                <div class="form-group" style="display:none; padding-left:20px"" id="M_lable">
                                    <div class="input-group input-group-lg"  >

                                        <label style="color:red;">For Minors Only</label>

                                    </div>
                                </div>

                                <div class="form-group" style="display:none" id="P_name">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"> <span class="fa fa-user" style="color: #182441"></span></span>
                                        <input type="text" name="Parents" id="Parents" class="form-control" placeholder="Enter Parents Name" value="<?php echo $RegParents; ?>">
                                    </div>
                                </div>
                                <lable id="Email_error1" style="color: Red; display: none">Enter Email Id</lable>
                                <lable id="EmailValid_error1" style="color: Red; display: none">Please enter valid Email</lable>
                                <div class="form-group"  style="display:none" id="p_email">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-envelope text-success" style="color: #182441"></span></span>
                                        <input type="email" name="P_Email" id="P_Email" class="form-control" placeholder="Enter Parents Email" value="<?php echo $RegP_Email; ?>">
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#P_Email").blur(function () {

                                            $('#P_Email').filter(function () {
                                                var emil = $('#P_Email').val();
                                                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                                                if (!emailReg.test(emil)) {
                                                    document.getElementById("Email_error1").style.display = 'block';
                                                    $("#Email_error1").text("Please enter valid email");
                                                    $("#P_Email").focus();

                                                } else if ($('#email').val() == "")
                                                {
                                                    document.getElementById("Email_error1").style.display = 'block';
                                                    $("#Email_error1").text("Enter Email Id");
                                                    $("#P_Email").focus();
                                                } else
                                                {
                                                    document.getElementById("Email_error1").style.display = 'none';
                                                }
                                            });
                                        });

                                        </script>
                                    </div>
                                </div>                                 
                                <!--                                <div class="form-group" style="display:none" id="country">
                                                                    <div class="input-group input-group-lg">
                                                                        <span class="input-group-addon"> <span class="fa fa-user" style="color: #182441"></span></span>
                                                                        <input type="text" name="Country" id="Country" class="form-control" placeholder="Enter Country Of Residence" value="<?php echo $RegCountry; ?>">
                                                                    </div>
                                                                </div>-->
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success" name="signup" id="signup">Save </button>
                                </div>
                            </form>                      
                            <div id="popup2"style="display: none">
                                <h2 style="text-align: center"></h2> <br>
                                <div class="content" style="text-align: center">                               
                                    <br><br>Thank you ! Your Profile has been updated successfully. Please proceed to register for the <a href = 'Update_profile.php'  id="complink" name="complink" style="color:blue">competition.</a>
                                    <form action="Update_profile.php">
                                        <br><br><button type="submit" class="btn btn-block btn-success">Back </button>
                                    </form>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js_1/tos.js"></script>
            <script src="js_1/moment.js" type="text/javascript"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
            <script>
                                        $(function () {
                                            $('#dob').datetimepicker({
                                                format: 'DD-MM-YYYY'
                                            });
                                            $('#dob').on('dp.change', function (e) {
                                                dateChange();

                                            });

                                            window.onload = dateChange();
//                                            $(document).ready(function () {
//                                                dateChange();
//                                            });

                                        });

                                        function dateChange()
                                        {

                                            var myDate = $('#dob').val();
                                            var chunks = myDate.split('-');

                                            var d = chunks[1] + '-' + chunks[0] + '-' + chunks[2];



                                            // var d = new Date($('#dob').val());
                                            var year = chunks[2];
                                            var today = new Date();
                                            var today_year = today.getFullYear();
                                            var age = today_year - year;
                                            $("#Dage").val(age);
                                            if (age <= 18)
                                            {
                                                document.getElementById('M_lable').style.display = 'block';
                                                document.getElementById('P_name').style.display = 'block';
                                                document.getElementById('p_email').style.display = 'block';

                                            } else
                                            {
                                                document.getElementById('M_lable').style.display = 'none';
                                                document.getElementById('P_name').style.display = 'none';
                                                document.getElementById('p_email').style.display = 'none';

                                            }

                                            if (age <= 12)
                                            {
                                                document.getElementById("age").selectedIndex = 1;
                                            } else if (age >= 13 && age <= 16)
                                            {
                                                document.getElementById("age").selectedIndex = 2;
                                            } else if (age >= 17)
                                            {
                                                document.getElementById("age").selectedIndex = 3;
                                            }
                                        }
            </script>
            <script>
                $(function () {
                    $("#Language").change(function () {
                        //               var selectedText = $(this).find("option:selected").text();
                        //                var selectedValue = $(this).val();
                        //                alert("Selected Text: " + selectedText + " Value: " + selectedValue);
                        LoadComp();
                    });

                    LoadComp();

                });
                function LoadComp() {
                    $.ajax({
                        url: 'Reg_responce.php',
                        type: "POST",
                        data: ({Language: $("#Language").val()}),

                        success: function (data) {
                            console.log(data);
                            $("#div_comp").html(data);
                        }
                    });
                }
            </script>
        </section><!--/#nino-services-->
        <footer id="footer">
            <div class="nino-copyright">
                <div class="row">
                    <div class="colInfo">
                        <div class="col-md-1"></div>
                        <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                            <ul class="nav navbar-nav">
                                <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3" style="padding-left: 30px">
                            <div class="nino-followUs">
                                <div class="socialNetwork">
                                    <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                    <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
    <!--                                <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                                                <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>-->
    <!--                                                            <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
        </div>
    </footer>

    <!-- Search Form - Display when click magnify icon in menu
    ================================================== -->
    <form action="" id="nino-searchForm">
        <input type="text" placeholder="Search..." class="form-control nino-searchInput">
        <i class="mdi mdi-close nino-close"></i>
    </form><!--/#nino-searchForm-->

    <!-- Scroll to top
    ================================================== -->
    <a href="#" id="nino-scrollToTop">Go to Top</a>

    <!-- javascript -->
    <script type="text/javascript" src="js/jquery.min.js"></script>	
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/unslider-min.js"></script>
    <script type="text/javascript" src="js/template.js"></script>
    <script src="js_1/moment.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>


</body>
</html>
<?php
$recordAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
}

if ($recordAdded) {
    echo '
<script type="text/javascript">
     
    document.getElementById("freg_id1").style.display = "none";
    document.getElementById("popup2").style.display = "block";
  
</script>';
}
?>