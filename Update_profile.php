 

<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 
                                      <li><a onclick="window.location.href = 'change_pass.php'">Change Password</a></li>
                                    <li><a onclick="window.location.href = 'cont_Login.php'">Logout</a></li>
                                  
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>            
            </div>
        </header><!--/#header-->
        <!-- Services
        ================================================== -->
        <section id="nino-brand" style="background: white">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">User</span>
                    Profile
                </h2>
                <div class="sectionContent">
                    <div class="row">

                        <!--                        <div class="col-md-6" style="padding-top: 20px;padding-left: 180px; align-content: center">-->
                        <div class="col-md-6 col-xs-12"> 
                            
<!--                            <div class="col-md-3"></div>-->
                            <div class="col-xs-8 col-xs-offset-2">
                                <div class="verticalCenter fw" layout="row">
                                    <div class="text-center">
                                        <h2 class="nino-sectionHeading">
                                            <span class="nino-subHeading"></span>
                                            Update Profile
                                        </h2>
                                        <a onclick="window.location.href = 'UpdateUser_profile.php'" style="font-size:20px;color: black;"><img src="images/what-we-do/u_1.png" alt=""/></a></div>                    
                                </div>
                            </div>
                            
                        </div>
                        <?PHP
                        $link = DbConnect::GetConnection();
                        if ($link) {
                            $sid = $_SESSION['Creg_id'];

                            $sql2 = "SELECT  CAST(`is_reg` AS UNSIGNED) AS `is_reg` from contestant_registration where Contestant_id=$sid";
                            $result2 = $link->query($sql2);
                            $record = '';
                            $row = $result2->fetch_assoc();
                            $is_reg = $row['is_reg'];
                            if ($is_reg == '1') {
                                echo '<div class="col-md-6 col-xs-12" style="display: block" id="compreg">
                                     
                                     <div class="col-xs-8 col-xs-offset-2">
                            <div class="verticalCenter fw" layout="row">
                                <div class="text-center">
                                    <h2 class="nino-sectionHeading">
                                        <span class="nino-subHeading"></span>
                                       Register For Competition
                                    </h2>
                                    <a onclick="window.location.href =\'Competition_reg.php\'" style="font-size:20px;color: black;padding-bottom: 40px"><img src="images/what-we-do/c1.png" alt=""></a></div>                    
                            </div>
                                </div>
                             
                        </div>';
                            } else {
                                echo '<div class="col-md-6 col-xs-12" style="display: none" id="compreg">
                                     
                                      <div class="col-xs-8 col-xs-offset-2">
                            <div class="verticalCenter fw" layout="row">
                                <div class="text-center">
                                    <h2 class="nino-sectionHeading">
                                        <span class="nino-subHeading"></span>
                                       Register For Competition
                                    </h2>
                                    <a onclick="window.location.href =\'Competition_reg.php\'" style="font-size:20px;color: black;padding-bottom: 40px"><img src="images/what-we-do/c1.png" alt=""></a></div>                    
                            </div>
                                  </div>
                             
                        </div>';
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>

        <!--            <div id="popup">
            <Center>
                Record added successfully 
                    </Center>
                    </div>-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js_1/tos.js"></script>
        <script src="js_1/moment.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>



    </section><!--/#nino-services-->

    <footer id="footer">
        <div class="nino-copyright">
            <div class="row">
                <div class="colInfo">
                    <div class="col-md-1"></div>
                    <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                        <ul class="nav navbar-nav">
                            <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                     <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3" style="padding-left: 30px">
                        <div class="nino-followUs">
                            <div class="socialNetwork">
                                <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
<!--                                <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                                            <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>-->
<!--                                                            <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>-->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
    </div>
</footer>
<!-- Search Form - Display when click magnify icon in menu
================================================== -->
<form action="" id="nino-searchForm">
    <input type="text" placeholder="Search..." class="form-control nino-searchInput">
    <i class="mdi mdi-close nino-close"></i>
</form><!--/#nino-searchForm-->

<!-- Scroll to top
================================================== -->
<a href="#" id="nino-scrollToTop">Go to Top</a>

<!-- javascript -->
<script type="text/javascript" src="js/jquery.min.js"></script>	
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.hoverdir.js"></script>
<script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="js/unslider-min.js"></script>
<script type="text/javascript" src="js/template.js"></script>
<script src="js_1/moment.js" type="text/javascript"></script>
<script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>


</body>
</html>