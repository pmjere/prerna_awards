<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">

        <!-- Header
    ================================================== -->
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="homepage.html">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav">
                                  <li><a onclick="window.location.href = 'admin_password.php'">Change Password</a></li>
                                    <li><a onclick="window.location.href = 'cont_Login.php'">Logout</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                           
                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
 
            </div>
        </header><!--/#header-->
 
  
        <section id="nino-brand" style="background-color: white">
            <div class="container">
                <div class="verticalCenter fw" layout="row">
                    <div class="col-xs-6"><a onclick="window.location.href = 'Language.php'" style="font-size:30px;color: black;padding-bottom: 40px">Language List <br><img src="images/brand/h.png" alt=""></a></div>
                    <div class="col-xs-6"><a onclick="window.location.href = 'competition.php'" style="font-size:30px;color: black;padding-bottom: 40px">Competition List<br><img src="images/brand/uu.png" alt=""></a></div>
                    <div class="col-xs-6"><a onclick="window.location.href = 'NewUser.php'" style="font-size:30px;color: black;padding-bottom: 40px">Create New User<br><img src="images/brand/3.png" alt=""></a></div>
                    <div class="col-xs-6"><a onclick="window.location.href = 'admin_regList.php'" style="font-size:30px;color: black;padding-bottom: 40px;">Registration List<br><img src="images/brand/rr.png" alt=""></a></div>
                    
                </div>
            </div>
        </section><!--/#nino-brand-->

      

        <!-- javascript -->
        <script type="text/javascript" src="js/jquery.min.js"></script>	
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
        <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="js/unslider-min.js"></script>
        <script type="text/javascript" src="js/template.js"></script>
        <script src="js_1/moment.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>


        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- css3-mediaqueries.js for IE less than 9 -->
        <!--[if lt IE 9]>
            <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
  <footer id="footer">
            <div class="nino-copyright">
                <div class="row">
                    <div class="colInfo">
                        <div class="col-md-1"></div>
                        <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                            <ul class="nav navbar-nav">
                                <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                       <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3" style="padding-left: 30px">
                            <div class="nino-followUs">
                                <div class="socialNetwork">
                                    <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                    <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
    <!--                                <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                                                <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>-->
    <!--                                                            <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
        </div>
    </footer>
    </body>
</html>