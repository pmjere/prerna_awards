

<?php

class DbConnect {

    public static $db_host = "localhost";
    public static $db_name = "prerna-awards";
     // public static $db_name = "server_data";
    public static $db_user = "root";
    public static $db_pass = "";

    public static function GetConnection() {
        $conn = new mysqli(DbConnect::$db_host, DbConnect::$db_user, DbConnect::$db_pass, DbConnect::$db_name,3307);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    }
}
