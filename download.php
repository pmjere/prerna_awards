<?php

/* Attempt MySQL server connection. Assuming you are running MySQL
  server with default setting (user 'root' with no password) */
session_start();
include_once './dbconnect.php';
$file =$_GET["file"];

$link = DbConnect::GetConnection();

//Check connection
if ($link === false) {
    die("ERROR: Could not connect. " . mysqli_error());
}
if (!empty($file)) {
    $Filename = basename($file);
    $filepath = 'uploads/' . $Filename;
    if (!empty($Filename) && file_exists($filepath)) {
        header("Chache-Controlv: public");
        header("Content-Description:File Transfer");
        header("Content-disposition: attachment; filename=".$Filename);
        header("Content-type: application/zip");
        header("Content-Transfer-Encoding: binary");
        
        readfile($filepath);
        exit();
    }
}

mysqli_close($link);
header('Location:Reg_list.php?status=1');
?>
 




 