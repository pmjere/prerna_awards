<?php

include_once './dbconnect.php';
include("functions.php");
 
session_start();
$link = DbConnect::GetConnection();

//Check connection
if ($link === false) {
    die("ERROR: Could not connect. " . mysqli_error());
}

/* Database connection end */
// storing  request (ie, get/post) global array to a variable  
$requestData = $_REQUEST;
$columns = array(
// datatable column index  => database column name
    0 => 'name',
    1 => 'contact_no',
    2 => 'email',
    3 => 'language',
    4 => 'category',
    5 => 'country',
    6 => 'marks'
);
// getting total number records without any search
//$sql = "SELECT name, contact_no, email";
$sql = "  SELECT * FROM    contestant_registration r 
 INNER JOIN participation p on r.Contestant_id=p.pCont_id
 INNER join language l on p.pLanguage_id=l.lang_id 
 INNER join competition c on p.pCompetition_id=c.com_id 
 Inner join judge j on j.category_id= r.age_category and j.language_id = p.pLanguage_id
 where j.judge_id='46' and r.Payment=true and r.is_reg=true";

$query = mysqli_query($link, $sql);
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if (!empty($requestData['search']['value'])) {
// if there is a search parameter
    $sql = "SELECT * FROM    contestant_registration r 
 INNER JOIN participation p on r.Contestant_id=p.pCont_id
 INNER join language l on p.pLanguage_id=l.lang_id 
 INNER join competition c on p.pCompetition_id=c.com_id 
 Inner join judge j on j.category_id= r.age_category and j.language_id = p.pLanguage_id
 where j.judge_id='46' and r.Payment=true and r.is_reg=true";
 
    $sql .= " And name LIKE '" . $requestData['search']['value'] . "%' ";    // $requestData['search']['value'] contains search parameter
    $sql .= " OR contact_no LIKE '" . $requestData['search']['value'] . "%' ";
    $sql .= " OR email LIKE '" . $requestData['search']['value'] . "%' ";
     $sql .= " OR language LIKE '" . $requestData['search']['value'] . "%' ";    // $requestData['search']['value'] contains search parameter
 
    $sql .= " OR country LIKE '" . $requestData['search']['value'] . "%' ";
      
    $query = mysqli_query($link, $sql);
    $totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 
    $sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
    $query = mysqli_query($link, $sql); // again run query with limit
} else {
    $sql = "SELECT * FROM    contestant_registration r 
 INNER JOIN participation p on r.Contestant_id=p.pCont_id
 INNER join language l on p.pLanguage_id=l.lang_id 
 INNER join competition c on p.pCompetition_id=c.com_id 
 Inner join judge j on j.category_id= r.age_category and j.language_id = p.pLanguage_id
 where j.judge_id='46' and r.Payment=true and r.is_reg=true";
    
   $sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
    $query = mysqli_query($link, $sql);
}
$data = array();
while ($row = mysqli_fetch_array($query)) {  // preparing an array
    $nestedData = array();
    $nestedData[] = $row["name"];
    $nestedData[] = $row["contact_no"];
    $nestedData[] = $row["email"];
       $nestedData[] = $row["language"];
         $nestedData[] = getFormatedAge($row["age_category"]);
          $nestedData[] = $row["country"];
 
 
  
    $l = $row["pLanguage_id"];
    $c1 = $row["pCompetition_id"];
    $reg_id = $row["reg_id"];
    $judge_id = $_SESSION["usre_id"];

    $sql1 = "SELECT COUNT(judge_id) as count FROM marks WHERE language_id=$l and competition_id=$c1  and Mreg_id=$reg_id";
    $result1 = mysqli_query($link, $sql1);
    $row1 = $result1->fetch_assoc();
    $type = $row1["count"];
    $sql2 = "SELECT marks FROM marks WHERE language_id=$l and competition_id=$c1 and judge_id=$judge_id  and Mreg_id=$reg_id";
    $result2 = mysqli_query($link, $sql2);
    $row2 = $result2->fetch_assoc();
    if ($row2 == 0) {
        $row2["marks"] = 0;
    } else {
        
    }
    $nestedData[] = $row2["marks"];
       // $nestedData[] ='<a href="" onclick="RegDetails(' . $row["reg_id"] . ',\'' . $row["name"] . '\',\'' . $row["contact_no"] . '\',\'' . $row["email"] . '\',\'' . $row["age_category"] . '\',\'' . $row["address"] . '\',\'' . $row["DOB"] . '\',\'' . $row["language"] . '\',\'' . $row["pLanguage_id"] . '\',\'' . $row["competition"] . '\',\'' . $row["pCompetition_id"] . '\',\'' . $row["country"] . '\',\'' . $row["reference"] . '\',\'' . $row["File_Name"] . '\',\'' . $row1["count"] . '\')" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Details">&#xE254;</i></a>';
    $data[] = $nestedData;
}



$json_data = array(
    "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
    "recordsTotal" => intval($totalData), // total number of records
    "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
    "data" => $data   // total data array
);
echo json_encode($json_data);  // send data as json format
?>