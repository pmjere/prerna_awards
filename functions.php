<?php

// functions.php
//session_start();
include_once './dbconnect.php';

function check_txnid($tnxid) {
    global $link;
    return true;
    $valid_txnid = true;
    //get result set
    $sql = mysqli_query("SELECT * FROM `payments` WHERE txnid = '$tnxid'", $link);
    if ($row = mysqli_fetch_array($sql)) {
        $valid_txnid = false;
    }
    return $valid_txnid;
}

function check_price($price, $id) {
    $valid_price = false;
    //you could use the below to check whether the correct price has been paid for the product

    /*
      $sql = mysql_query("SELECT amount FROM `products` WHERE id = '$id'");
      if (mysql_num_rows($sql) != 0) {
      while ($row = mysql_fetch_array($sql)) {
      $num = (float)$row['amount'];
      if($num == $price){
      $valid_price = true;
      }
      }
      }
      return $valid_price;
     */
    return true;
}

function updatePayments($data) {
    global $link;

    if (is_array($data)) {
        $sql = mysqli_query("INSERT INTO `payments` (txnid, payment_amount, payment_status, itemid, createdtime) VALUES (
				'" . $data['txn_id'] . "' ,
				'" . $data['payment_amount'] . "' ,
				'" . $data['payment_status'] . "' ,
				'" . $data['item_number'] . "' ,
				'" . date("Y-m-d H:i:s") . "'
				)", $link);
        return mysqli_insert_id($link);
    }
}

function ConvertToMysqlFormat($dateVal) {

    $now = new DateTime();
    //echo '<br/>New Obj=' . $now->format('Y-m-d H:i:s');
    $d1 = explode(" ", trim($dateVal)); // change method split to explode()
    $date = $d1[0];
    $time = $d1[1];
    $d = explode("-", trim($date)); //change method split to explode()
    $year = $d[2];
    $month = $d[1];
    $day = $d[0];
//        $t = explode(":", trim($time)); //change method split to explode()
//        //echo 'count($t)='.count($t);
//        $hour = $t[0];
//        $minute = $t[1];
//        $second = (count($t) <= 2) ? "00" : $t[2];

    $now->setDate($year, $month, $day);
//        $now->setTime($hour, $minute, $second);

    $gmt_time = date('Y-m-d', strtotime($now->format('Y-m-d')));
    return $gmt_time;
}

function ConvertToRegularFormat($dateVal) {

    $now = new DateTime();
    //echo '<br/>New Obj=' . $now->format('Y-m-d H:i:s');
    $d1 = explode(" ", trim($dateVal)); // change method split to explode()
    $date = $d1[0];
    $time = $d1[1];
    $d = explode("-", trim($date)); //change method split to explode()
    $year = $d[0];
    $month = $d[1];
    $day = $d[2];
//        $t = explode(":", trim($time)); //change method split to explode()
//        //echo 'count($t)='.count($t);
//        $hour = $t[0];
//        $minute = $t[1];
//        $second = (count($t) <= 2) ? "00" : $t[2];

    $now->setDate($year, $month, $day);
//        $now->setTime($hour, $minute, $second);
    $gmt_time = date('d-m-Y', strtotime($now->format('d-m-Y')));
    //$gmt_time = $now->format('d-m-Y');
    return $gmt_time;
}

function Age_Category($dob) {


    if (!empty($dob)) {
        $birthdate = new DateTime($dob);
        $today = new DateTime('today');
        $age = $birthdate->diff($today)->y;

        if ($age <= 12) {
            return '1';
        } else if ($age >= 13 && $age <= 16) {
            return '2';
        } else if ($age >= 17) {
            return '3';
        }
    } else {
        return "";
    }
}

function getFormatedAge($age_category) {

    if ($age_category === '1') {
        return 'Child : 8-12';
    }
    if ($age_category === '2') {
        return 'Youth :13-17';
    }
    if ($age_category === '3') {
        return 'Adults : 18 & above';
    }
}
 