<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />


        <style type="text/css">

            .popup {
                margin: 70px auto;
                padding: 20px;
                background: #fff;
                border-radius: 5px;
                width: 30%;
                position: relative;
                transition: all 5s ease-in-out;
            }

            .popup h2 {
                margin-top: 0;
                color: #333;
                font-family: Tahoma, Arial, sans-serif;
            }
            .popup .close {
                position: absolute;
                top: 20px;
                right: 30px;
                transition: all 200ms;
                font-size: 30px;
                font-weight: bold;
                text-decoration: none;
                color: #333;
            }
            .popup .close:hover {
                color: #06D85F;
            }
            .popup .content {
                max-height: 30%;
                overflow: auto;
            }

        </style>
    </head>
    <body data-target="#nino-navbar" data-spy="scroll">

        <!-- Header
    ================================================== -->
        <header id="nino-header">
            <div id="nino-headerInner">	
                <nav id="nino-navbar" class="navbar navbar-inverse" role="navigation">
                    <div class="container-fluid">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#nino-header">Home <span class="sr-only">(current)</span></a></li>
                                    <li><a href="#nino-story">About</a></li>
                                    <li><a href="#registration">Registration</a></li>
                                    <!--                                    <li><a href="#nino-ourTeam">Our Advisors</a></li>-->
                                    <li><a href="#nino-portfolio">Our Events</a></li>
                                    <li><a href="#nino-Contact">Contact Us</a></li>
                                    <li><a onclick="window.location.href = 'cont_Login.php'">Login</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="text-center">
                            <img src="images/what-we-do/lg12.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <section id="nino-slider" class="carousel slide container col-md-12 col-sm-12" data-ride="carousel" >
                            <!--                    <div class="col-md-6 col-sm-6">-->
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner  " role="listbox">
                                <div class="item active">
                                    <h2 class="nino-sectionHeading" style="font-weight:10;">
                                        <span class="nino-subHeading"> </span>
                                        भाषा दृष्टि नई सृष्टि <br> 
                                    </h2>
                                    <!--                            <a href="#" class="nino-btn">Learn more</a>-->
                                </div>
                                <div class="item">
                                    <h2 class="nino-sectionHeading" style="font-weight:10;">
                                        <span class="nino-subHeading"> </span>
                                        ભાષા દૃષ્ટિ થી નવી સૃષ્ટિ <br>
                                    </h2>
                                    <!--                            <a href="#" class="nino-btn">Learn more</a>-->
                                </div>
                                <div class="item">
                                    <h2 class="nino-sectionHeading" style="font-weight:10;">
                                        <span class="nino-subHeading"> </span>
                                        ভাষা দৃষ্টি নতুন সৃষ্টি <br> 
                                    </h2>
                                    <!--                            <a href="#" class="nino-btn">Learn more</a>-->
                                </div>
                                <div class="item">
                                    <h2 class="nino-sectionHeading" style="font-weight:10;">
                                        <span class="nino-subHeading"> </span>
                                        மொழியால் புதுமை செய்வோம்!    <br>
                                    </h2>
                                    <!--                            <a href="#" class="nino-btn">Learn more</a>-->
                                </div>
                            </div>      
                            <!--                    </div>-->
                        </section>
                    </div>

                </div>


        </header><!--/#header-->

        <!-- Story About Us
    ================================================== -->
        <section id="nino-story">
            <div class="container">
                <h2 class="nino-sectionHeading">

                    About Us
                </h2>
                <p class="nino-sectionDesc">Singapore Language Organisation & Prerna Awards aims to create an international platform to connect the world with the rich indigenous languages & literature in an engaging manner with national & international level events & competitions.<br><br>
                    <b>Mission & Vision</b><br>
                    Contribute towards Community development by building and preserving “COMMUNITY WEALTH” and promote Indigenous languages, globally. <br><br>
                    <b>Community</b><br>
                    To seek support from individuals, media, academic, social and political institutions and invite vernacular writers, philosophers and poets to disseminate their works, expertise and talents.<br><br>

                    <b>Programs</b><br>
                    Organize local & global competitions for promoting use & awareness about indigenous languages, heritage, culture and history through workshops and camps.<br>
                </p>
                <!--                <div class="sectionContent">
                                    <div class="row nino-hoverEffect">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="item">
                                                <a class="overlay" href="#">
                                                    <span class="content">
                                                        <i class="mdi mdi-account-multiple nino-icon"></i>
                                                        Prerna Awards 2017: Award Ceremony
                                                    </span>
                                                    <img src="images/story/Sangam26_1.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="item">
                                                <a class="overlay" href="#">
                                                    <span class="content">
                                                        <i class="mdi mdi-image-filter-center-focus-weak nino-icon"></i>
                                                        Prerna Awards 2017 : Storey Telling 
                                                    </span>
                                                    <img src="images/story/2.jpeg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="item">
                                                <a class="overlay" href="#">
                                                    <span class="content">
                                                        <i class="mdi mdi-airplay nino-icon"></i>
                                                        Prerna Awards 2017 : Prize Distribution
                                                    </span>
                                                    <img src="images/story/3.jpeg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
            </div>
        </section>
<!--            <section id="nino-story">
                <div class="container">
                    <div class="sectionContent">
                        <div class="row nino-hoverEffect">
                            <div class="col-md-4 col-sm-4">
                                <form method="post" autocomplete="off" action="volunteer.php" enctype="multipart/form-data"class="nino-subscribeForm">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success" name="signup12" id="signup12">Join As Volunteer </button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="item">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <form method="post" autocomplete="off" action="mailchimp.php" enctype="multipart/form-data"class="nino-subscribeForm">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success" name="signup12" id="signup12">Join As Member </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>/#nino-story-->

        <!--            logo
                    ======================================================================== -->
<!--            <section id="Awards">
            <div class="container">
                <h2 class="nino-sectionHeading">  
                    Announcing Prerna Awards 2018 
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6" style="padding-top: 40px">
                            <div class="text-center">
                                <img src="images/what-we-do/logo.JPEG" alt="">
                            </div>
                        </div>
                        <div class="col-md-6" style="background-color: #f7f7f7"> 
                            <p class="nino-sectionDesc" style="color: black;">
                                <br><br>
                                <b style="font-size: 15px; text-align: center">Storytelling competition in Hindi, Tamil, Bangla and Gujarati.  </b> 
                            </p>

                            <p style="color: black">    If you want to participate and partner  in a nationwide campaign to revive our Folk Tales & Folk Art pls fill in our registration form below. Alternatively if you need any more information please feel free to call <b style="color: #1b6db8 "> 98877117</b> or email to <b style="color: #1b6db8"> hindifoundation@gmail.com</b> <br></p>

                            <b style="color: black">Theme of Competition : </b><br>
                            <p style="padding-left: 30px; color: black;">   •   Folk Tales Retold in Modern Time</p>  
                            <b style="color: black">Age Groups :</b><br> 
                            <p style="padding-left: 30px; color: black;">   •   Child -8-12 </p>
                            <p style="padding-left: 30px; color: black;">   •  Youth-13-17</p>
                            <p style="padding-left: 30px; color: black;">   •   Open -18 & above</p>

                            <b style="color: black"> Key Dates to Remember : </b><br> 
                            <p style="padding-left: 30px; color: black;">   •   Registration ends : 25 Jan 2018 </p>
                            <p style="padding-left: 30px; color: black;">   •   Submission ends : 27 Jan 2018</p>
                            <p style="padding-left: 30px; color: black;">   •   Semi Finals/Finals :10 & 11 Feb 2018</p>
                            <p style="padding-left: 30px; color: black;">   •   Prerna Awards ceremony:24 Feb 2018</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>/#nino-services-->

        <!-- Counting
    ================================================== -->
        <!--            <h2 class="nino-sectionHeading"> 
                        Registration ends 
                    </h2>
                    <section id="nino-counting">
         
                        <div class="container">
                            <div layout="row" class="verticalStretch">
                                <div class="item">
                                    <div class="number"></div>
                                    <div class="text"></div>
                                </div>
                                <div class="item">
                                    <div class="number" id="day"></div>
                                    <div class="text">Days</div>
                                </div>
                                <div class="item">
                                    <div class="number" id="hr"></div>
                                    <div class="text">Hours</div>
                                </div>
                                <div class="item">
                                    <div class="number" id="min"></div>
                                    <div class="text">Minitus</div>
                                </div>
                                <div class="item">
                                    <div class="number" id="sec"></div>
                                    <div class="text">Seconds</div>
                                </div>
        
                                <div class="item">
                                    <div class="number"></div>
                                    <div class="text"></div>
                                </div>
                            </div>
                        </div> 
                    </section>-->

<!--            <script>
                // Set the date we're counting down to
                var countDownDate = new Date("Jan 26, 2018 23:56:56").getTime();

                // Update the count down every 1 second
                var x = setInterval(function () {

                    // Get todays date and time
                    var now = new Date().getTime();

                    // Find the distance between now an the count down date
                    var distance = countDownDate - now;

                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    // Output the result in an element with id="demo"
                    //    document.getElementsByClassName("verticalStretch").innerHTML = days + "d " + hours + "h "
                    //    + minutes + "m " + seconds + "s ";
                    //    
                    document.getElementById("day").innerHTML = days;
                    document.getElementById("hr").innerHTML = hours;
                    document.getElementById("min").innerHTML = minutes;
                    document.getElementById("sec").innerHTML = seconds;
                    // If the count down is over, write some text 
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementsByClassName("verticalStretch").innerHTML = "EXPIRED";
                    }
                }, 1000);
            </script>-->

        <!-- Services
        ================================================== -->
        <section id="registration">
            <div class="container">
                <h2 class="nino-sectionHeading">

                    Announcing Prerna Awards 2018 

                </h2>
                <div class="sectionContent">
                    <div class="row">

                        <div class="col-md-6"> 
                            <h2 style="text-align: center; padding-bottom: 10px"> Registeration Form </h2>
                            <form method="post" autocomplete="off" action="Payments.php" id="freg_id" name="freg_id" enctype="multipart/form-data"class="nino-subscribeForm" target="_blank">
                                <?php
                                if (isset($errMSG)) {
                                    ?>
                                    <div class="form-group"  style="width: 50%">
                                        <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                            <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <lable id="name_error" style="color: Red; display: none">Please Enter Name</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user"  style="color: #172f6a" ></span></span> 
                                        <input type="text" name="uname" id="uname" class="form-control" placeholder="Enter Name" required/>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $(function () {
                                            $("#uname").blur(function () {
                                                if ($(this).val() == "") {
                                                    document.getElementById("name_error").style.display = 'block';
                                                    $("#uname").focus();
                                                } else
                                                {
                                                    document.getElementById("name_error").style.display = 'none';
                                                }
                                            });
                                        });
                                        </script>
                                    </div>
                                </div>
                                <lable id="phone_error" style="color: Red; display: none">Enter Phone no</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt text-success"  style="color: #172f6a" ></span></span>
                                        <input type="number" name="phone" id="phone" class="form-control" placeholder="Phone No." required/>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#phone").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("phone_error").style.display = 'block';
                                                $("#phone").focus();
                                            } else
                                            {
                                                document.getElementById("phone_error").style.display = 'none';
                                            }

                                        });
                                        </script>
                                    </div>
                                </div>
                                <lable id="Email_error" style="color: Red; display: none">Enter Email Id</lable>
                                <lable id="EmailValid_error" style="color: Red; display: none">Please enter valid Email</lable>
                                <lable id="PresentEmail_error" style="color: Red; display: none">We already have received a registration with this email ID. If you have not received your login details kindly contact us.</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope text-success"  style="color: #172f6a"></span></span>
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" required/>                     
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#email").blur(function () {

                                            $('#email').filter(function () {
                                                var emil = $('#email').val();
                                                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                                                if (!emailReg.test(emil)) {
                                                    document.getElementById("Email_error").style.display = 'block';
                                                    $("#Email_error").text("Please enter valid email");
                                                    $("#email").focus();
                                                } else if ($('#email').val() == "")
                                                {
                                                    document.getElementById("Email_error").style.display = 'block';
                                                    $("#Email_error").text("Enter Email Id");
                                                    $("#email").focus();
                                                } else
                                                {
                                                    document.getElementById("Email_error").style.display = 'none';
                                                }
                                            });
                                        });
                                        </script>
                                    </div>
                                </div>

                                <lable id="dob_error" style="color: Red; display: none">Enter date of birth</lable>
                                <div class="form-group">                    
                                    <div class='input-group input-group-lg date' id='datetimepicker1'>
                                        <span class="input-group-addon">
                                            <span class="fa fa-birthday-cake text-success"  style="color: #172f6a"></span>
                                        </span>
                                        <input type="text" name="dob" id="dob" class = "form-control" placeholder = "Date Of  Birth" autocomplete="false"  required/>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#dob").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("dob_error").style.display = 'block';
                                                $("#dob").focus();
                                            } else
                                            {
                                                document.getElementById("dob_error").style.display = 'none';
                                            }
                                        });
                                        </script>
                                    </div>                    
                                </div>


                                <lable id="country_error" style="color: Red; display: none">Please Select country </lable>
                                <div class="form-group"  id="country">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-home text-success" style="color: #172f6a"></span></span>
                                        <select name="cntry" id="cntry"  class="form-control " required>                                           
                                            <option class="input-group input-group-lg" value="">Select Country</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                            <option value="India">India</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="Indonesia">Thailand</option>
                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                        </select>

                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#cntry").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("country_error").style.display = 'block';
                                                $("#cntry").focus();
                                            } else
                                            {
                                                document.getElementById("country_error").style.display = 'none';
                                            }
                                        });
                                        $("#cntry").change(function () {
                                            document.getElementById("promocode").style.display = "none";
                                                 document.getElementById("promo_sucess").style.display = "none";
                                                document.getElementById("promo_error").style.display = "none";
                                                document.getElementById("apply").style.display = "none";
                                            calc();
                                        });
                                        </script>
<!--                                        <input type="text" name="Country" id="Country" class="form-control" placeholder="Enter Country Of Residence" value="<?php echo $RegCountry; ?>" required/>-->
                                    </div>
                                </div>
                                <lable class="input-group input-group-lg"   style=" font-size: 15px; color: palevioletred">Participate For Storytelling competition</lable> <br>

                                <lable id="privacy_error" style="color: Red; display: none">Please Select language </lable>
                                <lable  class="input-group input-group-lg"   style=" font-size: 17px;">Select Language : </lable> <br>
                                <div class="form-group">
                                    <div class="input-group input-group-lg" style="font-size: 15px">

                                        <input type="checkbox" name="Hindi" id="Hindi"  onclick="calc()"/> Hindi &#8194;&#8194;&#8194;&#8194;
                                        <input type="checkbox" name="Tamil" id="Tamil"   onclick="calc()"/> Tamil &#8194;&#8194;&#8194;&#8194;
                                        <input type="checkbox" name="Bangla" id="Bangla"   onclick="calc()"/> Bangla  &#8194;&#8194;&#8194;&#8194;
                                        <input type="checkbox" name="Gujarati" id="Gujarati"   onclick="calc()"/> Gujarati

                                    </div>
                                </div>
                                <script>
                                    function calc()
                                    {
                                        var cntry = $('#cntry').val();
                                        if (cntry === 'Singapore')
                                        {
                                            document.getElementById("check").style.display = "block";

                                        } else
                                        {
                                            document.getElementById("check").style.display = "none";
                                        }
                                        var sum = 0;
                                        $("#fee").val(sum);
                                        var h = t = b = g = 0;
                                        if (document.getElementById('Hindi').checked)
                                        {
                                            if (cntry === 'Singapore')
                                            {
                                                h = 25.00;
                                            } else
                                            {
                                                h = 10.00;
                                            }
                                            sum = h + t + b + g;
                                            $("#fee").val(sum);
                                        }
                                        if (document.getElementById('Tamil').checked)
                                        {
                                            if (cntry === 'Singapore')
                                            {
                                                t = 25.00;
                                            } else
                                            {
                                                t = 10.00;
                                            }

                                            sum = h + t + b + g;
                                            $("#fee").val(sum);
                                        }
                                        if (document.getElementById('Bangla').checked)
                                        {
                                            if (cntry === 'Singapore')
                                            {
                                                b = 25.00;
                                            } else
                                            {
                                                b = 10.00;
                                            }

                                            sum = h + t + b + g;
                                            $("#fee").val(sum);
                                        }
                                        if (document.getElementById('Gujarati').checked)
                                        {
                                            if (cntry === 'Singapore')
                                            {
                                                g = 25.00;
                                            } else
                                            {
                                                g = 10.00;
                                            }
                                            sum = h + t + b + g;
                                            $("#fee").val(sum);
                                        }
                                        $("#spnAmt").html(sum);
//                                        if (sum === 25.00)
//                                        {
//                                            document.getElementById("PFee25").style.display = "block";
//                                            document.getElementById("PFee50").style.display = "none";
//                                            document.getElementById("PFee75").style.display = "none";
//                                            document.getElementById("PFee100").style.display = "none";
//                                        }
//                                        if (sum === 50.00)
//                                        {
//                                            document.getElementById("PFee25").style.display = "none";
//                                            document.getElementById("PFee50").style.display = "block";
//                                            document.getElementById("PFee75").style.display = "none";
//                                            document.getElementById("PFee100").style.display = "none";
//                                        }
//                                        if (sum === 75.00)
//                                        {
//                                            document.getElementById("PFee25").style.display = "none";
//                                            document.getElementById("PFee50").style.display = "none";
//                                            document.getElementById("PFee75").style.display = "block";
//                                            document.getElementById("PFee100").style.display = "none";
//                                        }
//                                        if (sum === 100.00)
//                                        {
//                                            document.getElementById("PFee25").style.display = "none";
//                                            document.getElementById("PFee50").style.display = "none";
//                                            document.getElementById("PFee75").style.display = "none";
//                                            document.getElementById("PFee100").style.display = "block";
//                                        }
//
//                                        if (sum === 10.00)
//                                        {
//                                            document.getElementById("PFee10").style.display = "block";
//                                            document.getElementById("PFee20").style.display = "none";
//                                            document.getElementById("PFee30").style.display = "none";
//                                            document.getElementById("PFee40").style.display = "none";
//                                        }
//
//                                        if (sum === 20.00)
//                                        {
//                                            document.getElementById("PFee10").style.display = "none";
//                                            document.getElementById("PFee20").style.display = "block";
//                                            document.getElementById("PFee30").style.display = "none";
//                                            document.getElementById("PFee40").style.display = "none";
//                                        }
//
//                                        if (sum === 30.00)
//                                        {
//                                            document.getElementById("PFee10").style.display = "none";
//                                            document.getElementById("PFee20").style.display = "none";
//                                            document.getElementById("PFee30").style.display = "block";
//                                            document.getElementById("PFee40").style.display = "none";
//                                        }
//
//                                        if (sum === 40.00)
//                                        {
//                                            document.getElementById("PFee10").style.display = "none";
//                                            document.getElementById("PFee20").style.display = "none";
//                                            document.getElementById("PFee30").style.display = "none";
//                                            document.getElementById("PFee40").style.display = "block";
//                                        }

                                    }
//                                    $("#Hindi").click(function () {
//                                    var HindiCheck = $('#Hindi').is(':checked');
//                                            var TamilCheck = $('#Tamil').is(':checked');
//                                            var BanglaCheck = $('#Bangla').is(':checked');
//                                            var GujaratiCheck = $('#Gujarati').is(':checked');
//                                            var sum = 0;
//                                            $("#fee").val(sum);
//                                            var h = t = b = g = 0;
//                                            if (HindiCheck === true)
//                                    {
//                                    h = 25.00;
//                                            sum = h + t + b + g;
//                                    }
//                                    if (TamilCheck === true)
//                                    {
//                                    t = 25.00;
//                                            sum = h + t + b + g;
//                                    }
//                                    if (BanglaCheck === true)
//                                    {
//                                    b = 25.00;
//                                            sum = h + t + b + g;
//                                    }
//                                    if (GujaratiCheck === true)
//                                    {
//                                    b = 25.00;
//                                            sum = h + t + b + g;
//                                    }
//                                    $("#fee").val(sum);
//                                            )}
//                                    $("#Hindi").click(function () {
//                                        var promoFile = $('#Hindi').is(':checked');
//                                        
//
//                                        if (promoFile == true)
//                                        {
//                                            document.getElementById("promocode").style.display = "block";
//                                            document.getElementById("apply").style.display = "block";
//                                            //    document.getElementById("promo25").style.display = "none";
////                                            document.getElementById("PFee25").style.display = "none";
//                                            document.getElementById("promo_sucess").style.display = "none";
//                                            document.getElementById("promo_error").style.display = "none";
//
//
//
//                                        } else
//                                        {
//                                            document.getElementById("promocode").style.display = "none";
//                                            document.getElementById("apply").style.display = "none";
//                                            //  document.getElementById("promo25").style.display = "block";
////                                            document.getElementById("PFee25").style.display = "block";
//                                            document.getElementById("promo_sucess").style.display = "none";
//                                            document.getElementById("promo_error").style.display = "none";
//
//                                        }


                                </script>

                                <input type="text"  class="form-control" name="fee" id="fee" style="display: none">


                                <lable id="PFee25" style="color: orange;font-size: 20px;">Participation Fee : <span id="spnAmt"> </span>$</lable> 
                                <!--                                <lable id="PFee50" style="color: orange;font-size: 20px; display: none">Participation Fee : 50$</lable> 
                                                                <lable id="PFee75" style="color: orange;font-size: 20px; display: none">Participation Fee : 75$</lable> 
                                                                <lable id="PFee100" style="color: orange;font-size: 20px; display: none">Participation Fee : 100$</lable> 
                                
                                                                <lable id="PFee15" style="color: orange;font-size: 20px;  display: none">Participation Fee : 15$</lable> 
                                                                <lable id="PFee30" style="color: orange;font-size: 20px;  display: none">Participation Fee : 30$</lable> 
                                                                <lable id="PFee45" style="color: orange;font-size: 20px;  display: none">Participation Fee : 45$</lable> 
                                                                <lable id="PFee60" style="color: orange;font-size: 20px;  display: none">Participation Fee : 60$</lable> 
                                
                                                                <lable id="PFee10" style="color: orange;font-size: 20px;  display: none">Participation Fee : 10$</lable> 
                                                                <lable id="PFee20" style="color: orange;font-size: 20px;  display: none">Participation Fee : 20$</lable> 
                                                                <lable id="PFee30" style="color: orange;font-size: 20px;  display: none">Participation Fee : 30$</lable> 
                                                                <lable id="PFee40" style="color: orange;font-size: 20px;  display: none">Participation Fee : 40$</lable> -->

                                <div class="form-group" >
                                    <div class="input-group input-group-lg" id="check" style="display: none">
                                        <input type="checkbox" name="checkpromo" id="checkpromo" value="Uncheck" /> I have a Promocode
                                    </div>
                                </div>
                                <script>
                                    $("#checkpromo").click(function () {
                                        var promoFile = $('#checkpromo').is(':checked');
                                        if (promoFile == true)
                                        {
                                            document.getElementById("promocode").style.display = "block";
                                            document.getElementById("apply").style.display = "block";
                                            //    document.getElementById("promo25").style.display = "none";
//                                            document.getElementById("PFee25").style.display = "none";
                                            document.getElementById("promo_sucess").style.display = "none";
                                            document.getElementById("promo_error").style.display = "none";
                                        } else
                                        {
                                            document.getElementById("promocode").style.display = "none";
                                            document.getElementById("apply").style.display = "none";
                                            //  document.getElementById("promo25").style.display = "block";
//                                            document.getElementById("PFee25").style.display = "block";
                                            document.getElementById("promo_sucess").style.display = "none";
                                            document.getElementById("promo_error").style.display = "none";
                                        }

                                    })
                                </script>


                                <lable id="promo_sucess" style="color: green; display: none">Promocode is sucessfully applied.</lable>
                                <lable id="promo_error" style="color: Red; display: none">Promocode is not applied.</lable>

                                    <div class="form-group" name="promocode" id="promocode" style="display: none">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user text-success"  style="color: #172f6a" ></span></span>
                                        <input type="text"  class="form-control" name="txtpromocode" id="txtpromocode" placeholder="Enter Promocode"  required/>
                                    </div>
                                </div>   
                                <div class="form-group">
                                    <button type="button" class="btn btn-block btn-success" name="apply" id="apply"style="display: none">Apply </button>
                                </div>




                                <lable id="pass1_error" style="color: Red; display: none">Please Enter Password</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-lock" style="color: #172f6a"></span></span>
                                        <input type="password" name="pass1" id="pass1" class="form-control" placeholder="Enter Password"/>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                    $("#pass1").blur(function () {
                                        if ($(this).val() == "") {
                                            document.getElementById("pass1_error").style.display = 'block';
                                            $("#pass1").focus();
                                        } else
                                        {
                                            document.getElementById("pass1_error").style.display = 'none';
                                        }

                                    });
                                        </script>
                                    </div>
                                </div>
                                <lable id="pass_error" style="color: Red; display: none">Passwords do not match.</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-lock" style="color: #172f6a"></span></span>
                                        <input type="password" name="Repass" id="Repass" class="form-control" placeholder="Re-Enter Password"/>
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    $("#Repass").blur(function () {
                                        var password = document.getElementById("pass1").value;
                                        var confirmPassword = document.getElementById("Repass").value;
                                        if (password != confirmPassword) {
                                            document.getElementById('pass_error').style.display = 'block';
                                            $("#Repass").focus();
                                        } else
                                        {
                                            document.getElementById('pass_error').style.display = 'none';
                                        }

                                    });
                                </script>
                                <lable id="terms_error" style="color: Red; display: none">Please Accept terms and conditions </lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <input type="checkbox" name="terms" id="terms" value="Uncheck"/> Accept Rules and Regulations
                                        <span><a href = 'rules.php' a target="_blank" id="File1" name="File1" style="color:blue" required>Read</a></span>
                                    </div>
                                </div>
                                <lable id="privacy_error" style="color: Red; display: none">Please Accept Privacy Policies </lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <input type="checkbox" name="privacy" id="privacy" value="Uncheck"/> Privacy Policies 
                                        <span><a href = 'Privacy_Policy.php' a target="_blank"  id="File2" name="File2" style="color:blue" required>Read</a></span>
                                    </div>
                                </div>
                                <input type="hidden" name="cmd" value="_xclick" />
                                <input type="hidden" name="no_note" value="1" />
                                <input type="hidden" name="lc" value="UK" />
                                <input type="hidden" name="currency_code" value="SGD" />
                                <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
                                <input type="hidden" name="first_name" value="Customer's First Name"  />
                                <input type="hidden" name="last_name" value="Customer's Last Name"  />
                                <input type="hidden" name="payer_email" value="customer@example.com"  />
                                <input type="hidden" name="item_number" value="123456" />

                                <div class="form-group">
<!--                                        <input class="btn btn-block btn-success" type="submit" name="submit" id="signup" value="Submit Payment"/>-->
                                    <button type="submit" class="btn btn-block btn-success" name="signup" id="signup">Submit Payment </button>
                                </div>
                            </form>     
                            <div id="popup1"style="display: none">
                                <b style="font-size: 15px; text-align: center"></b> <br>
                                <div class="content" style="text-align: center">                               
                                    <br><br><b style="font-size: 15px; text-align: center">Thank you ! Your registration has been recorded successfully. Please note submission ends on 26 Jan 18</b>
                                    <form action="index.php">
                                        <br><br><button type="submit" class="btn btn-block btn-success">Back </button>
                                    </form>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6" style="background-color: #f7f7f7"> 
                            <p class="nino-sectionDesc" style="color: black;">
                                <br><br>
                                <b style="font-size: 15px; text-align: center">Storytelling competition in Hindi, Tamil, Bangla and Gujarati. <br>   For Singapore participants only </b>
                             
                            </p>

                            <p style="color: black"> Participate and partner in a nationwide campaign to revive our Folk Tales & Folk Art by simply filling in the registration form. For further information <a href = 'rules.php' a target="_blank"  id="faq" name="faq" style="color:blue">Click Here </a><br></p>

                            <b style="color: black">Theme of Competition : </b><br>
                            <p style="padding-left: 30px; color: black;">   •   Folk Tales Retold in Modern Times</p>  
                            <b style="color: black">Age Groups :</b><br> 
                            <p style="padding-left: 30px; color: black;">   •   Child : 8-12 </p>
                            <p style="padding-left: 30px; color: black;">   •  Youth :13-17</p>
                            <p style="padding-left: 30px; color: black;">   •   Adults : 18 & above</p>

                            <b style="color: black"> Key Dates to Remember : </b><br> 
                            <p style="padding-left: 30px; color: black;">   •   Registration ends : 25 Jan 2018 </p>
                            <p style="padding-left: 30px; color: black;">   •   Submission ends : 26 Jan 2018</p>
                            <p style="padding-left: 30px; color: black;">   •   Semi Finals/Finals :10 & 11 Feb 2018</p>
                            <p style="padding-left: 30px; color: black;">   •   Prerna Awards ceremony:24 Feb 2018</p>
                        </div>
                    </div>
                </div>
            </div>

            <!--            <div id="popup">
                <Center>
                    Record added successfully 
                        </Center>
                        </div>-->
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js_1/tos.js"></script>
            <script src="js_1/moment.js" type="text/javascript"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>

            <script>
                                    $(function () {
                                        $('#dob').datetimepicker({
                                            format: 'DD-MM-YYYY'
                                        });
//                                        $('#dob').on('dp.change', function (e) {
//
//                                            var d = new Date(e.date);
//                                            var year = d.getFullYear();
//                                            var today = new Date();
//                                            var today_year = today.getFullYear();
//                                            var age = today_year - year;
//                                            if (age <= 18)
//                                            {
//                                                document.getElementById('M_lable').style.display = 'block';
//                                                document.getElementById('P_name').style.display = 'block';
//                                                document.getElementById('p_email').style.display = 'block';
//                                                document.getElementById('country').style.display = 'block';
//                                            } else
//                                            {
//                                                document.getElementById('M_lable').style.display = 'none';
//                                                document.getElementById('P_name').style.display = 'none';
//                                                document.getElementById('p_email').style.display = 'none';
//                                                document.getElementById('country').style.display = 'none';
//                                            }
//                                        });
                                    });
            </script>


            <script type="text/javascript">


                //               $("#terms").click(function () {
                //                    var terms = $('#terms').is(':checked');
                //                    if(terms=="true")
                //                    $("#terms").val("check");
                //                else
                //                       $("#terms").val("Uncheck");
                //                
                //                });
                //
                //                $("#privacy").click(function () {
                //                    var privacy = $('#privacy').is(':checked');
                //                       if(terms=="true")
                //                          $("#privacy").val("check");
                //                      else
                //                       $("#privacy").val("Uncheck");
                //                });


                $("#signup").click(function () {
                    var uname = $("#uname").val();
                    if ($("#uname").val() == "") {
                        document.getElementById("name_error").style.display = 'block';
                        $("#uname").focus();
                        return false;
                    } else
                    {
                        document.getElementById("name_error").style.display = 'none';
                    }

                    if ($("#phone").val() == "") {
                        document.getElementById("phone_error").style.display = 'block';
                        $("#phone").focus();
                        return false;
                    } else
                    {
                        document.getElementById("phone_error").style.display = 'none';
                    }
                    var emil = $('#email').val();
                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if (!emailReg.test(emil)) {
                        document.getElementById("Email_error").style.display = 'block';
                        $("#Email_error").text("Please enter valid email");
                        $("#email").focus();
                    } else if ($('#email').val() == "")
                    {
                        document.getElementById("Email_error").style.display = 'block';
                        $("#Email_error").text("Enter Email Id");
                        $("#email").focus();
                    }
                    if ($("#dob").val() == "") {
                        document.getElementById("dob_error").style.display = 'block';
                        $("#dob").focus();
                    } else
                    {
                        document.getElementById("dob_error").style.display = 'none';
                    }
                    if ($("#cntry").val() == "") {
                        document.getElementById("country_error").style.display = 'block';
                        $("#cntry").focus();
                    } else
                    {
                        document.getElementById("country_error").style.display = 'none';
                    }

                    $.ajax({
                        url: 'Check_email.php',
                        type: "POST",
                        data: ({Email: $("#email").val()}),
                        async: false,
                        success: function (data) {
                            console.log(data);
                            var str = $.trim(data);
                            if (str === "success")
                            {


                                if ($("#pass1").val() == "") {
                                    document.getElementById("pass1_error").style.display = 'block';
                                    $("#pass1").focus();
                                } else
                                {
                                    document.getElementById("pass1_error").style.display = 'none';
                                }

                                var password = document.getElementById("pass1").value;
                                var confirmPassword = document.getElementById("Repass").value;
                                if (password != confirmPassword) {
                                    document.getElementById('pass_error').style.display = 'block';
                                    $("#Repass").focus();
                                } else
                                {
                                    document.getElementById('pass_error').style.display = 'none';
                                }

                                var terms = $('#terms').is(':checked'); //true
                                var privacy = $('#privacy').is(':checked'); // false
                                if (terms == true && privacy == true)
                                {
                                    document.getElementById('terms_error').style.display = 'none';
                                    document.getElementById('privacy_error').style.display = 'none';
                                } else if (terms == false && privacy == true)
                                {
                                    document.getElementById('terms_error').style.display = 'block';
                                    document.getElementById('privacy_error').style.display = 'none';
                                    return false;
                                } else if (terms == false && privacy == false) {
                                    document.getElementById('terms_error').style.display = 'block';
                                    document.getElementById('privacy_error').style.display = 'block';
                                    return false;
                                } else if (terms == true && privacy == false)
                                {
                                    document.getElementById('terms_error').style.display = 'none';
                                    document.getElementById('privacy_error').style.display = 'block';
                                    return false;
                                }


                                document.getElementById("Email_error").style.display = 'none';
                                $("#freg_id").submit();
                            } else
                            {
                                document.getElementById('PresentEmail_error').style.display = 'block';
                                return false;
                            }


                        }
                    });
//                    $.ajax({
//                        url: 'RegSuccessMail.php', //This is the current doc
//                        type: "POST",
//                        data: ({NAME: uname, EMAIL: emil}),
//                        //data: ({ NAME: 'AAA' }),
//                        success: function (data) {
//                            console.log(data);
//                            alert("Thank you.. Our team will contact you soon..");
//                        }
//                    });
                    return false;
                }
                );
            </script>


            <script>
                $(function () {
                    $("#Language").change(function () {
                        //               var selectedText = $(this).find("option:selected").text();
                        //                var selectedValue = $(this).val();
                        //                alert("Selected Text: " + selectedText + " Value: " + selectedValue);
                        LoadComp();
                    });
                    LoadComp();
                });
                function LoadComp() {
                    $.ajax({
                        url: 'Reg_responce.php',
                        type: "POST",
                        data: ({Language: $("#Language").val()}),
                        success: function (data) {
                            console.log(data);
                            $("#div_comp").html(data);
                        }
                    });
                }
            </script>
        </section><!--/#nino-services-->


        <!--  Slider                                                              ================================================== -->
<!--            <section id="nino-uniqueDesign">
         <div class="sectionContent">
             <div id="myCarousel" class="carousel slide" data-ride="carousel">
                 <div class="carousel-inner">
                     <div class="item active">
                         <div class="col-md-4"></div>
                         <div class="col-md-4">                    
                             <img src="images/Slider/genie.jpg" alt=""/>
                         </div>
                         <div class="col-md-4"></div>
                     </div>
                     <div class="item">
                         <div class="col-md-4"></div>
                         <div class="col-md-4">
                             <img src="images/Slider/Prerna-D1-04.png" alt=""/>
                         </div>
                         <div class="col-md-4"></div>
                     </div>
                     <div class="item">
                         <div class="col-md-4"></div>
                         <div class="col-md-4">
                             <img src="images/Slider/Prerna-D1-07.png" alt=""/>
                         </div>
                         <div class="col-md-4"></div>
                     </div>
                     <div class="item">
                         <div class="col-md-4"></div>
                         <div class="col-md-4">
                             <img src="images/Slider/Prerna-D1-08.png" alt=""/>
                         </div>
                         <div class="col-md-4"></div>
                     </div>
                     <div class="item">
                         <div class="col-md-4"></div>
                         <div class="col-md-4">
                             <img src="images/Slider/Prerna-D1-09.png" alt=""/>
                         </div>
                         <div class="col-md-4"></div>
                     </div>
                 </div>
                 <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                     <span class="glyphicon glyphicon-chevron-left"></span>
                     <span class="sr-only">Previous</span>
                 </a>
                 <a class="right carousel-control" href="#myCarousel" data-slide="next">
                     <span class="glyphicon glyphicon-chevron-right"></span>
                     <span class="sr-only">Next</span>
                 </a>
             </div>
         </div>
     </section>/#nino-uniqueDesign-->

        <!-- What We Do
        ================================================== -->
<!--        <section id="nino-whatWeDo">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">Service</span>
                    what we do
                </h2>
                <p class="nino-sectionDesc">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                </p>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                <img src="images/what-we-do/w1.jpeg" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="mdi mdi-chevron-up nino-icon arrow"></i>
                                                <i class="mdi mdi-camera nino-icon"></i> 
                                                Photography
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <i class="mdi mdi-chevron-up nino-icon arrow"></i>
                                                <i class="mdi mdi-owl nino-icon"></i> 
                                                creativity
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <i class="mdi mdi-chevron-up nino-icon arrow"></i>
                                                <i class="mdi mdi-laptop-mac nino-icon"></i> 
                                                web design
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>/#nino-whatWeDo-->

        <!-- Testimonial
        ================================================== -->


        <!-- Our Team
        ================================================== -->
<!--            <section id="nino-ourTeam">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">Who we are</span>
                    Meet our Advisors
                </h2>
                <p class="nino-sectionDesc">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                </p>
                <div class="sectionContent">
                    <div class="row nino-hoverEffect">
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="overlay" href="#">
                                    <div class="content">                                      
                                        <h2 class="nino-sectionHeading" style="color: white">AABID SURTI </h2>
                                            <a href="#" class="nino-icon"><i class="mdi mdi-facebook"></i></a>
                                          <a href="#" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                          <a href="#" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                          <a href="#" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                    </div>
                                    <img src="images/our-team/22.jpg" alt="">
                                </div>
                            </div>
                            <div class="info">
                                <h4 class="name">AABID SURTI</h4>
                                    <span class="regency">Graphic Design</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="overlay" href="#">
                                    <div class="content">
                                        <h2 class="nino-sectionHeading" style="color: white">DR.KUMAR ARUNODAYA </h2>
                                            <a href="#" class="nino-icon"><i class="mdi mdi-facebook"></i></a>
                                         <a href="#" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                         <a href="#" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                         <a href="#" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                    </div> 
                                    <img src="images/our-team/33.jpg" alt="">
                                </div>
                            </div>
                            <div class="info">
                                <h4 class="name">DR.KUMAR ARUNODAYA </h4>
                                    <span class="regency">Branding/UX design</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="overlay" href="#">
                                    <div class="content">
                                        <h2 class="nino-sectionHeading" style="color: white">DR.MADHU PANT</h2>
                                            <a href="#" class="nino-icon"><i class="mdi mdi-facebook"></i></a>
                                 <a href="#" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                 <a href="#" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                 <a href="#" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                    </div>
                                    <img src="images/our-team/44.jpg" alt="">
                                </div>
                            </div>
                            <div class="info">
                                <h4 class="name">DR.MADHU PANT </h4>
                                <span class="regency">Developer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>/#nino-ourTeam-->

        <!-- Brand
    ================================================== -->
<!--        <section id="nino-brand">
            <div class="container">
                <div class="verticalCenter fw" layout="row">
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-1.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-2.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-3.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-4.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-5.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-6.png" alt=""></a></div>
                </div>
            </div>
        </section>/#nino-brand-->

        <!-- Portfolio
    ================================================== -->
        <section id="nino-portfolio">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading"></span>
                    Our Past Events
                </h2>
                <p class="nino-sectionDesc">
                    Through this competition we like to foster excellence in creativity by providing a platform to reinforce talent of expression and creative writing skills. We wish that this would encourage others to write and express themselves. Language, one of the most important skill in the 21st century, is not just school work, but an effective tool for communication and self-expression.
                </p>
            </div>
            <div class="sectionContent">
                <ul class="nino-portfolioItems">
                    <li class="item" style="padding: 3px">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="" href="images/our-work/nn.jpg">
                            <img src="images/our-work/nn.jpg" />
                            <!--                                <div class="overlay">
                                                                <div class="content">
                                                                    <i class="mdi mdi-crown nino-icon"></i>
                                                                    <h4 class="title">Past Events</h4>
                                                                    <span class="desc">Lorem ipsum dolor sit</span>
                                                                </div>
                                                            </div>-->
                        </a>
                    </li>
                    <li class="item" style="padding: 3px">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="" href="images/our-work/n.jpg">
                            <img src="images/our-work/n.jpg" />
                            <!--                                <div class="overlay">
                                                                <div class="content">
                                                                    <i class="mdi mdi-crown nino-icon"></i>
                                                                    <h4 class="title">Past Events</h4>
                                                                    <span class="desc">Lorem ipsum dolor sit</span>
                                                                </div>
                                                            </div>-->
                        </a>
                    </li>
                    <li class="item" style="padding: 3px">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="" href="images/our-work/7.jpg">
                            <img src="images/our-work/7.jpg" />
                            <!--                                <div class="overlay">
                                                                <div class="content">
                                                                    <i class="mdi mdi-crown nino-icon"></i>
                                                                    <h4 class="title">Past Events</h4>
                                                                    <span class="desc">Lorem ipsum dolor sit</span>
                                                                </div>
                                                            </div>-->
                        </a>
                    </li>
                    <li class="item" style="padding: 3px">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="" href="images/our-work/99.jpg">
                            <img src="images/our-work/99.jpg" />
                            <!--                                <div class="overlay">
                                                                <div class="content">
                                                                    <i class="mdi mdi-crown nino-icon"></i>
                                                                    <h4 class="title">Past Events</h4>
                                                                    <span class="desc">Lorem ipsum dolor sit</span>
                                                                </div>
                                                            </div>-->
                        </a>
                    </li>
                    <li class="item" style="padding: 3px">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="" href="images/our-work/77.jpg">
                            <img src="images/our-work/77.jpg" />
                            <!--                                <div class="overlay">
                                                                <div class="content">
                                                                    <i class="mdi mdi-crown nino-icon"></i>
                                                                    <h4 class="title">Past Events</h4>
                                                                    <span class="desc">Lorem ipsum dolor sit</span>
                                                                </div>
                                                            </div>-->
                        </a>
                    </li>
                    <li class="item" style="padding: 3px">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="" href="images/our-work/66.jpg">
                            <img src="images/our-work/66.jpg" />
                            <!--                                <div class="overlay">
                                                                <div class="content">
                                                                    <i class="mdi mdi-crown nino-icon"></i>
                                                                    <h4 class="title">Past Events</h4>
                                                                    <span class="desc">Lorem ipsum dolor sit</span>
                                                                </div>
                                                            </div>-->
                        </a>
                    </li>
                    <li class="item" style="padding: 3px">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="" href="images/our-work/33.jpg">
                            <img src="images/our-work/33.jpg" />
                            <!--                                <div class="overlay">
                                                                <div class="content">
                                                                    <i class="mdi mdi-crown nino-icon"></i>
                                                                    <h4 class="title">Past Events</h4>
                                                                    <span class="desc">Lorem ipsum dolor sit</span>
                                                                </div>
                                                            </div>-->
                        </a>
                    </li>
                </ul>
            </div>
        </section><!--/#nino-portfolio-->


        <section id="nino-Contact">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    Contact Us
                </h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="contact-text">
                            <h2>Our main office</h2><br>
                            <address style="color: black; font-size: 15px">

                                SINGAPORE LANGUAGE ORGANISATION <br>
                                Loyang Avenue, Singapore. <br><br>                                        

                                <b>  Phone : </b> +65-98877117<br>
                                <b> E-mail : </b><a href="mailto:singaporelanguage@gmail.com"> singaporelanguage@gmail.com</a>
                                <div style="padding-top: 20px">
                                    <form method="post" autocomplete="off" action="volunteer.php" enctype="multipart/form-data"class="nino-subscribeForm">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-success" name="signup12" id="signup12">Join As Volunteer </button>
                                        </div>
                                    </form>
                                </div>

                                <div>
                                    <form method="post" autocomplete="off" action="mailchimp.php" enctype="multipart/form-data"class="nino-subscribeForm">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-success" name="signup12" id="signup12">Join As Member </button>
                                        </div>
                                    </form>
                                </div>


                            </address> 
                        </div>

                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-sm-6">
                        <div id="contact-section">
                            <h2 style="text-align: center"> Send a message </h2><br>
                            <div class="status alert alert-success" style="display: none"></div>
                            <form id="main-contact-form" class="contact-form" name="contact-form" method="post" onsubmit="return false;">
                                <div class="to">
                                    <div class="form-group">
                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"> <span class="glyphicon glyphicon-user" style="color: #172f6a"></span></span>
                                            <input type="text" name="Contact_Name" id="Contact_Name" class="form-control" placeholder="Your Name"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"> <span class="glyphicon glyphicon-envelope text-success" style="color: #172f6a"></span></span>
                                            <input type="email" name="Contact_email" id="Contact_email" class="form-control" placeholder="Email Id"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="text">
                                    <div class="form-group">
                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"> <span class="glyphicon glyphicon-list-alt" style="color: #172f6a"></span></span>
                                            <textarea  name="Contact_message" id="Contact_message" class="form-control" placeholder="Your Message"></textarea>
                                        </div>
                                    </div>          

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success" name="Contact_Submit" id="Contact_Submit">Submit </button>
                                    </div>
                                </div>

                            </form>	    
                            <div id="contactPopup"style="display: none">
                                <b style="font-size: 15px; text-align: center"></b> <br>
                                <div class="content" style="text-align: center">                               
                                    <br><br><b style="font-size: 15px; text-align: center">Thank you ! Team Prerna will contact you soon.</b>
                                    <form action="index.php">
                                        <br><br><button type="submit" class="btn btn-block btn-success">Back </button>
                                    </form>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Testimonial
    ================================================== -->
<!--        <section class="nino-testimonial bg-white">
            <div class="container">
                <div class="nino-testimonialSlider">
                    <ul>
                        <li>
                            <div layout="row" class="verticalCenter">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle img-thumbnail" src="images/testimonial/img-1.jpg" alt="">
                                </div>
                                <div>
                                    <p class="quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation."</p>
                                    <span class="name">Joshua Earle</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div layout="row" class="verticalCenter">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle img-thumbnail" src="images/testimonial/img-2.jpg" alt="">
                                </div>
                                <div>
                                    <p class="quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation."</p>
                                    <span class="name">Jon Doe</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div layout="row" class="verticalCenter">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle img-thumbnail" src="images/testimonial/img-3.jpg" alt="">
                                </div>
                                <div>
                                    <p class="quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation."</p>
                                    <span class="name">Jon Doe</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>/#nino-testimonial-->

        <!-- Happy Client
        ================================================== -->
<!--        <section id="nino-happyClient">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">Happy Clients</span>
                    What people say
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6">
                            <div layout="row" class="item">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle" src="images/happy-client/img-1.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h4 class="name">Matthew Dix</h4>
                                    <span class="regency">Graphic Design</span>
                                    <p class="desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo illo cupiditate temporibus sapiente, sint, voluptatibus tempora esse. Consectetur voluptate nihil quo nulla voluptatem dolorem harum nostrum
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div layout="row" class="item">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle" src="images/happy-client/img-2.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h4 class="name">Nick Karvounis</h4>
                                    <span class="regency">Graphic Design</span>
                                    <p class="desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo illo cupiditate temporibus sapiente, sint, voluptatibus tempora esse. Consectetur voluptate nihil quo nulla voluptatem dolorem harum nostrum
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div layout="row" class="item">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle" src="images/happy-client/img-3.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h4 class="name">Jaelynn Castillo</h4>
                                    <span class="regency">Graphic Design</span>
                                    <p class="desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo illo cupiditate temporibus sapiente, sint, voluptatibus tempora esse. Consectetur voluptate nihil quo nulla voluptatem dolorem harum nostrum
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div layout="row" class="item">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle" src="images/happy-client/img-4.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h4 class="name">Mike Petrucci</h4>
                                    <span class="regency">Graphic Design</span>
                                    <p class="desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo illo cupiditate temporibus sapiente, sint, voluptatibus tempora esse. Consectetur voluptate nihil quo nulla voluptatem dolorem harum nostrum
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>/#nino-happyClient-->

        <!-- Latest Blog
        ================================================== -->
<!--        <section id="nino-latestBlog">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">Our stories</span>
                    Latest Blog
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <article>
                                <div class="articleThumb">
                                    <a href="#"><img src="images/our-blog/img-1.jpg" alt=""></a>
                                    <div class="date">
                                        <span class="number">15</span>
                                        <span class="text">Jan</span>
                                    </div>
                                </div>
                                <h3 class="articleTitle"><a href="">Lorem ipsum dolor sit amet</a></h3>
                                <p class="articleDesc">
                                    Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="articleMeta">
                                    <a href="#"><i class="mdi mdi-eye nino-icon"></i> 543</a>
                                    <a href="#"><i class="mdi mdi-comment-multiple-outline nino-icon"></i> 15</a>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <article>
                                <div class="articleThumb">
                                    <a href="#"><img src="images/our-blog/img-2.jpg" alt=""></a>
                                    <div class="date">
                                        <span class="number">14</span>
                                        <span class="text">Jan</span>
                                    </div>
                                </div>
                                <h3 class="articleTitle"><a href="">sed do eiusmod tempor</a></h3>
                                <p class="articleDesc">
                                    Adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="articleMeta">
                                    <a href="#"><i class="mdi mdi-eye nino-icon"></i> 995</a>
                                    <a href="#"><i class="mdi mdi-comment-multiple-outline nino-icon"></i> 42</a>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <article>
                                <div class="articleThumb">
                                    <a href="#"><img src="images/our-blog/img-3.jpg" alt=""></a>
                                    <div class="date">
                                        <span class="number">12</span>
                                        <span class="text">Jan</span>
                                    </div>
                                </div>
                                <h3 class="articleTitle"><a href="">incididunt ut labore et dolore</a></h3>
                                <p class="articleDesc">
                                    Elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="articleMeta">
                                    <a href="#"><i class="mdi mdi-eye nino-icon"></i> 1264</a>
                                    <a href="#"><i class="mdi mdi-comment-multiple-outline nino-icon"></i> 69</a>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>/#nino-latestBlog-->

        <!-- Map
        ================================================== -->
<!--        <section id="nino-map">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <i class="mdi mdi-map-marker nino-icon"></i>
                    <span class="text">Open map</span>
                    <span class="text" style="display: none;">Close map</span>
                </h2>
                <div class="mapWrap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d79466.26604960626!2d-0.19779784176715043!3d51.50733004537892!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C+UK!3m2!1d51.5073509!2d-0.1277583!5e0!3m2!1sen!2s!4v1469206441744" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </section>/#nino-map-->

        <!-- Footer
        ================================================== -->
        <footer id="footer">
            <!--            <div class="container">
                            <div class="row">
                                <div class="colInfo">
                                    
                                    <div class="col-md-10" style="font-size: 17px ;padding-left: 20px">
                     
                                       
                                            
                                                <ul class="nav navbar-nav">
                                                    <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                                    <li><a href="Terms.php" a target="_blank"  style="color: #172f6a">Terms of Services</a></li>
                                                 
                                                </ul>
                                          
                                        </div>
                                  
                                       <div class="col-md-3" style="padding-left: 30px">
                                    <div class="nino-followUs">
                                        <div class="socialNetwork">
                                            <span class="text">Follow Us: </span>
                                            <a href="www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
                                            <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                            <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                                                        <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                            <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                            <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>
                                                                        <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                            <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>
                                        </div>
                                    </div>
                                                 </div>
                                    </div>
                                </div>
            
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="colInfo">
                                                            <h3 class="nino-colHeading">Blogs</h3>
                                                            <ul class="listArticles">
                                                                <li layout="row" class="verticalCenter">
                                                                    <a class="articleThumb fsr" href="#"><img src="images/our-blog/img-4.jpg" alt=""></a>
                                                                    <div class="info">
                                                                        <h3 class="articleTitle"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing</a></h3>
                                                                        <div class="date">Jan 9, 2016</div>
                                                                    </div>
                                                                </li>
                                                                <li layout="row" class="verticalCenter">
                                                                    <a class="articleThumb fsr" href="#"><img src="images/our-blog/img-5.jpg" alt=""></a>
                                                                    <div class="info">
                                                                        <h3 class="articleTitle"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing</a></h3>
                                                                        <div class="date">Jan 9, 2016</div>
                                                                    </div>
                                                                </li>
                                                                <li layout="row" class="verticalCenter">
                                                                    <a class="articleThumb fsr" href="#"><img src="images/our-blog/img-6.jpg" alt=""></a>
                                                                    <div class="info">
                                                                        <h3 class="articleTitle"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing</a></h3>
                                                                        <div class="date">Jan 9, 2016</div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="colInfo">
                                                            <h3 class="nino-colHeading">instagram</h3>
                                                            <div class="instagramImages clearfix">
                                                                <a href="#"><img src="images/instagram/img-1.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-2.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-3.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-4.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-5.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-6.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-7.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-8.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-9.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-3.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-4.jpg" alt=""></a>
                                                                <a href="#"><img src="images/instagram/img-5.jpg" alt=""></a>
                                                            </div>
                                                            <a href="#" class="morePhoto">View more photos</a>
                                                        </div>
                                                    </div>
                            </div>-->
            <div class="nino-copyright">
                <div class="row">
                    <div class="colInfo">
                        <div class="col-md-1"></div>
                        <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">



                            <ul class="nav navbar-nav">
                                <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                                <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                            </ul>

                        </div>

                        <div class="col-md-3" style="padding-left: 30px">
                            <div class="nino-followUs">
                                <div class="socialNetwork">
                                    <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                    <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
    <!--                                <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                                                <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>-->
    <!--                                                            <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
        </div>
    </footer>

    <!-- Search Form - Display when click magnify icon in menu
    ================================================== -->
    <form action="" id="nino-searchForm">
        <input type="text" placeholder="Search..." class="form-control nino-searchInput">
        <i class="mdi mdi-close nino-close"></i>
    </form><!--/#nino-searchForm-->

    <!-- Scroll to top
    ================================================== -->
    <a href="#" id="nino-scrollToTop">Go to Top</a>

    <!-- javascript -->
    <script type="text/javascript" src="js/jquery.min.js"></script>	
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<!--            <script type="text/javascript" src="js/bootstrap.min.js"></script>-->
<!--            <script src="js/bootstrap.js" type="text/javascript"></script>-->
    <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/unslider-min.js"></script>
    <script type="text/javascript" src="js/template.js"></script>
    <script src="js_1/moment.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="js/contact.js" type="text/javascript"></script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- css3-mediaqueries.js for IE less than 9 -->
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <script>
                                $("#apply").click(function () {


                                    $.ajax({
                                        url: 'applycode.php',
                                        type: "POST",
                                        data: ({Promo: $('#txtpromocode').val()}),
                                        async: false,
                                        success: function (data) {
                                            console.log(data);
                                            var str = $.trim(data);
                                            if (str === "success")
                                            {

                                                var sum = 0;
                                                $("#fee").val(sum);
                                                var h = t = b = g = 0;
                                                if (document.getElementById('Hindi').checked)
                                                {
                                                    h = 15.00;
                                                    sum = h + t + b + g;
                                                    $("#fee").val(sum);
                                                }
                                                if (document.getElementById('Tamil').checked)
                                                {
                                                    t = 15.00;
                                                    sum = h + t + b + g;
                                                    $("#fee").val(sum);
                                                }
                                                if (document.getElementById('Bangla').checked)
                                                {
                                                    b = 15.00;
                                                    sum = h + t + b + g;
                                                    $("#fee").val(sum);
                                                }
                                                if (document.getElementById('Gujarati').checked)
                                                {
                                                    g = 15.00;
                                                    sum = h + t + b + g;
                                                    $("#fee").val(sum);
                                                }
                                                $("#spnAmt").html(sum);
                                                document.getElementById("check").style.display = "none";
                                                document.getElementById("txtpromocode").disabled = "disabled";
                                                document.getElementById("promo_sucess").style.display = "block";
                                                document.getElementById("promo_error").style.display = "none"
                                                document.getElementById("apply").style.display = "none";
//                                                if (sum === 15.00)
//                                                {
//                                                    document.getElementById("PFee15").style.display = "block";
//                                                    document.getElementById("PFee30").style.display = "none";
//                                                    document.getElementById("PFee45").style.display = "none";
//                                                    document.getElementById("PFee60").style.display = "none";
//                                                    document.getElementById("promo_sucess").style.display = "block";
//                                                    document.getElementById("PFee25").style.display = "none";
//                                                    document.getElementById("PFee50").style.display = "none";
//                                                    document.getElementById("PFee75").style.display = "none";
//                                                    document.getElementById("PFee100").style.display = "none";
//                                                    document.getElementById("txtpromocode").disabled = "disabled";
//                                                    document.getElementById("check").style.display = "none";
//                                                    document.getElementById("promo_error").style.display = "none";
//                                                }
//                                                if (sum === 30.00)
//                                                {
//                                                    document.getElementById("PFee15").style.display = "none";
//                                                    document.getElementById("PFee30").style.display = "block";
//                                                    document.getElementById("PFee45").style.display = "none";
//                                                    document.getElementById("PFee60").style.display = "none";
//                                                    document.getElementById("promo_sucess").style.display = "block";
//                                                    document.getElementById("PFee25").style.display = "none";
//                                                    document.getElementById("PFee50").style.display = "none";
//                                                    document.getElementById("PFee75").style.display = "none";
//                                                    document.getElementById("PFee100").style.display = "none";
//                                                    document.getElementById("txtpromocode").disabled = "disabled";
//                                                    document.getElementById("check").style.display = "none";
//                                                    document.getElementById("promo_error").style.display = "none";
//                                                }
//                                                if (sum === 45.00)
//                                                {
//                                                    document.getElementById("PFee15").style.display = "none";
//                                                    document.getElementById("PFee30").style.display = "none";
//                                                    document.getElementById("PFee45").style.display = "block";
//                                                    document.getElementById("PFee60").style.display = "none";
//                                                    document.getElementById("promo_sucess").style.display = "block";
//                                                    document.getElementById("PFee25").style.display = "none";
//                                                    document.getElementById("PFee50").style.display = "none";
//                                                    document.getElementById("PFee75").style.display = "none";
//                                                    document.getElementById("PFee100").style.display = "none";
//                                                    document.getElementById("txtpromocode").disabled = "disabled";
//                                                    document.getElementById("check").style.display = "none";
//                                                    document.getElementById("promo_error").style.display = "none";
//                                                }
//                                                if (sum === 60.00)
//                                                {
//                                                    document.getElementById("PFee15").style.display = "none";
//                                                    document.getElementById("PFee30").style.display = "none";
//                                                    document.getElementById("PFee45").style.display = "none";
//                                                    document.getElementById("PFee60").style.display = "block";
//                                                    document.getElementById("promo_sucess").style.display = "block";
//                                                    document.getElementById("PFee25").style.display = "none";
//                                                    document.getElementById("PFee50").style.display = "none";
//                                                    document.getElementById("PFee75").style.display = "none";
//                                                    document.getElementById("PFee100").style.display = "none";
//                                                    document.getElementById("txtpromocode").disabled = "disabled";
//                                                    document.getElementById("check").style.display = "none";
//                                                    document.getElementById("promo_error").style.display = "none";
//                                                }


                                                //   document.getElementById("promo10").style.display = "block";
                                                //    document.getElementById("promo25").style.display = "none";




//                                                 document.getElementById("promo_sucess").style.display = "block";
//                                                document.getElementById("promo_error").style.display = "none";
//                                                document.getElementById("PFee15").style.display = "block";
//                                                document.getElementById("PFee25").style.display = "none";
//                                                document.getElementById("apply").style.display = "none";
//                                                document.getElementById("check").style.display = "none";
//                                                document.getElementById("txtpromocode").disabled = "disabled"





                                            } else
                                            {
                                                document.getElementById("apply").style.display = "block";
                                                document.getElementById("check").style.display = "block"
                                                document.getElementById("promo_error").style.display = "block";
                                                document.getElementById("promo_sucess").style.display = "none";
                                                document.getElementById("apply").style.display = "block";
                                            }
                                            //else
//                                        {
//                                        //     document.getElementById("promo10").style.display = "none";
//                                        //   document.getElementById("promo25").style.display = "block";
//                                        document.getElementById("promo_sucess").style.display = "none";
//                                                document.getElementById("promo_error").style.display = "block";
//                                                document.getElementById("PFee15").style.display = "none";
//                                                document.getElementById("PFee25").style.display = "block";
//                                                document.getElementById("apply").style.display = "block";
//                                                document.getElementById("check").style.display = "block";
//                                        }
                                        }
                                    });
                                });
    </script>
</body>
</html>
<?php
$recordAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
}

if ($recordAdded) {
    echo '
<script type="text/javascript">
     
    document.getElementById("freg_id").style.display = "none";
    document.getElementById("popup1").style.display = "block";
  
</script>';
}
?>