<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Mogo | OnePage Responsive Theme</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">

        <!-- Header
    ================================================== -->
        <header id="nino-header">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="homepage.html">Mogo</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#nino-header">Home <span class="sr-only">(current)</span></a></li>
                                    <li><a href="#nino-story">About</a></li>
                                    <li><a href="#registration">Registration</a></li>
                                    <li><a href="#nino-ourTeam">Our Team</a></li>
                                    <li><a href="#nino-portfolio">Work</a></li>
                                    <li><a href="#nino-latestBlog">Blog</a></li>
                                    <li><a onclick="window.location.href = 'Login.php'">Login</a></li>


                                </ul>
                            </div><!-- /.navbar-collapse -->
                            <ul class="nino-iconsGroup nav navbar-nav">
                                <li><a href="#"><i class="mdi mdi-cart-outline nino-icon"></i></a></li>
                                <li><a href="#" class="nino-search"><i class="mdi mdi-magnify nino-icon"></i></a></li>
                            </ul>
                        </div>
                    </div><!-- /.container-fluid -->
                </nav>

                <section id="nino-slider" class="carousel slide container" data-ride="carousel">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <h2 class="nino-sectionHeading">
                                <span class="nino-subHeading">Creative Template</span>
                                Welcome <br>to MoGo
                            </h2>
                            <a href="#" class="nino-btn">Learn more</a>
                        </div>
                        <div class="item">
                            <h2 class="nino-sectionHeading">
                                <span class="nino-subHeading">Creative Template</span>
                                Welcome <br>to MoGo
                            </h2>
                            <a href="#" class="nino-btn">Learn more</a>
                        </div>
                        <div class="item">
                            <h2 class="nino-sectionHeading">
                                <span class="nino-subHeading">Creative Template</span>
                                Welcome <br>to MoGo
                            </h2>
                            <a href="#" class="nino-btn">Learn more</a>
                        </div>
                        <div class="item">
                            <h2 class="nino-sectionHeading">
                                <span class="nino-subHeading">Creative Template</span>
                                Welcome <br>to MoGo
                            </h2>
                            <a href="#" class="nino-btn">Learn more</a>
                        </div>
                    </div>

                    <!-- Indicators -->
                    <ol class="carousel-indicators clearfix">
                        <li data-target="#nino-slider" data-slide-to="0" class="active">
                            <div class="inner">
                                <span class="number">01</span> intro	
                            </div>
                        </li>
                        <li data-target="#nino-slider" data-slide-to="1">
                            <div class="inner">
                                <span class="number">02</span> work
                            </div>
                        </li>
                        <li data-target="#nino-slider" data-slide-to="2">
                            <div class="inner">
                                <span class="number">03</span> about
                            </div>
                        </li>
                        <li data-target="#nino-slider" data-slide-to="3">
                            <div class="inner">
                                <span class="number">04</span> contacts
                            </div>
                        </li>
                    </ol>
                </section>
            </div>
        </header><!--/#header-->

        <!-- Story About Us
    ================================================== -->
        <section id="nino-story">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">What we do</span>
                    Story about us
                </h2>
                <p class="nino-sectionDesc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                <div class="sectionContent">
                    <div class="row nino-hoverEffect">
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <a class="overlay" href="#">
                                    <span class="content">
                                        <i class="mdi mdi-account-multiple nino-icon"></i>
                                        super team
                                    </span>
                                    <img src="images/story/img-1.jpg" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <a class="overlay" href="#">
                                    <span class="content">
                                        <i class="mdi mdi-image-filter-center-focus-weak nino-icon"></i>
                                        Creativity
                                    </span>
                                    <img src="images/story/img-2.jpg" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <a class="overlay" href="#">
                                    <span class="content">
                                        <i class="mdi mdi-airplay nino-icon"></i>
                                        Digital
                                    </span>
                                    <img src="images/story/img-3.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>		
        </section><!--/#nino-story-->

        <!-- Counting
    ================================================== -->
        <section id="nino-counting">
            <div class="container">
                <div layout="row" class="verticalStretch">
                    <div class="item">
                        <div class="number">42</div>
                        <div class="text">Web Design Projects</div>
                    </div>
                    <div class="item">
                        <div class="number">123</div>
                        <div class="text">happy client</div>
                    </div>
                    <div class="item">
                        <div class="number">15</div>
                        <div class="text">award winner</div>
                    </div>
                    <div class="item">
                        <div class="number">99</div>
                        <div class="text">cup of coffee</div>
                    </div>
                    <div class="item">
                        <div class="number">24</div>
                        <div class="text">members</div>
                    </div>
                </div>
            </div>
        </section><!--/#nino-counting-->

        <!-- Services
        ================================================== -->
        <section id="registration">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">For all users</span>
                    Registeration Form
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                <img src="images/what-we-do/reg_page.png" alt="">
                                <img src="images/what-we-do/regsquad-teamjump.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6"> 
                            <form method="post" autocomplete="off" action="insert_Reg.php" enctype="multipart/form-data"class="nino-subscribeForm">
                                <?php
                                if (isset($errMSG)) {
                                    ?>
                                    <div class="form-group"  style="width: 50%">
                                        <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                            <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <lable id="name_error" style="color: Red; display: none">Please Enter Name</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user"  style="color: #95e1d3" ></span></span> 
                                        <input type="text" name="uname" id="uname" class="form-control" placeholder="Enter Name" required/>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $(function () {
                                            $("#uname").blur(function () {
                                                if ($(this).val() == "") {
                                                    document.getElementById("name_error").style.display = 'block';
                                                    $("#uname").focus();
                                                } else
                                                {
                                                    document.getElementById("name_error").style.display = 'none';
                                                }
                                            });
                                        });
                                        </script>
                                    </div>
                                </div>
                                <lable id="phone_error" style="color: Red; display: none">Enter Phone no</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt text-success"  style="color: #95e1d3" ></span></span>
                                        <input type="number" name="phone" id="phone" class="form-control" placeholder="Phone No." required/>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#phone").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("phone_error").style.display = 'block';
                                                    $("#phone").focus();
                                            } else
                                            {
                                                document.getElementById("phone_error").style.display = 'none';
                                            }

                                        });
                                        </script>
                                    </div>
                                </div>
                                <lable id="Email_error" style="color: Red; display: none">Enter Email Id</lable>
                                <lable id="EmailValid_error" style="color: Red; display: none">Please enter valid Email</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope text-success"  style="color: #95e1d3"></span></span>
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" required/>

                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#email").blur(function () {

                                            $('#email').filter(function () {
                                                var emil = $('#email').val();
                                                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                                                if (!emailReg.test(emil)) {
                                                    document.getElementById("Email_error").style.display = 'block';
                                                    $("#Email_error").text("Please enter valid email");
                                                        $("#uname").focus();

                                                } else if ($('#email').val() == "")
                                                {
                                                    document.getElementById("Email_error").style.display = 'block';
                                                    $("#Email_error").text("Enter Email Id");
                                                        $("#uname").focus();
                                                } else
                                                {
                                                    document.getElementById("Email_error").style.display = 'none';
                                                }
                                            });
                                        });

                                        </script>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-home text-success"  style="color: #95e1d3"></span></span>
                                        <input type="textarea" name="add" id="add" class="form-control" placeholder="Enter Address" required/>
                                    </div>
                                </div>
                                <lable id="dob_error" style="color: Red; display: none">Enter date of birth</lable>
                                <div class="form-group">                    
                                    <div class='input-group input-group-lg date' id='datetimepicker1'>
                                        <span class="input-group-addon">
                                            <span class="fa fa-birthday-cake text-success"  style="color: #95e1d3"></span>
                                        </span>
                                        <input type="text" name="dob" id="dob" class = "form-control" placeholder = "Date Of  Birth" required/>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#dob").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("dob_error").style.display = 'block';
                                                    $("#dob").focus();
                                            } else
                                            {
                                                document.getElementById("dob_error").style.display = 'none';
                                            }
                                        });
                                        </script>
                                    </div>                    
                                </div>
                                <lable id="AgeCategory_error" style="color: Red; display: none">Please Select Age Category</lable>
                                <div class="form-group">                    
                                    <div class='input-group input-group-lg'>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"  style="color: #95e1d3"></span>
                                        </span>
                                        <select  class = "form-control" name="age" id="age" required >
                                            <option value="">Select Age Category </option>
                                            <option value="Child">Child : 8 - 12 Yrs</option>
                                            <option value="Youth">Youth : 13 -16 Yrs</option>
                                            <option value="Open">Open : 17 Yrs & Above </option>    
                                        </select>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#age").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("AgeCategory_error").style.display = 'block';
                                                    $("#age").focus();
                                            } else
                                            {
                                                document.getElementById("AgeCategory_error").style.display = 'none';
                                            }
                                        });
                                        </script>

                                    </div>                    
                                </div>
                                <div class="form-group">                    
                                    <div class='input-group input-group-lg'>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-heart text-success"  style="color: #95e1d3"></span>
                                        </span>
                                        <select  class = "form-control" name="abt_us" id="abt_us" required>
                                            <option value="">How did you know about us</option>
                                            <option value="Facebook">Facebook</option>
                                            <option value="Urbandesis">Urbandesis</option>
                                            <option value="School">School</option>    
                                            <option value="Internet">Internet</option>    
                                        </select>
                                    </div>                    
                                </div>
                                <lable id="language_error" style="color: Red; display: none">Please Select Language</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"><span class="fa fa-american-sign-language-interpreting text-success"  style="color: #95e1d3"></span></span>
                                        <?php
                                        $link = DbConnect::GetConnection();
                                        if ($link) {
                                            $sql = "SELECT id, language FROM Language";
                                            $result = $link->query($sql);
                                            $select = '<select name="Language" id="Language"  class="form-control" required>';
                                            $select .= '<option value="">Select Language</option>';
                                            while ($row = $result->fetch_assoc()) {
                                                $select .= '<option value="' . $row['id'] . '">' . $row['language'] . '</option>';
                                            }
                                            $select .= '</select>';
                                            echo $select;
                                        }
                                        ?>
                                         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#Language").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("language_error").style.display = 'block';
                                                    $("#Language").focus();
                                            } else
                                            {
                                                document.getElementById("language_error").style.display = 'none';
                                            }
                                        });
                                        </script>

                                    </div>
                                </div>
                                  <lable id="comp_error" style="color: Red; display: none">Please Select Competition</lable>
                                <div class="form-group" >
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon" ><span class="glyphicon glyphicon-copyright-mark text-success"  style="color: #95e1d3"></span></span>
                                        <div id="div_comp"  name ="div_comp" class="input-group input-group-lg" style="width: 100%">
                                        </div>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#Compitation").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("comp_error").style.display = 'block';
                                                    $("#Compitation").focus();
                                            } else
                                            {
                                                document.getElementById("comp_error").style.display = 'none';
                                            }
                                        });
                                        </script>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-home text-success"  style="color: #95e1d3"></span></span>
                                        <input type="text" name="School" id="School" class="form-control" placeholder="Enter School Name"/ required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-lg"  >
                                        <span class="input-group-addon"> <span class="fa fa-user" style="color: #95e1d3"></span></span>
                                        <input type="text" name="Teacher" id="Teacher" class="form-control" placeholder="Enter Teacher Name" required/>
                                    </div>
                                </div>


                                <div class="form-group" style="display:none; padding-left:20px"" id="M_lable">
                                    <div class="input-group input-group-lg"  >

                                        <label style="color:red;">For Minors Only</label>

                                    </div>
                                </div>

                                <div class="form-group" style="display:none" id="P_name">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"> <span class="fa fa-user" style="color: #95e1d3"></span></span>
                                        <input type="text" name="Parents" id="Parents" class="form-control" placeholder="Enter Parents Name" />
                                    </div>
                                </div>
                                <div class="form-group"  style="display:none" id="p_email">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-envelope text-success" style="color: #95e1d3"></span></span>
                                        <input type="email" name="P_Email" id="P_Email" class="form-control" placeholder="Enter Parents Email"/>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none" id="country">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"> <span class="fa fa-user" style="color: #95e1d3"></span></span>
                                        <input type="text" name="Country" id="Country" class="form-control" placeholder="Enter Country Of Residence"/>
                                    </div>
                                </div>


                                <div class="form-group">                    
                                    <div class='input-group input-group-lg'>
                                        <input type="file" name="file" id="file"  required/>
                                    </div>                    
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success" name="signup" id="signup">Register </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div id="popup">
                <Center>
                    Record added successfully 
                        </Center>
                        </div>-->
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js_1/tos.js"></script>
            <script src="js_1/moment.js" type="text/javascript"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>

            <script>
                                        $(function () {
                                            $('#dob').datetimepicker({
                                                format: 'YYYY-DD-MM'
                                            });
                                            $('#dob').on('dp.change', function (e) {

                                                var d = new Date(e.date);
                                                var year = d.getFullYear();
                                                var today = new Date();
                                                var today_year = today.getFullYear();
                                                var age = today_year - year;
                                                if (age <= 18)
                                                {
                                                    document.getElementById('M_lable').style.display = 'block';
                                                    document.getElementById('P_name').style.display = 'block';
                                                    document.getElementById('p_email').style.display = 'block';
                                                    document.getElementById('country').style.display = 'block';
                                                } else
                                                {
                                                    document.getElementById('M_lable').style.display = 'none';
                                                    document.getElementById('P_name').style.display = 'none';
                                                    document.getElementById('p_email').style.display = 'none';
                                                    document.getElementById('country').style.display = 'none';
                                                }
                                            });
                                        });
            </script>

            <script>

            </script>

            <script>
                $(function () {
                    $("#Language").change(function () {
                        //               var selectedText = $(this).find("option:selected").text();
                        //                var selectedValue = $(this).val();
                        //                alert("Selected Text: " + selectedText + " Value: " + selectedValue);
                        LoadComp();
                    });

                    LoadComp();

                });



                function LoadComp() {
                    $.ajax({
                        url: 'Reg_responce.php',
                        type: "POST",
                        data: ({Language: $("#Language").val()}),

                        success: function (data) {
                            console.log(data);
                            $("#div_comp").html(data);
                        }
                    });
                }
            </script>
        </section><!--/#nino-services-->

        <!-- Unique Design
                                                                           ================================================== -->
        <section id="nino-uniqueDesign">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">For all devices</span>
                    Unique design
                </h2>
                <div class="sectionContent">
                    <div class="nino-devices">
                        <img class="tablet" src="images/unique-design/img-1.png" alt="">
                        <img class="mobile" src="images/unique-design/img-2.png" alt="">
                    </div>
                </div>
            </div>
        </section><!--/#nino-uniqueDesign-->

        <!-- What We Do
        ================================================== -->
        <section id="nino-whatWeDo">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">Service</span>
                    what we do
                </h2>
                <p class="nino-sectionDesc">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                </p>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                <img src="images/what-we-do/img-1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="mdi mdi-chevron-up nino-icon arrow"></i>
                                                <i class="mdi mdi-camera nino-icon"></i> 
                                                Photography
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <i class="mdi mdi-chevron-up nino-icon arrow"></i>
                                                <i class="mdi mdi-owl nino-icon"></i> 
                                                creativity
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <i class="mdi mdi-chevron-up nino-icon arrow"></i>
                                                <i class="mdi mdi-laptop-mac nino-icon"></i> 
                                                web design
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--/#nino-whatWeDo-->

        <!-- Testimonial
        ================================================== -->
        <section class="nino-testimonial">
            <div class="container">
                <div class="nino-testimonialSlider">
                    <ul>
                        <li>
                            <div layout="row">
                                <div class="nino-symbol fsr">
                                    <i class="mdi mdi-comment-multiple-outline nino-icon"></i>
                                </div>
                                <div>
                                    <p class="quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation."</p>
                                    <span class="name">Jon Doe</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div layout="row">
                                <div class="nino-symbol fsr">
                                    <i class="mdi mdi-wechat nino-icon"></i>	
                                </div>
                                <div>
                                    <p class="quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation."</p>
                                    <span class="name">Jon Doe</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div layout="row">
                                <div class="nino-symbol fsr">
                                    <i class="mdi mdi-message-text-outline nino-icon"></i>
                                </div>
                                <div>
                                    <p class="quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation."</p>
                                    <span class="name">Jon Doe</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section><!--/#nino-testimonial-->

        <!-- Our Team
        ================================================== -->
        <section id="nino-ourTeam">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">Who we are</span>
                    Meet our team
                </h2>
                <p class="nino-sectionDesc">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                </p>
                <div class="sectionContent">
                    <div class="row nino-hoverEffect">
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="overlay" href="#">
                                    <div class="content">
                                        <a href="#" class="nino-icon"><i class="mdi mdi-facebook"></i></a>
                                        <a href="#" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                        <a href="#" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                        <a href="#" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                    </div>
                                    <img src="images/our-team/img-1.jpg" alt="">
                                </div>
                            </div>
                            <div class="info">
                                <h4 class="name">Matthew Dix</h4>
                                <span class="regency">Graphic Design</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="overlay" href="#">
                                    <div class="content">
                                        <a href="#" class="nino-icon"><i class="mdi mdi-facebook"></i></a>
                                        <a href="#" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                        <a href="#" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                        <a href="#" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                    </div>
                                    <img src="images/our-team/img-2.jpg" alt="">
                                </div>
                            </div>
                            <div class="info">
                                <h4 class="name">Christopher Campbell</h4>
                                <span class="regency">Branding/UX design</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="overlay" href="#">
                                    <div class="content">
                                        <a href="#" class="nino-icon"><i class="mdi mdi-facebook"></i></a>
                                        <a href="#" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                        <a href="#" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                        <a href="#" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                    </div>
                                    <img src="images/our-team/img-3.jpg" alt="">
                                </div>
                            </div>
                            <div class="info">
                                <h4 class="name">Michael Fertig </h4>
                                <span class="regency">Developer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--/#nino-ourTeam-->

        <!-- Brand
    ================================================== -->
        <section id="nino-brand">
            <div class="container">
                <div class="verticalCenter fw" layout="row">
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-1.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-2.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-3.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-4.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-5.png" alt=""></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-6"><a href="#"><img src="images/brand/img-6.png" alt=""></a></div>
                </div>
            </div>
        </section><!--/#nino-brand-->

        <!-- Portfolio
    ================================================== -->
        <section id="nino-portfolio">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">What we do</span>
                    some of our work
                </h2>
                <p class="nino-sectionDesc">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                </p>
            </div>
            <div class="sectionContent">
                <ul class="nino-portfolioItems">
                    <li class="item">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="Development Mobile" href="images/our-work/img-1.jpg">
                            <img src="images/our-work/img-1.jpg" />
                            <div class="overlay">
                                <div class="content">
                                    <i class="mdi mdi-crown nino-icon"></i>
                                    <h4 class="title">creatively designed</h4>
                                    <span class="desc">Lorem ipsum dolor sit</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="item">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="Development Mobile" href="images/our-work/img-2.jpg">
                            <img src="images/our-work/img-2.jpg" />
                            <div class="overlay">
                                <div class="content">
                                    <i class="mdi mdi-cube-outline nino-icon"></i>
                                    <h4 class="title">creatively designed</h4>
                                    <span class="desc">Lorem ipsum dolor sit</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="item">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="Development Mobile" href="images/our-work/img-3.jpg">
                            <img src="images/our-work/img-3.jpg" />
                            <div class="overlay">
                                <div class="content">
                                    <i class="mdi mdi-desktop-mac nino-icon"></i>
                                    <h4 class="title">creatively designed</h4>
                                    <span class="desc">Lorem ipsum dolor sit</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="item">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="Development Mobile" href="images/our-work/img-4.jpg">
                            <img src="images/our-work/img-4.jpg" />
                            <div class="overlay">
                                <div class="content">
                                    <i class="mdi mdi-flower nino-icon"></i>
                                    <h4 class="title">creatively designed</h4>
                                    <span class="desc">Lorem ipsum dolor sit</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="item">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="Development Mobile" href="images/our-work/img-5.jpg">
                            <img src="images/our-work/img-5.jpg" />
                            <div class="overlay">
                                <div class="content">
                                    <i class="mdi mdi-gamepad-variant nino-icon"></i>
                                    <h4 class="title">creatively designed</h4>
                                    <span class="desc">Lorem ipsum dolor sit</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="item">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="Development Mobile" href="images/our-work/img-6.jpg">
                            <img src="images/our-work/img-6.jpg" />
                            <div class="overlay">
                                <div class="content">
                                    <i class="mdi mdi-gnome nino-icon"></i>
                                    <h4 class="title">creatively designed</h4>
                                    <span class="desc">Lorem ipsum dolor sit</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="item">
                        <a class="nino-prettyPhoto" rel="prettyPhoto[gallery1]" title="Development Mobile" href="images/our-work/img-7.jpg">
                            <img src="images/our-work/img-7.jpg" />
                            <div class="overlay">
                                <div class="content">
                                    <i class="mdi mdi-guitar-electric nino-icon"></i>
                                    <h4 class="title">creatively designed</h4>
                                    <span class="desc">Lorem ipsum dolor sit</span>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </section><!--/#nino-portfolio-->

        <!-- Testimonial
    ================================================== -->
        <section class="nino-testimonial bg-white">
            <div class="container">
                <div class="nino-testimonialSlider">
                    <ul>
                        <li>
                            <div layout="row" class="verticalCenter">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle img-thumbnail" src="images/testimonial/img-1.jpg" alt="">
                                </div>
                                <div>
                                    <p class="quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation."</p>
                                    <span class="name">Joshua Earle</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div layout="row" class="verticalCenter">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle img-thumbnail" src="images/testimonial/img-2.jpg" alt="">
                                </div>
                                <div>
                                    <p class="quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation."</p>
                                    <span class="name">Jon Doe</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div layout="row" class="verticalCenter">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle img-thumbnail" src="images/testimonial/img-3.jpg" alt="">
                                </div>
                                <div>
                                    <p class="quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation."</p>
                                    <span class="name">Jon Doe</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section><!--/#nino-testimonial-->

        <!-- Happy Client
        ================================================== -->
        <section id="nino-happyClient">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">Happy Clients</span>
                    What people say
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6">
                            <div layout="row" class="item">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle" src="images/happy-client/img-1.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h4 class="name">Matthew Dix</h4>
                                    <span class="regency">Graphic Design</span>
                                    <p class="desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo illo cupiditate temporibus sapiente, sint, voluptatibus tempora esse. Consectetur voluptate nihil quo nulla voluptatem dolorem harum nostrum
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div layout="row" class="item">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle" src="images/happy-client/img-2.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h4 class="name">Nick Karvounis</h4>
                                    <span class="regency">Graphic Design</span>
                                    <p class="desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo illo cupiditate temporibus sapiente, sint, voluptatibus tempora esse. Consectetur voluptate nihil quo nulla voluptatem dolorem harum nostrum
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div layout="row" class="item">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle" src="images/happy-client/img-3.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h4 class="name">Jaelynn Castillo</h4>
                                    <span class="regency">Graphic Design</span>
                                    <p class="desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo illo cupiditate temporibus sapiente, sint, voluptatibus tempora esse. Consectetur voluptate nihil quo nulla voluptatem dolorem harum nostrum
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div layout="row" class="item">
                                <div class="nino-avatar fsr">
                                    <img class="img-circle" src="images/happy-client/img-4.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h4 class="name">Mike Petrucci</h4>
                                    <span class="regency">Graphic Design</span>
                                    <p class="desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo illo cupiditate temporibus sapiente, sint, voluptatibus tempora esse. Consectetur voluptate nihil quo nulla voluptatem dolorem harum nostrum
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--/#nino-happyClient-->

        <!-- Latest Blog
        ================================================== -->
        <section id="nino-latestBlog">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">Our stories</span>
                    Latest Blog
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <article>
                                <div class="articleThumb">
                                    <a href="#"><img src="images/our-blog/img-1.jpg" alt=""></a>
                                    <div class="date">
                                        <span class="number">15</span>
                                        <span class="text">Jan</span>
                                    </div>
                                </div>
                                <h3 class="articleTitle"><a href="">Lorem ipsum dolor sit amet</a></h3>
                                <p class="articleDesc">
                                    Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="articleMeta">
                                    <a href="#"><i class="mdi mdi-eye nino-icon"></i> 543</a>
                                    <a href="#"><i class="mdi mdi-comment-multiple-outline nino-icon"></i> 15</a>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <article>
                                <div class="articleThumb">
                                    <a href="#"><img src="images/our-blog/img-2.jpg" alt=""></a>
                                    <div class="date">
                                        <span class="number">14</span>
                                        <span class="text">Jan</span>
                                    </div>
                                </div>
                                <h3 class="articleTitle"><a href="">sed do eiusmod tempor</a></h3>
                                <p class="articleDesc">
                                    Adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="articleMeta">
                                    <a href="#"><i class="mdi mdi-eye nino-icon"></i> 995</a>
                                    <a href="#"><i class="mdi mdi-comment-multiple-outline nino-icon"></i> 42</a>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <article>
                                <div class="articleThumb">
                                    <a href="#"><img src="images/our-blog/img-3.jpg" alt=""></a>
                                    <div class="date">
                                        <span class="number">12</span>
                                        <span class="text">Jan</span>
                                    </div>
                                </div>
                                <h3 class="articleTitle"><a href="">incididunt ut labore et dolore</a></h3>
                                <p class="articleDesc">
                                    Elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="articleMeta">
                                    <a href="#"><i class="mdi mdi-eye nino-icon"></i> 1264</a>
                                    <a href="#"><i class="mdi mdi-comment-multiple-outline nino-icon"></i> 69</a>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--/#nino-latestBlog-->

        <!-- Map
        ================================================== -->
        <section id="nino-map">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <i class="mdi mdi-map-marker nino-icon"></i>
                    <span class="text">Open map</span>
                    <span class="text" style="display: none;">Close map</span>
                </h2>
                <div class="mapWrap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d79466.26604960626!2d-0.19779784176715043!3d51.50733004537892!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C+UK!3m2!1d51.5073509!2d-0.1277583!5e0!3m2!1sen!2s!4v1469206441744" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </section><!--/#nino-map-->

        <!-- Footer
        ================================================== -->
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="colInfo">
                            <div class="footerLogo">
                                <a href="#" >MoGo</a>	
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                            <div class="nino-followUs">
                                <div class="totalFollow"><span>15k</span> followers</div>
                                <div class="socialNetwork">
                                    <span class="text">Follow Us: </span>
                                    <a href="" class="nino-icon"><i class="mdi mdi-facebook"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-twitter"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-instagram"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-pinterest"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-google-plus"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-youtube-play"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-dribbble"></i></a>
                                    <a href="" class="nino-icon"><i class="mdi mdi-tumblr"></i></a>
                                </div>
                            </div>
                            <form action="" class="nino-subscribeForm">
                                <div class="input-group input-group-lg">
                                    <input type="email" class="form-control" placeholder="Your Email" required>
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="submit">Subscribe</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="colInfo">
                            <h3 class="nino-colHeading">Blogs</h3>
                            <ul class="listArticles">
                                <li layout="row" class="verticalCenter">
                                    <a class="articleThumb fsr" href="#"><img src="images/our-blog/img-4.jpg" alt=""></a>
                                    <div class="info">
                                        <h3 class="articleTitle"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing</a></h3>
                                        <div class="date">Jan 9, 2016</div>
                                    </div>
                                </li>
                                <li layout="row" class="verticalCenter">
                                    <a class="articleThumb fsr" href="#"><img src="images/our-blog/img-5.jpg" alt=""></a>
                                    <div class="info">
                                        <h3 class="articleTitle"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing</a></h3>
                                        <div class="date">Jan 9, 2016</div>
                                    </div>
                                </li>
                                <li layout="row" class="verticalCenter">
                                    <a class="articleThumb fsr" href="#"><img src="images/our-blog/img-6.jpg" alt=""></a>
                                    <div class="info">
                                        <h3 class="articleTitle"><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing</a></h3>
                                        <div class="date">Jan 9, 2016</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="colInfo">
                            <h3 class="nino-colHeading">instagram</h3>
                            <div class="instagramImages clearfix">
                                <a href="#"><img src="images/instagram/img-1.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-2.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-3.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-4.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-5.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-6.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-7.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-8.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-9.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-3.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-4.jpg" alt=""></a>
                                <a href="#"><img src="images/instagram/img-5.jpg" alt=""></a>
                            </div>
                            <a href="#" class="morePhoto">View more photos</a>
                        </div>
                    </div>
                </div>
                <div class="nino-copyright">Copyright &copy; 2016 <a target="_blank" href="http://www.ninodezign.com/" title="Ninodezign.com - Top quality open source resources for web developer and web designer">Ninodezign.com</a>. All Rights Reserved. <br/> MoGo free PSD template by <a href="https://www.behance.net/laaqiq">Laaqiq</a></div>
            </div>
        </footer><!--/#footer-->

        <!-- Search Form - Display when click magnify icon in menu
        ================================================== -->
        <form action="" id="nino-searchForm">
            <input type="text" placeholder="Search..." class="form-control nino-searchInput">
            <i class="mdi mdi-close nino-close"></i>
        </form><!--/#nino-searchForm-->

        <!-- Scroll to top
        ================================================== -->
        <a href="#" id="nino-scrollToTop">Go to Top</a>

        <!-- javascript -->
        <script type="text/javascript" src="js/jquery.min.js"></script>	
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
        <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="js/unslider-min.js"></script>
        <script type="text/javascript" src="js/template.js"></script>
        <script src="js_1/moment.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>


        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- css3-mediaqueries.js for IE less than 9 -->
        <!--[if lt IE 9]>
            <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->

    </body>
</html>