 

<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Mogo | OnePage Responsive Theme</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
        <header id="nino-header1" style="background-color: #F14F72">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="homepage.html">Mogo</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 
                                    <li><a onclick="window.location.href = 'index.php'"><img src="images/brand/back.png" alt=""></a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->




        <!-- Services
        ================================================== -->
        <section id="registration">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">For all users</span>
                Create New User
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6" style="padding-top: 40px;padding-left: 40px">
                            <div class="text-center">
                                <img src="images/what-we-do/securitykeys.png" alt="">
                              
                            </div>
                        </div>
                        <div class="col-md-6"> 
                           <form method="post" autocomplete="off" action="insert_newUser.php" enctype="multipart/form-data">
                <div class="col-md-8">
                    <row>
                        
                        <?php
                        if (isset($errMSG)) {
                            ?>
                            <div class="form-group">
                                <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                    <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user  text-success"  style="color: #95e1d3" ></span></span>
                                <input type="text" name="Fname" id="Fname" class="form-control" placeholder="Enter Your Name"  required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope  text-success"  style="color: #95e1d3"></span></span>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user  text-success"  style="color: #95e1d3" ></span></span>
                                <input type="text" name="uname" id="uname" class="form-control" placeholder="Username"  required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock  text-success"  style="color: #95e1d3" ></span></span>
                                <input type="password" name="Password" id="Password" class="form-control" placeholder="Password"  required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock  text-success"  style="color: #95e1d3" ></span></span>
                                <select class = "form-control" name="User_type" id="User_type" required>
                                    <option value="">Select User Type</option>
                                    <option value="Admin">Admin</option>
                                    <option value="judge">judge</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="display:none" id="judge_lang" name="judge_lang">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-american-sign-language-interpreting  text-success"  style="color: #95e1d3"></span></span>
                                <?php
                                $link = DbConnect::GetConnection();
                                if ($link) {
                                    $sql = "SELECT lang_id, language FROM Language";
                                    $result = $link->query($sql);
                                    $select = '<select name="Language" id="Language"  class="form-control" required>';
                                    $select .= '<option value="">Select Language</option>';
                                    while ($row = $result->fetch_assoc()) {
                                        $select .= '<option value="' . $row['lang_id'] . '">' . $row['language'] . '</option>';
                                    }
                                    $select .= '</select>';
                                    echo $select;
                                }
                                ?>
                            </div>
                        </div>
                            
                        
 
                        <div class="form-group">
                            <hr/>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success" name="signup" id="signup">Create </button>
                        </div>
                        <div class="form-group">
                            <hr/>
                        </div>
<!--                        <div id="popup">
                            <Center>
                                Record added successfully 
                            </Center>
                        </div>-->
                </div>
            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div id="popup">
                <Center>
                    Record added successfully 
                        </Center>
                        </div>-->
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js_1/tos.js"></script>
            <script src="js_1/moment.js" type="text/javascript"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>



        </section><!--/#nino-services-->


        <!-- Search Form - Display when click magnify icon in menu
        ================================================== -->
        <form action="" id="nino-searchForm">
            <input type="text" placeholder="Search..." class="form-control nino-searchInput">
            <i class="mdi mdi-close nino-close"></i>
        </form><!--/#nino-searchForm-->

        <!-- Scroll to top
        ================================================== -->
        <a href="#" id="nino-scrollToTop">Go to Top</a>

        <!-- javascript -->
        <script type="text/javascript" src="js/jquery.min.js"></script>	
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
        <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="js/unslider-min.js"></script>
        <script type="text/javascript" src="js/template.js"></script>
        <script src="js_1/moment.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script>
        $(function () {
        $("#User_type").change(function () {
            var u_type=$("#User_type").val();
            if(u_type=="judge")
            {
                 document.getElementById('judge_lang').style.display = 'block';
                 document.getElementById('judge_comp').style.display = 'block';
            }
            else
            {
                document.getElementById('judge_lang').style.display = 'none';
                document.getElementById('judge_comp').style.display = 'none';
            }

        });
        });
        </script>

    </body>
</html>
<?php
        $recordAdded = false;

        if (isset($_GET['status']) && $_GET['status'] == 1) {
            $recordAdded = true;
        }

        if ($recordAdded) {
            echo '
<script type="text/javascript">
    function hideMsg()
    {
        document.getElementById("popup").style.visibility = "hidden";
    }

    document.getElementById("popup").style.visibility = "visible";
    window.setTimeout("hideMsg()", 2000);
</script>';
        }
        ?>
