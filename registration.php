<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Registration</title>
    <link rel="stylesheet" href="css_1/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="css_1/style.css" type="text/css"/>
    <link href="css_1/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css"/>
    <link href="css_1/bootstrap.icon-large.min.css" rel="stylesheet">
</head>
<body>

    <div class="container">

        <div id="login-form">
            <form method="post" autocomplete="off" action="insert_Reg.php" enctype="multipart/form-data">

                <row>
                    <div class="form-group">
                        <div class="col-md-8">
                            <h2 class="">Registeration Form</h2>
                        </div>
                        <div class="col-md-4" style="margin-top: 20px;">
                            <h4><a href="Login.php">User Login</a></h4> 
                        </div>


                    </div>


                    <?php
                    if (isset($errMSG)) {
                        ?>
                        <div class="form-group">
                            <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="col-md-12">
                        <div class="form-group">
                            <hr/>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user" ></span></span>
                                <input type="text" name="uname" id="uname" class="form-control" placeholder="Enter Name" required/>
                                <span class="input-group-addon"><span id="error" style="color: Red; display: none"></span></span>
                                

                       <!--           <script type="text/javascript">
                                    var specialKeys = new Array();
                                    specialKeys.push(8); //Backspace
                                    function IsNumeric(e) {
                                        var keyCode = e.which ? e.which : e.keyCode
                                        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                                        document.getElementById("error").style.display = ret ? "none" : "inline";
                                        return ret;
                                    }
                                </script>-->
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt" ></span></span>
                                <input type="text" name="phone" id="phone" class="form-control"  placeholder="Phone No." onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" required/>
                               
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-home"></span></span>
                                <input type="textarea" name="add" id="add" class="form-control" placeholder="Enter Address" required/>
                            </div>
                        </div>

                        <div class="form-group">                    
                            <div class='input-group date' id='datetimepicker1'>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-bishop"></span>
                                </span>
                                <input type="text" name="dob" id="dob" class = "form-control" placeholder = "Date Of  Birth" />                        
                            </div>                    
                        </div>
                        <div class="form-group">                    
                            <div class='input-group'>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <select  class = "form-control" name="age" id="age" required>
                                    <option value="">Select Age Category </option>
                                    <option value="Child">Child : 8 - 12 Yrs</option>
                                    <option value="Youth">Youth : 13 -16 Yrs</option>
                                    <option value="Open">Open : 17 Yrs & Above </option>    
                                </select>           
                            </div>                    
                        </div>
                        <div class="form-group">                    
                            <div class='input-group'>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-heart"></span>
                                </span>
                                <select  class = "form-control" name="abt_us" id="abt_us" required>
                                    <option value="">How did you know about us</option>
                                    <option value="Facebook">Facebook</option>
                                    <option value="Urbandesis">Urbandesis</option>
                                    <option value="School">School</option>    
                                    <option value="Internet">Internet</option>    
                                </select> 
                            </div>                    
                        </div>
                    </div>

                    <div class="col-md-6"> 
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-font"></span></span> 
                                <?php
                                $link = DbConnect::GetConnection();
                                if ($link) {
                                    $sql = "SELECT id, language FROM Language";
                                    $result = $link->query($sql);
                                    $select = '<select name="Language" id="Language"  class="form-control">';
                                    while ($row = $result->fetch_assoc()) {
                                        $select .= '<option value="' . $row['id'] . '">' . $row['language'] . '</option>';
                                    }
                                    $select .= '</select>';
                                    echo $select;
                                }
                                ?>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6"> 
                        <div class="form-group">
                            <div class="input-group" >
                                <span class="input-group-addon"><span class="glyphicon glyphicon-copyright-mark"></span></span>

                                <div id="div_comp">

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12"> 
                        <div class="form-group">                    
                            <div class='input-group ' >
                                <input type="file" name="file" id="file"  required/>                        
                            </div>                    
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn    btn-block btn-primary" name="signup" id="signup">Register</button>
                    </div>
                    <div class="form-group">
                        <hr/>
                    </div>
            </form>

        </div>

    </div>
    <div id="popup">
        <Center>
            Record added successfully 
        </Center>
    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/tos.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>

    <script>
                                    $(function () {
                                        $('#datetimepicker1').datetimepicker({
                                            format: 'YYYY-DD-MM'
                                        });
                                    });
    </script>
    <script>
        $(function () {
            $("#Language").change(function () {
//               var selectedText = $(this).find("option:selected").text();
//                var selectedValue = $(this).val();
//                alert("Selected Text: " + selectedText + " Value: " + selectedValue);
                LoadComp();

            });

            LoadComp();
        });

        function LoadComp() {
            $.ajax({
                url: 'Reg_responce.php',
                type: "POST",
                data: ({Language: $("#Language").val()}),

                success: function (data) {
                    console.log(data);
                    $("#div_comp").html(data);
                }
            });
        }
    </script>
</body>
</html>
<?php
$recordAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
}

if ($recordAdded) {
    echo '
<script type="text/javascript">
    function hideMsg()
    {
        document.getElementById("popup").style.visibility = "hidden";
    }

    document.getElementById("popup").style.visibility = "visible";
    window.setTimeout("hideMsg()", 2000);
</script>';
}
?>
