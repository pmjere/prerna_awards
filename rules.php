 

<?php
session_start();
include_once './dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Prerna Awards</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">Prerna Awards</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 
                                    <li><a onclick="window.location.href = 'index.php'">Home</a></li> 
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->




        <!-- Services
        ================================================== -->
        <div class="col-md-6" style="padding-top: 40px">
            <div class="text-center">
                <img src="images/what-we-do/New_Logo.png" alt="">
            </div>
        </div>
    <section1 id="registration">
        <div class="container">


            <div class="sectionContent" style="text-align: justify">

                <div>
                    <row> 
                         <h4 class="nino-sectionHeading1">

                          PRERNA AWARDS 2018 
                        </h4>
                        <h2 class="nino-sectionHeading2">

                            RULES & REGUALTIONS
                        </h2>
                        
                         <h2 class="nino-sectionHeading3">

                             FOLK TALES RETOLD IN MODERN TIMES
                        </h2>
                
                    </row>
                </div>


                1. All the participants have to submit a story based on the Theme Folk Tales Retold in Modern Times<br><br>

                2. Stories presented can be adapted from folk tales of any Region or Language or Country and presented in the language selected for the competition and can be done as prose and/or poetry.<br><br>

                3. The story and/or poem must be entirely the original work of the entrant and must have reference to any folk tale.<br><br>

                4. Participants are free to use any medium for telling their stories ranging from Audio Visual aids, puppet, traditional story-telling, songs, mono acting, use props backdrop, sound effects etc.<br><br>

                5. You can submit your entries in one or more language in your age group via website submission. Each entry will require a complete registration process.<br><br>

                6. Registration fee must be made online.<br><br>

                7.  Entries must be accompanied by a completed entry form and payment proof before submission.<br><br>

                8. Participants and winners from previous years can participate.<br><br>

                9. The competition is open to all Singaporean/PR/EP/Work Permit/Students/DP residing in Singapore only.<br><br>

                10. Age Groups: Child (8-12 Yr.) | Youth (13-17 Yr.) | Open (18 Yr. & above) on the submission date.<br><br>

                11. Stories uploaded through the website: Minimum 250 words and Maximum 1200 word, Video should not more than 2 mins (only for shortlisting phase). <br><br>

                12. Entries must be typed, single sided A4. Handwritten entries may be scanned and must be legible.  <br><br>

                13. A video submission of the presentation in MP4 format must be uploaded along with the transcript.<br><br>

                14. Shortlisted participants will have to present in front of the judges for a minimum duration of 2 minutes & maximum of 7 minutes.<br><br>

                15. Entries must never have been published, self-published on any website or broadcast in any form nor currently entered in any other competition.<br><br>

                16. The stories and/or poem must not have won a prize in any previous competition.<br><br>

                17. All entries will be considered anonymously by the judges.<br><br>

                18. Competitions entries will not be returned, so please keep a copy of your submission. <br><br>

                19. We are unable under any circumstances to allow changes to be made to the content once submitted.<br><br>

                20. Entrants may withdraw entries from the competition by notification in writing but any entry fees will not be refunded.<br><br>

                21. Singapore Language Organisation reserves the right to amend these rules where it is deemed necessary to do so or where circumstances are beyond the organizers control. Any changes to the rules will be posted on www.prerna-awards.org.<br><br>

                22. Submissions must be entered under an entrant’s real name.<br><br>

                23. The organizers cannot accept responsibility for any damage, loss, injury or disappointment suffered by any entrant entering the Competition.<br><br>

                24. Finalists will be notified by 5 February’ 18 and invited to present in front of the jury on 10 /11 February’ 18. <br><br>

                25. All finalist must attend the PRERNA AWARDS FUNCTION on 24 February’ 18 to collect their awards and/or certificates. <br><br>

                26. Those who are unable to attend (No arrangements can be made to handover the prizes), only E-certificates will be provided.<br><br>

                27. All winners will be required to provide a short profile and photograph.<br><br>

                28. The copyright of all the entries remains with the authors. However, the participant's submitted entry to the competition, grant Singapore Language Organisation the right to publish in print and online/digital and/or broadcast their winning story in perpetuity.<br><br>

                29. Individual scores will not be disclosed.<br><br>

                30. The decision of the judges shall be final and binding on all the participants & organizers.<br><br>



                <p style="font-size: 20px;color: black">PRESENTATION GUIDELINES – INITIAL VIDEO SUBMISSION & LIVE PRESENTATION IF SHORTLISTED</p><br>

                1. Participants are free to choose their own topic. However, they are expected to monitor the content and delivery of the speeches according to the standards of good taste and according to the theme of the competition.<br><br>

                2. Presenters should use language, which is appropriate to the topic, the audience and the formality of the occasion. Avoid off-colour jokes, slang expressions and “bathroom humour”.<br><br>

                3. Statements which might be construed as slanderous, embarrassing, or containing gender, cultural, or racial stereotypes are unsuitable for a public competition. Speakers, from the beginning, should consider their intended audience to be “the public” – a mixed age group, not simply an audience of peers.<br><br>

                4. Participants are expected to dress neatly and appropriately to suit the formality of the occasion, and to stand appropriately on the platform.<br><br>

                5. Gestures & expressions can be dramatic and props are allowed to be used for Story Telling. (Examples of unacceptable expressions- turning one’s back to the audience, vulgar & ugly gestures). <br><br>

                6. Inappropriate language or comments will be disqualified (vulgarity, swearing and that sort).<br><br>

                7. No (explicit) sexual content is allowed.<br><br>

                8. No religious references can be made.<br><br>

                9. Notes on cards not exceeding 3” x 5” in size are permitted for reference for final competition.<br><br>

                10. Presentation shall not be less than two minutes and not more than 7 minutes in length. <br><br>



            </div>
        </div>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js_1/tos.js"></script>
        <script src="js_1/moment.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>



    </section1><!--/#nino-services-->

    <footer id="footer">
        <div class="nino-copyright">
            <div class="row">
                <div class="colInfo">
                    <div class="col-md-1"></div>
                    <div class="col-md-7" style="font-size: 17px ;padding-left: 20px">
                        <ul class="nav navbar-nav">
                            <li><a href="Privacy_Policy.php" a target="_blank"  style="color: #172f6a">Privacy Policy</a></li>
                            <li><a href="rules.php" a target="_blank"  style="color: #172f6a">Rules and Regulations</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3" style="padding-left: 30px">
                        <div class="nino-followUs">
                            <div class="socialNetwork">
                                <span class="text" style="font-size: 17px ;" >Follow Us: </span>
                                <a href="https://www.facebook.com/prernaawards" class="nino-icon"  a target="_blank"><i class="mdi mdi-facebook"  style="color: #172f6a"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            Copyright &copy; 2017 <a target="_blank" href="http://prerna-awards.org/index.php"  style="color: #172f6a">Prerna Awards.</a> All Rights Reserved. <br/> Created By <a href="http://www.edifynow.com/" style="color: #172f6a">Edify Now</a></div>
    </div>
</footer>



<!-- javascript -->
<script type="text/javascript" src="js/jquery.min.js"></script>	
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.hoverdir.js"></script>
<script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="js/unslider-min.js"></script>
<script type="text/javascript" src="js/template.js"></script>
<script src="js_1/moment.js" type="text/javascript"></script>
<script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script>
                                        $(function () {
                                            $("#User_type").change(function () {
                                                var u_type = $("#User_type").val();
                                                if (u_type == "judge")
                                                {
                                                    document.getElementById('judge_lang').style.display = 'block';
                                                    document.getElementById('judge_comp').style.display = 'block';
                                                } else
                                                {
                                                    document.getElementById('judge_lang').style.display = 'none';
                                                    document.getElementById('judge_comp').style.display = 'none';
                                                }

                                            });
                                        });
</script>

</body>
</html>
<?php
$recordAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
}

if ($recordAdded) {
    echo '
<script type="text/javascript">
    function hideMsg()
    {
        document.getElementById("popup").style.visibility = "hidden";
    }

    document.getElementById("popup").style.visibility = "visible";
    window.setTimeout("hideMsg()", 2000);
</script>';
}
?>
