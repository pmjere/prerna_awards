-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema registration_system
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema registration_system
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `registration_system` DEFAULT CHARACTER SET latin1 ;
USE `registration_system` ;

-- -----------------------------------------------------
-- Table `registration_system`.`competition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registration_system`.`competition` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `competition` VARCHAR(50) NOT NULL,
  `language_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `registration_system`.`language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registration_system`.`language` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `language` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `registration_system`.`registration`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registration_system`.`registration` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `contact_no` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `email` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `address` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `DOB` DATETIME NOT NULL,
  `File_name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `Language_id` INT(11) NOT NULL,
  `competition_id` INT(11) NOT NULL,
  `Age` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `reference` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `school_name` VARCHAR(45) NULL DEFAULT NULL,
  `teacher_name` VARCHAR(45) NULL DEFAULT NULL,
  `Parents_name` VARCHAR(45) NULL DEFAULT NULL,
  `Parents_email` VARCHAR(45) NULL DEFAULT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `language_idx` (`Language_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `registration_system`.`userdata`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registration_system`.`userdata` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `User_name` VARCHAR(255) NOT NULL,
  `Password` VARCHAR(255) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `Email` VARCHAR(45) NULL DEFAULT NULL,
  `User_type` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
