 

<?php
session_start();
include_once './dbconnect.php';

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ninodezign.com, ninodezign@gmail.com">
        <meta name="copyright" content="ninodezign.com"> 
        <title>Mogo | OnePage Responsive Theme</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="images/ico/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css" />
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" />
        <link rel="stylesheet" type="text/css" href="css/unslider.css" />
        <link rel="stylesheet" type="text/css" href="css/template.css" />

    </head>

    <body data-target="#nino-navbar" data-spy="scroll">
        <header id="nino-header1" style="background-color: #182441">
            <div id="nino-headerInner">					
                <nav id="nino-navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nino-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="homepage.html">Mogo</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="nino-menuItem pull-right">
                            <div class="collapse navbar-collapse pull-left" id="nino-navbar-collapse">
                                <ul class="nav navbar-nav"> 
<!--                                    <li><a onclick="window.location.href = 'index.php'">Home</a></li>-->
                                      <li><a onclick="window.location.href = 'contestant-login.php'">Logout</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </div>
                    </div><!-- /.container-fluid -->
                </nav>
                </section>
            </div>
        </header><!--/#header-->




        <!-- Services
        ================================================== -->
     <section id="registration">
            <div class="container">
                <h2 class="nino-sectionHeading">
                    <span class="nino-subHeading">For all users</span>
               Update Profile
                </h2>
                <div class="sectionContent">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                <img src="images/what-we-do/reg.png" alt="">
<!--                                <img src="images/what-we-do/regsquad-teamjump.png" alt="">-->
                            </div>
                        </div>
                        <div class="col-md-6" pa> 
                            <form method="post" autocomplete="off" action="Update_comp.php"id="frm_update" name="frm_update" enctype="multipart/form-data"class="nino-subscribeForm">
                                  <lable id="language_error" style="color: Red; display: none">Please Select Language</lable>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"><span class="fa fa-american-sign-language-interpreting text-success"  style="color: #95e1d3"></span></span>
                                        <?php
                                        $link = DbConnect::GetConnection();
                                        if ($link) {
                                            $sql = "SELECT lang_id, language FROM Language";
                                            $result = $link->query($sql);
                                            $select = '<select name="Language" id="Language"  class="form-control" required>';
                                            $select .= '<option value="">Select Language</option>';
                                            while ($row = $result->fetch_assoc()) {
                                                $select .= '<option value="' . $row['lang_id'] . '">' . $row['language'] . '</option>';
                                            }
                                            $select .= '</select>';
                                            echo $select;
                                        }
                                        ?>
                                         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#Language").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("language_error").style.display = 'block';
                                                    $("#Language").focus();
                                            } else
                                            {
                                                document.getElementById("language_error").style.display = 'none';
                                            }
                                        });
                                        </script>

                                    </div>
                                </div>
                                  <lable id="comp_error" style="color: Red; display: none">Please Select Competition</lable>
                                <div class="form-group" >
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon" ><span class="glyphicon glyphicon-copyright-mark text-success"  style="color: #95e1d3"></span></span>
                                        <div id="div_comp"  name ="div_comp" class="input-group input-group-lg" style="width: 100%">
                                        </div>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                                        <script type="text/javascript">
                                        $("#Compitation").blur(function () {
                                            if ($(this).val() == "") {
                                                document.getElementById("comp_error").style.display = 'block';
                                                    $("#Compitation").focus();
                                            } else
                                            {
                                                document.getElementById("comp_error").style.display = 'none';
                                            }
                                        });
                                        </script>
                                    </div>
                                </div>
                                    <div class="form-group">                    
                                    <div class='input-group input-group-lg'>
                                        <input type="file" name="file" id="file"  required/>
                                    </div>                    
                                </div>
                              
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success" name="signup" id="signup">Register </button>
                                </div>
                            </form>
                                  <div id="popup3"style="display: none">
                                <h2 style="text-align: center">Thank you!</h2> 
                                <div class="content" style="text-align: center">                               
                                    <br><br>Thank you very much for your participation !
                                      <form action="lg.php">
                                        <br><br><button type="submit" class="btn btn-block btn-success">Register for another competititon </button>
                                    </form>
                                    <form action="Update_profile.php">
                                        <br><br><button type="submit" class="btn btn-block btn-success">Back </button>
                                    </form>
                                   
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="row" style="padding-top: 30px">
                        <div class="col-md-6"></div>
                           <div class="col-md-6">
                               <h2 class="nino-sectionHeading">
                   
          Competitions
                </h2>
                        <table class="table table-striped table-hover table-responsive">
                    <thead>
                        <tr>
                             
                            <th style="font-size: 20px;color: black">Language</th>
                            <th style="font-size: 20px;color: black">Competition</th>  
                 
                        </tr>
                    </thead>
                    <tbody style="font-size: 15px;color: black">
                        <?php
                        $link = DbConnect::GetConnection();
                        if ($link) {
                        $RId=$_SESSION['Creg_id'];
//                                $sql = " SELECT * FROM    contestant_registration r  INNER JOIN language l ON r.Language_id = l.lang_id INNER JOIN competition c ON r.competition_id = c.com_id where Contestant_id='$RId'";
                          $sql="SELECT * FROM  participation r  INNER JOIN language l ON r.pLanguage_id = l.lang_id INNER JOIN competition c ON r.pCompetition_id = c.com_id where pCont_id='$RId'";
                            $result = $link->query($sql);
                            $record = '';
                            while ($row = $result->fetch_assoc()) {

                                $reg_id = $row["pLanguage_id"];
                                    $reg_id = $row["pCompetition_id"];

                                $record .= '<tr><td>' .  $row["language"] . '</td><td>' . $row["competition"] . '</td> <td>
                                 

                            </td></tr>';
                            }
                            echo $record;
                        }
                        ?>
                    </tbody>
                </table>
                    </div>
                    </div>
                </div>
            </div>
            <!--            <div id="popup">
                <Center>
                    Record added successfully 
                        </Center>
                        </div>-->
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js_1/tos.js"></script>
            <script src="js_1/moment.js" type="text/javascript"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>

            <script>
                                        $(function () {
                                            $('#dob').datetimepicker({
                                                format: 'YYYY-DD-MM'
                                            });
                                            $('#dob').on('dp.change', function (e) {

                                                var d = new Date(e.date);
                                                var year = d.getFullYear();
                                                var today = new Date();
                                                var today_year = today.getFullYear();
                                                var age = today_year - year;
                                                if (age <= 18)
                                                {
                                                    document.getElementById('M_lable').style.display = 'block';
                                                    document.getElementById('P_name').style.display = 'block';
                                                    document.getElementById('p_email').style.display = 'block';
                                                    document.getElementById('country').style.display = 'block';
                                                } else
                                                {
                                                    document.getElementById('M_lable').style.display = 'none';
                                                    document.getElementById('P_name').style.display = 'none';
                                                    document.getElementById('p_email').style.display = 'none';
                                                    document.getElementById('country').style.display = 'none';
                                                }
                                                
                                                if(age<=12)
                                                {
                                                     document.getElementById("age").selectedIndex = 1;
                                                }
                                                else if(age>=13&& age<=16)
                                               {
                                                      document.getElementById("age").selectedIndex = 2;
                                                }
                                              else if(age>=17)
                                              {
                                                      document.getElementById("age").selectedIndex = 3;
                                              }
                                            });
                                        });
            </script>

            <script>

            </script>

            <script>
                $(function () {
                    $("#Language").change(function () {
                        //               var selectedText = $(this).find("option:selected").text();
                        //                var selectedValue = $(this).val();
                        //                alert("Selected Text: " + selectedText + " Value: " + selectedValue);
                        LoadComp();
                    });

                    LoadComp();

                });



                function LoadComp() {
                    $.ajax({
                        url: 'Reg_responce.php',
                        type: "POST",
                        data: ({Language: $("#Language").val()}),

                        success: function (data) {
                            console.log(data);
                            $("#div_comp").html(data);
                        }
                    });
                }
            </script>
        </section><!--/#nino-services-->


        <!-- Search Form - Display when click magnify icon in menu
        ================================================== -->
        <form action="" id="nino-searchForm">
            <input type="text" placeholder="Search..." class="form-control nino-searchInput">
            <i class="mdi mdi-close nino-close"></i>
        </form><!--/#nino-searchForm-->

        <!-- Scroll to top
        ================================================== -->
        <a href="#" id="nino-scrollToTop">Go to Top</a>

        <!-- javascript -->
        <script type="text/javascript" src="js/jquery.min.js"></script>	
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.97074.js"></script>
        <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="js/unslider-min.js"></script>
        <script type="text/javascript" src="js/template.js"></script>
        <script src="js_1/moment.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <script src="js_1/bootstrap-datetimepicker.min.js" type="text/javascript"></script>


    </body>
</html>
<?php
$recordAdded = false;

if (isset($_GET['status']) && $_GET['status'] == 1) {
    $recordAdded = true;
}

if ($recordAdded) {
    echo '
<script type="text/javascript">
     
    document.getElementById("frm_update").style.display = "none";
    document.getElementById("popup3").style.display = "block";
  
</script>';
}
?>